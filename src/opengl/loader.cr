require "bit_array"
require "./procs"
require "./translations"

module OpenGL
  # Raised when an OpenGL isn't loaded or unavailable to call.
  class FunctionUnavailableError < Exception; end

  # Loads OpenGL functions dynamically at runtime and returns `Proc` instances to invoke them.
  # The OpenGL functions are lazy-loaded.
  struct Loader
    FUNCTION_COUNT = 657

    # Creates the loader.
    # The *get_proc_address* block is used to retrieve addresses of OpenGL functions.
    # It is given a string that is the name of the OpenGL function to lookup.
    # The block must return a pointer to the function corresponding to the name.
    # If a function is unavailable, the block should return a null pointer.
    def initialize(&@get_proc_address : ::String -> ::Void*)
      @addresses = ::Pointer(::Void*).malloc(FUNCTION_COUNT)
      @loaded = ::BitArray.new(FUNCTION_COUNT)
    end

    @[AlwaysInline]
    private def get_address(index, name) : ::Pointer(::Void)
      return @addresses[index] if @loaded.unsafe_fetch(index)
      address = @get_proc_address.call(name)
      @addresses[index] = address
      @loaded.unsafe_put(index, true)
      address
    end

    @[AlwaysInline]
    private def get_proc(index, name, proc_type)
      return unless address = get_address(index, name)
      proc_type.new(address, ::Pointer(::Void).null)
    end

    # Retrieves a `Proc` for the OpenGL function *glCullFace*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def cull_face
      get_proc(0, Translations.cull_face, Procs.cull_face)
    end

    # Retrieves a `Proc` for the OpenGL function *glCullFace*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def cull_face!
      self.cull_face ||
        raise FunctionUnavailableError.new(Translations.cull_face)
    end

    # Checks if the OpenGL function *glCullFace* is available.
    def cull_face?
      !get_address(0, Translations.cull_face)
    end

    # Retrieves a `Proc` for the OpenGL function *glFrontFace*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def front_face
      get_proc(1, Translations.front_face, Procs.front_face)
    end

    # Retrieves a `Proc` for the OpenGL function *glFrontFace*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def front_face!
      self.front_face ||
        raise FunctionUnavailableError.new(Translations.front_face)
    end

    # Checks if the OpenGL function *glFrontFace* is available.
    def front_face?
      !get_address(1, Translations.front_face)
    end

    # Retrieves a `Proc` for the OpenGL function *glHint*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def hint
      get_proc(2, Translations.hint, Procs.hint)
    end

    # Retrieves a `Proc` for the OpenGL function *glHint*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def hint!
      self.hint ||
        raise FunctionUnavailableError.new(Translations.hint)
    end

    # Checks if the OpenGL function *glHint* is available.
    def hint?
      !get_address(2, Translations.hint)
    end

    # Retrieves a `Proc` for the OpenGL function *glLineWidth*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def line_width
      get_proc(3, Translations.line_width, Procs.line_width)
    end

    # Retrieves a `Proc` for the OpenGL function *glLineWidth*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def line_width!
      self.line_width ||
        raise FunctionUnavailableError.new(Translations.line_width)
    end

    # Checks if the OpenGL function *glLineWidth* is available.
    def line_width?
      !get_address(3, Translations.line_width)
    end

    # Retrieves a `Proc` for the OpenGL function *glPointSize*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def point_size
      get_proc(4, Translations.point_size, Procs.point_size)
    end

    # Retrieves a `Proc` for the OpenGL function *glPointSize*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def point_size!
      self.point_size ||
        raise FunctionUnavailableError.new(Translations.point_size)
    end

    # Checks if the OpenGL function *glPointSize* is available.
    def point_size?
      !get_address(4, Translations.point_size)
    end

    # Retrieves a `Proc` for the OpenGL function *glPolygonMode*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def polygon_mode
      get_proc(5, Translations.polygon_mode, Procs.polygon_mode)
    end

    # Retrieves a `Proc` for the OpenGL function *glPolygonMode*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def polygon_mode!
      self.polygon_mode ||
        raise FunctionUnavailableError.new(Translations.polygon_mode)
    end

    # Checks if the OpenGL function *glPolygonMode* is available.
    def polygon_mode?
      !get_address(5, Translations.polygon_mode)
    end

    # Retrieves a `Proc` for the OpenGL function *glScissor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def scissor
      get_proc(6, Translations.scissor, Procs.scissor)
    end

    # Retrieves a `Proc` for the OpenGL function *glScissor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def scissor!
      self.scissor ||
        raise FunctionUnavailableError.new(Translations.scissor)
    end

    # Checks if the OpenGL function *glScissor* is available.
    def scissor?
      !get_address(6, Translations.scissor)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameterf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_parameter_f
      get_proc(7, Translations.tex_parameter_f, Procs.tex_parameter_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameterf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_parameter_f!
      self.tex_parameter_f ||
        raise FunctionUnavailableError.new(Translations.tex_parameter_f)
    end

    # Checks if the OpenGL function *glTexParameterf* is available.
    def tex_parameter_f?
      !get_address(7, Translations.tex_parameter_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_parameter_fv
      get_proc(8, Translations.tex_parameter_fv, Procs.tex_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_parameter_fv!
      self.tex_parameter_fv ||
        raise FunctionUnavailableError.new(Translations.tex_parameter_fv)
    end

    # Checks if the OpenGL function *glTexParameterfv* is available.
    def tex_parameter_fv?
      !get_address(8, Translations.tex_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_parameter_i
      get_proc(9, Translations.tex_parameter_i, Procs.tex_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_parameter_i!
      self.tex_parameter_i ||
        raise FunctionUnavailableError.new(Translations.tex_parameter_i)
    end

    # Checks if the OpenGL function *glTexParameteri* is available.
    def tex_parameter_i?
      !get_address(9, Translations.tex_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_parameter_iv
      get_proc(10, Translations.tex_parameter_iv, Procs.tex_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_parameter_iv!
      self.tex_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.tex_parameter_iv)
    end

    # Checks if the OpenGL function *glTexParameteriv* is available.
    def tex_parameter_iv?
      !get_address(10, Translations.tex_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_image_1d
      get_proc(11, Translations.tex_image_1d, Procs.tex_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_image_1d!
      self.tex_image_1d ||
        raise FunctionUnavailableError.new(Translations.tex_image_1d)
    end

    # Checks if the OpenGL function *glTexImage1D* is available.
    def tex_image_1d?
      !get_address(11, Translations.tex_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_image_2d
      get_proc(12, Translations.tex_image_2d, Procs.tex_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_image_2d!
      self.tex_image_2d ||
        raise FunctionUnavailableError.new(Translations.tex_image_2d)
    end

    # Checks if the OpenGL function *glTexImage2D* is available.
    def tex_image_2d?
      !get_address(12, Translations.tex_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_buffer
      get_proc(13, Translations.draw_buffer, Procs.draw_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_buffer!
      self.draw_buffer ||
        raise FunctionUnavailableError.new(Translations.draw_buffer)
    end

    # Checks if the OpenGL function *glDrawBuffer* is available.
    def draw_buffer?
      !get_address(13, Translations.draw_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glClear*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear
      get_proc(14, Translations.clear, Procs.clear)
    end

    # Retrieves a `Proc` for the OpenGL function *glClear*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear!
      self.clear ||
        raise FunctionUnavailableError.new(Translations.clear)
    end

    # Checks if the OpenGL function *glClear* is available.
    def clear?
      !get_address(14, Translations.clear)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearColor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_color
      get_proc(15, Translations.clear_color, Procs.clear_color)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearColor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_color!
      self.clear_color ||
        raise FunctionUnavailableError.new(Translations.clear_color)
    end

    # Checks if the OpenGL function *glClearColor* is available.
    def clear_color?
      !get_address(15, Translations.clear_color)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearStencil*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_stencil
      get_proc(16, Translations.clear_stencil, Procs.clear_stencil)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearStencil*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_stencil!
      self.clear_stencil ||
        raise FunctionUnavailableError.new(Translations.clear_stencil)
    end

    # Checks if the OpenGL function *glClearStencil* is available.
    def clear_stencil?
      !get_address(16, Translations.clear_stencil)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearDepth*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_depth
      get_proc(17, Translations.clear_depth, Procs.clear_depth)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearDepth*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_depth!
      self.clear_depth ||
        raise FunctionUnavailableError.new(Translations.clear_depth)
    end

    # Checks if the OpenGL function *glClearDepth* is available.
    def clear_depth?
      !get_address(17, Translations.clear_depth)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilMask*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def stencil_mask
      get_proc(18, Translations.stencil_mask, Procs.stencil_mask)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilMask*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def stencil_mask!
      self.stencil_mask ||
        raise FunctionUnavailableError.new(Translations.stencil_mask)
    end

    # Checks if the OpenGL function *glStencilMask* is available.
    def stencil_mask?
      !get_address(18, Translations.stencil_mask)
    end

    # Retrieves a `Proc` for the OpenGL function *glColorMask*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def color_mask
      get_proc(19, Translations.color_mask, Procs.color_mask)
    end

    # Retrieves a `Proc` for the OpenGL function *glColorMask*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def color_mask!
      self.color_mask ||
        raise FunctionUnavailableError.new(Translations.color_mask)
    end

    # Checks if the OpenGL function *glColorMask* is available.
    def color_mask?
      !get_address(19, Translations.color_mask)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthMask*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def depth_mask
      get_proc(20, Translations.depth_mask, Procs.depth_mask)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthMask*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def depth_mask!
      self.depth_mask ||
        raise FunctionUnavailableError.new(Translations.depth_mask)
    end

    # Checks if the OpenGL function *glDepthMask* is available.
    def depth_mask?
      !get_address(20, Translations.depth_mask)
    end

    # Retrieves a `Proc` for the OpenGL function *glDisable*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def disable
      get_proc(21, Translations.disable, Procs.disable)
    end

    # Retrieves a `Proc` for the OpenGL function *glDisable*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def disable!
      self.disable ||
        raise FunctionUnavailableError.new(Translations.disable)
    end

    # Checks if the OpenGL function *glDisable* is available.
    def disable?
      !get_address(21, Translations.disable)
    end

    # Retrieves a `Proc` for the OpenGL function *glEnable*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def enable
      get_proc(22, Translations.enable, Procs.enable)
    end

    # Retrieves a `Proc` for the OpenGL function *glEnable*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def enable!
      self.enable ||
        raise FunctionUnavailableError.new(Translations.enable)
    end

    # Checks if the OpenGL function *glEnable* is available.
    def enable?
      !get_address(22, Translations.enable)
    end

    # Retrieves a `Proc` for the OpenGL function *glFinish*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def finish
      get_proc(23, Translations.finish, Procs.finish)
    end

    # Retrieves a `Proc` for the OpenGL function *glFinish*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def finish!
      self.finish ||
        raise FunctionUnavailableError.new(Translations.finish)
    end

    # Checks if the OpenGL function *glFinish* is available.
    def finish?
      !get_address(23, Translations.finish)
    end

    # Retrieves a `Proc` for the OpenGL function *glFlush*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def flush
      get_proc(24, Translations.flush, Procs.flush)
    end

    # Retrieves a `Proc` for the OpenGL function *glFlush*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def flush!
      self.flush ||
        raise FunctionUnavailableError.new(Translations.flush)
    end

    # Checks if the OpenGL function *glFlush* is available.
    def flush?
      !get_address(24, Translations.flush)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendFunc*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blend_func
      get_proc(25, Translations.blend_func, Procs.blend_func)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendFunc*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blend_func!
      self.blend_func ||
        raise FunctionUnavailableError.new(Translations.blend_func)
    end

    # Checks if the OpenGL function *glBlendFunc* is available.
    def blend_func?
      !get_address(25, Translations.blend_func)
    end

    # Retrieves a `Proc` for the OpenGL function *glLogicOp*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def logic_op
      get_proc(26, Translations.logic_op, Procs.logic_op)
    end

    # Retrieves a `Proc` for the OpenGL function *glLogicOp*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def logic_op!
      self.logic_op ||
        raise FunctionUnavailableError.new(Translations.logic_op)
    end

    # Checks if the OpenGL function *glLogicOp* is available.
    def logic_op?
      !get_address(26, Translations.logic_op)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilFunc*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def stencil_func
      get_proc(27, Translations.stencil_func, Procs.stencil_func)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilFunc*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def stencil_func!
      self.stencil_func ||
        raise FunctionUnavailableError.new(Translations.stencil_func)
    end

    # Checks if the OpenGL function *glStencilFunc* is available.
    def stencil_func?
      !get_address(27, Translations.stencil_func)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilOp*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def stencil_op
      get_proc(28, Translations.stencil_op, Procs.stencil_op)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilOp*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def stencil_op!
      self.stencil_op ||
        raise FunctionUnavailableError.new(Translations.stencil_op)
    end

    # Checks if the OpenGL function *glStencilOp* is available.
    def stencil_op?
      !get_address(28, Translations.stencil_op)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthFunc*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def depth_func
      get_proc(29, Translations.depth_func, Procs.depth_func)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthFunc*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def depth_func!
      self.depth_func ||
        raise FunctionUnavailableError.new(Translations.depth_func)
    end

    # Checks if the OpenGL function *glDepthFunc* is available.
    def depth_func?
      !get_address(29, Translations.depth_func)
    end

    # Retrieves a `Proc` for the OpenGL function *glPixelStoref*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def pixel_store_f
      get_proc(30, Translations.pixel_store_f, Procs.pixel_store_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glPixelStoref*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def pixel_store_f!
      self.pixel_store_f ||
        raise FunctionUnavailableError.new(Translations.pixel_store_f)
    end

    # Checks if the OpenGL function *glPixelStoref* is available.
    def pixel_store_f?
      !get_address(30, Translations.pixel_store_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glPixelStorei*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def pixel_store_i
      get_proc(31, Translations.pixel_store_i, Procs.pixel_store_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glPixelStorei*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def pixel_store_i!
      self.pixel_store_i ||
        raise FunctionUnavailableError.new(Translations.pixel_store_i)
    end

    # Checks if the OpenGL function *glPixelStorei* is available.
    def pixel_store_i?
      !get_address(31, Translations.pixel_store_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glReadBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def read_buffer
      get_proc(32, Translations.read_buffer, Procs.read_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glReadBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def read_buffer!
      self.read_buffer ||
        raise FunctionUnavailableError.new(Translations.read_buffer)
    end

    # Checks if the OpenGL function *glReadBuffer* is available.
    def read_buffer?
      !get_address(32, Translations.read_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glReadPixels*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def read_pixels
      get_proc(33, Translations.read_pixels, Procs.read_pixels)
    end

    # Retrieves a `Proc` for the OpenGL function *glReadPixels*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def read_pixels!
      self.read_pixels ||
        raise FunctionUnavailableError.new(Translations.read_pixels)
    end

    # Checks if the OpenGL function *glReadPixels* is available.
    def read_pixels?
      !get_address(33, Translations.read_pixels)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBooleanv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_boolean_v
      get_proc(34, Translations.get_boolean_v, Procs.get_boolean_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBooleanv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_boolean_v!
      self.get_boolean_v ||
        raise FunctionUnavailableError.new(Translations.get_boolean_v)
    end

    # Checks if the OpenGL function *glGetBooleanv* is available.
    def get_boolean_v?
      !get_address(34, Translations.get_boolean_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetDoublev*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_double_v
      get_proc(35, Translations.get_double_v, Procs.get_double_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetDoublev*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_double_v!
      self.get_double_v ||
        raise FunctionUnavailableError.new(Translations.get_double_v)
    end

    # Checks if the OpenGL function *glGetDoublev* is available.
    def get_double_v?
      !get_address(35, Translations.get_double_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetError*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_error
      get_proc(36, Translations.get_error, Procs.get_error)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetError*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_error!
      self.get_error ||
        raise FunctionUnavailableError.new(Translations.get_error)
    end

    # Checks if the OpenGL function *glGetError* is available.
    def get_error?
      !get_address(36, Translations.get_error)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFloatv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_float_v
      get_proc(37, Translations.get_float_v, Procs.get_float_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFloatv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_float_v!
      self.get_float_v ||
        raise FunctionUnavailableError.new(Translations.get_float_v)
    end

    # Checks if the OpenGL function *glGetFloatv* is available.
    def get_float_v?
      !get_address(37, Translations.get_float_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetIntegerv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_integer_v
      get_proc(38, Translations.get_integer_v, Procs.get_integer_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetIntegerv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_integer_v!
      self.get_integer_v ||
        raise FunctionUnavailableError.new(Translations.get_integer_v)
    end

    # Checks if the OpenGL function *glGetIntegerv* is available.
    def get_integer_v?
      !get_address(38, Translations.get_integer_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetString*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_string
      get_proc(39, Translations.get_string, Procs.get_string)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetString*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_string!
      self.get_string ||
        raise FunctionUnavailableError.new(Translations.get_string)
    end

    # Checks if the OpenGL function *glGetString* is available.
    def get_string?
      !get_address(39, Translations.get_string)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_tex_image
      get_proc(40, Translations.get_tex_image, Procs.get_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_tex_image!
      self.get_tex_image ||
        raise FunctionUnavailableError.new(Translations.get_tex_image)
    end

    # Checks if the OpenGL function *glGetTexImage* is available.
    def get_tex_image?
      !get_address(40, Translations.get_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_tex_parameter_fv
      get_proc(41, Translations.get_tex_parameter_fv, Procs.get_tex_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_tex_parameter_fv!
      self.get_tex_parameter_fv ||
        raise FunctionUnavailableError.new(Translations.get_tex_parameter_fv)
    end

    # Checks if the OpenGL function *glGetTexParameterfv* is available.
    def get_tex_parameter_fv?
      !get_address(41, Translations.get_tex_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_tex_parameter_iv
      get_proc(42, Translations.get_tex_parameter_iv, Procs.get_tex_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_tex_parameter_iv!
      self.get_tex_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_tex_parameter_iv)
    end

    # Checks if the OpenGL function *glGetTexParameteriv* is available.
    def get_tex_parameter_iv?
      !get_address(42, Translations.get_tex_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexLevelParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_tex_level_parameter_fv
      get_proc(43, Translations.get_tex_level_parameter_fv, Procs.get_tex_level_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexLevelParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_tex_level_parameter_fv!
      self.get_tex_level_parameter_fv ||
        raise FunctionUnavailableError.new(Translations.get_tex_level_parameter_fv)
    end

    # Checks if the OpenGL function *glGetTexLevelParameterfv* is available.
    def get_tex_level_parameter_fv?
      !get_address(43, Translations.get_tex_level_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexLevelParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_tex_level_parameter_iv
      get_proc(44, Translations.get_tex_level_parameter_iv, Procs.get_tex_level_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexLevelParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_tex_level_parameter_iv!
      self.get_tex_level_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_tex_level_parameter_iv)
    end

    # Checks if the OpenGL function *glGetTexLevelParameteriv* is available.
    def get_tex_level_parameter_iv?
      !get_address(44, Translations.get_tex_level_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsEnabled*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_enabled
      get_proc(45, Translations.is_enabled, Procs.is_enabled)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsEnabled*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_enabled!
      self.is_enabled ||
        raise FunctionUnavailableError.new(Translations.is_enabled)
    end

    # Checks if the OpenGL function *glIsEnabled* is available.
    def is_enabled?
      !get_address(45, Translations.is_enabled)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def depth_range
      get_proc(46, Translations.depth_range, Procs.depth_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def depth_range!
      self.depth_range ||
        raise FunctionUnavailableError.new(Translations.depth_range)
    end

    # Checks if the OpenGL function *glDepthRange* is available.
    def depth_range?
      !get_address(46, Translations.depth_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glViewport*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def viewport
      get_proc(47, Translations.viewport, Procs.viewport)
    end

    # Retrieves a `Proc` for the OpenGL function *glViewport*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def viewport!
      self.viewport ||
        raise FunctionUnavailableError.new(Translations.viewport)
    end

    # Checks if the OpenGL function *glViewport* is available.
    def viewport?
      !get_address(47, Translations.viewport)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawArrays*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_arrays
      get_proc(48, Translations.draw_arrays, Procs.draw_arrays)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawArrays*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_arrays!
      self.draw_arrays ||
        raise FunctionUnavailableError.new(Translations.draw_arrays)
    end

    # Checks if the OpenGL function *glDrawArrays* is available.
    def draw_arrays?
      !get_address(48, Translations.draw_arrays)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElements*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_elements
      get_proc(49, Translations.draw_elements, Procs.draw_elements)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElements*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_elements!
      self.draw_elements ||
        raise FunctionUnavailableError.new(Translations.draw_elements)
    end

    # Checks if the OpenGL function *glDrawElements* is available.
    def draw_elements?
      !get_address(49, Translations.draw_elements)
    end

    # Retrieves a `Proc` for the OpenGL function *glPolygonOffset*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def polygon_offset
      get_proc(50, Translations.polygon_offset, Procs.polygon_offset)
    end

    # Retrieves a `Proc` for the OpenGL function *glPolygonOffset*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def polygon_offset!
      self.polygon_offset ||
        raise FunctionUnavailableError.new(Translations.polygon_offset)
    end

    # Checks if the OpenGL function *glPolygonOffset* is available.
    def polygon_offset?
      !get_address(50, Translations.polygon_offset)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTexImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_tex_image_1d
      get_proc(51, Translations.copy_tex_image_1d, Procs.copy_tex_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTexImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_tex_image_1d!
      self.copy_tex_image_1d ||
        raise FunctionUnavailableError.new(Translations.copy_tex_image_1d)
    end

    # Checks if the OpenGL function *glCopyTexImage1D* is available.
    def copy_tex_image_1d?
      !get_address(51, Translations.copy_tex_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTexImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_tex_image_2d
      get_proc(52, Translations.copy_tex_image_2d, Procs.copy_tex_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTexImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_tex_image_2d!
      self.copy_tex_image_2d ||
        raise FunctionUnavailableError.new(Translations.copy_tex_image_2d)
    end

    # Checks if the OpenGL function *glCopyTexImage2D* is available.
    def copy_tex_image_2d?
      !get_address(52, Translations.copy_tex_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTexSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_tex_sub_image_1d
      get_proc(53, Translations.copy_tex_sub_image_1d, Procs.copy_tex_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTexSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_tex_sub_image_1d!
      self.copy_tex_sub_image_1d ||
        raise FunctionUnavailableError.new(Translations.copy_tex_sub_image_1d)
    end

    # Checks if the OpenGL function *glCopyTexSubImage1D* is available.
    def copy_tex_sub_image_1d?
      !get_address(53, Translations.copy_tex_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTexSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_tex_sub_image_2d
      get_proc(54, Translations.copy_tex_sub_image_2d, Procs.copy_tex_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTexSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_tex_sub_image_2d!
      self.copy_tex_sub_image_2d ||
        raise FunctionUnavailableError.new(Translations.copy_tex_sub_image_2d)
    end

    # Checks if the OpenGL function *glCopyTexSubImage2D* is available.
    def copy_tex_sub_image_2d?
      !get_address(54, Translations.copy_tex_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_sub_image_1d
      get_proc(55, Translations.tex_sub_image_1d, Procs.tex_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_sub_image_1d!
      self.tex_sub_image_1d ||
        raise FunctionUnavailableError.new(Translations.tex_sub_image_1d)
    end

    # Checks if the OpenGL function *glTexSubImage1D* is available.
    def tex_sub_image_1d?
      !get_address(55, Translations.tex_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_sub_image_2d
      get_proc(56, Translations.tex_sub_image_2d, Procs.tex_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_sub_image_2d!
      self.tex_sub_image_2d ||
        raise FunctionUnavailableError.new(Translations.tex_sub_image_2d)
    end

    # Checks if the OpenGL function *glTexSubImage2D* is available.
    def tex_sub_image_2d?
      !get_address(56, Translations.tex_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_texture
      get_proc(57, Translations.bind_texture, Procs.bind_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_texture!
      self.bind_texture ||
        raise FunctionUnavailableError.new(Translations.bind_texture)
    end

    # Checks if the OpenGL function *glBindTexture* is available.
    def bind_texture?
      !get_address(57, Translations.bind_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteTextures*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_textures
      get_proc(58, Translations.delete_textures, Procs.delete_textures)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteTextures*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_textures!
      self.delete_textures ||
        raise FunctionUnavailableError.new(Translations.delete_textures)
    end

    # Checks if the OpenGL function *glDeleteTextures* is available.
    def delete_textures?
      !get_address(58, Translations.delete_textures)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenTextures*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def gen_textures
      get_proc(59, Translations.gen_textures, Procs.gen_textures)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenTextures*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def gen_textures!
      self.gen_textures ||
        raise FunctionUnavailableError.new(Translations.gen_textures)
    end

    # Checks if the OpenGL function *glGenTextures* is available.
    def gen_textures?
      !get_address(59, Translations.gen_textures)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_texture
      get_proc(60, Translations.is_texture, Procs.is_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_texture!
      self.is_texture ||
        raise FunctionUnavailableError.new(Translations.is_texture)
    end

    # Checks if the OpenGL function *glIsTexture* is available.
    def is_texture?
      !get_address(60, Translations.is_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawRangeElements*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_range_elements
      get_proc(61, Translations.draw_range_elements, Procs.draw_range_elements)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawRangeElements*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_range_elements!
      self.draw_range_elements ||
        raise FunctionUnavailableError.new(Translations.draw_range_elements)
    end

    # Checks if the OpenGL function *glDrawRangeElements* is available.
    def draw_range_elements?
      !get_address(61, Translations.draw_range_elements)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_image_3d
      get_proc(62, Translations.tex_image_3d, Procs.tex_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_image_3d!
      self.tex_image_3d ||
        raise FunctionUnavailableError.new(Translations.tex_image_3d)
    end

    # Checks if the OpenGL function *glTexImage3D* is available.
    def tex_image_3d?
      !get_address(62, Translations.tex_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_sub_image_3d
      get_proc(63, Translations.tex_sub_image_3d, Procs.tex_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_sub_image_3d!
      self.tex_sub_image_3d ||
        raise FunctionUnavailableError.new(Translations.tex_sub_image_3d)
    end

    # Checks if the OpenGL function *glTexSubImage3D* is available.
    def tex_sub_image_3d?
      !get_address(63, Translations.tex_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTexSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_tex_sub_image_3d
      get_proc(64, Translations.copy_tex_sub_image_3d, Procs.copy_tex_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTexSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_tex_sub_image_3d!
      self.copy_tex_sub_image_3d ||
        raise FunctionUnavailableError.new(Translations.copy_tex_sub_image_3d)
    end

    # Checks if the OpenGL function *glCopyTexSubImage3D* is available.
    def copy_tex_sub_image_3d?
      !get_address(64, Translations.copy_tex_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glActiveTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def active_texture
      get_proc(65, Translations.active_texture, Procs.active_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glActiveTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def active_texture!
      self.active_texture ||
        raise FunctionUnavailableError.new(Translations.active_texture)
    end

    # Checks if the OpenGL function *glActiveTexture* is available.
    def active_texture?
      !get_address(65, Translations.active_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glSampleCoverage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def sample_coverage
      get_proc(66, Translations.sample_coverage, Procs.sample_coverage)
    end

    # Retrieves a `Proc` for the OpenGL function *glSampleCoverage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def sample_coverage!
      self.sample_coverage ||
        raise FunctionUnavailableError.new(Translations.sample_coverage)
    end

    # Checks if the OpenGL function *glSampleCoverage* is available.
    def sample_coverage?
      !get_address(66, Translations.sample_coverage)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def compressed_tex_image_3d
      get_proc(67, Translations.compressed_tex_image_3d, Procs.compressed_tex_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def compressed_tex_image_3d!
      self.compressed_tex_image_3d ||
        raise FunctionUnavailableError.new(Translations.compressed_tex_image_3d)
    end

    # Checks if the OpenGL function *glCompressedTexImage3D* is available.
    def compressed_tex_image_3d?
      !get_address(67, Translations.compressed_tex_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def compressed_tex_image_2d
      get_proc(68, Translations.compressed_tex_image_2d, Procs.compressed_tex_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def compressed_tex_image_2d!
      self.compressed_tex_image_2d ||
        raise FunctionUnavailableError.new(Translations.compressed_tex_image_2d)
    end

    # Checks if the OpenGL function *glCompressedTexImage2D* is available.
    def compressed_tex_image_2d?
      !get_address(68, Translations.compressed_tex_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def compressed_tex_image_1d
      get_proc(69, Translations.compressed_tex_image_1d, Procs.compressed_tex_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def compressed_tex_image_1d!
      self.compressed_tex_image_1d ||
        raise FunctionUnavailableError.new(Translations.compressed_tex_image_1d)
    end

    # Checks if the OpenGL function *glCompressedTexImage1D* is available.
    def compressed_tex_image_1d?
      !get_address(69, Translations.compressed_tex_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def compressed_tex_sub_image_3d
      get_proc(70, Translations.compressed_tex_sub_image_3d, Procs.compressed_tex_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def compressed_tex_sub_image_3d!
      self.compressed_tex_sub_image_3d ||
        raise FunctionUnavailableError.new(Translations.compressed_tex_sub_image_3d)
    end

    # Checks if the OpenGL function *glCompressedTexSubImage3D* is available.
    def compressed_tex_sub_image_3d?
      !get_address(70, Translations.compressed_tex_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def compressed_tex_sub_image_2d
      get_proc(71, Translations.compressed_tex_sub_image_2d, Procs.compressed_tex_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def compressed_tex_sub_image_2d!
      self.compressed_tex_sub_image_2d ||
        raise FunctionUnavailableError.new(Translations.compressed_tex_sub_image_2d)
    end

    # Checks if the OpenGL function *glCompressedTexSubImage2D* is available.
    def compressed_tex_sub_image_2d?
      !get_address(71, Translations.compressed_tex_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def compressed_tex_sub_image_1d
      get_proc(72, Translations.compressed_tex_sub_image_1d, Procs.compressed_tex_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTexSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def compressed_tex_sub_image_1d!
      self.compressed_tex_sub_image_1d ||
        raise FunctionUnavailableError.new(Translations.compressed_tex_sub_image_1d)
    end

    # Checks if the OpenGL function *glCompressedTexSubImage1D* is available.
    def compressed_tex_sub_image_1d?
      !get_address(72, Translations.compressed_tex_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetCompressedTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_compressed_tex_image
      get_proc(73, Translations.get_compressed_tex_image, Procs.get_compressed_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetCompressedTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_compressed_tex_image!
      self.get_compressed_tex_image ||
        raise FunctionUnavailableError.new(Translations.get_compressed_tex_image)
    end

    # Checks if the OpenGL function *glGetCompressedTexImage* is available.
    def get_compressed_tex_image?
      !get_address(73, Translations.get_compressed_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendFuncSeparate*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blend_func_separate
      get_proc(74, Translations.blend_func_separate, Procs.blend_func_separate)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendFuncSeparate*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blend_func_separate!
      self.blend_func_separate ||
        raise FunctionUnavailableError.new(Translations.blend_func_separate)
    end

    # Checks if the OpenGL function *glBlendFuncSeparate* is available.
    def blend_func_separate?
      !get_address(74, Translations.blend_func_separate)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawArrays*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def multi_draw_arrays
      get_proc(75, Translations.multi_draw_arrays, Procs.multi_draw_arrays)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawArrays*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def multi_draw_arrays!
      self.multi_draw_arrays ||
        raise FunctionUnavailableError.new(Translations.multi_draw_arrays)
    end

    # Checks if the OpenGL function *glMultiDrawArrays* is available.
    def multi_draw_arrays?
      !get_address(75, Translations.multi_draw_arrays)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawElements*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def multi_draw_elements
      get_proc(76, Translations.multi_draw_elements, Procs.multi_draw_elements)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawElements*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def multi_draw_elements!
      self.multi_draw_elements ||
        raise FunctionUnavailableError.new(Translations.multi_draw_elements)
    end

    # Checks if the OpenGL function *glMultiDrawElements* is available.
    def multi_draw_elements?
      !get_address(76, Translations.multi_draw_elements)
    end

    # Retrieves a `Proc` for the OpenGL function *glPointParameterf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def point_parameter_f
      get_proc(77, Translations.point_parameter_f, Procs.point_parameter_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glPointParameterf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def point_parameter_f!
      self.point_parameter_f ||
        raise FunctionUnavailableError.new(Translations.point_parameter_f)
    end

    # Checks if the OpenGL function *glPointParameterf* is available.
    def point_parameter_f?
      !get_address(77, Translations.point_parameter_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glPointParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def point_parameter_fv
      get_proc(78, Translations.point_parameter_fv, Procs.point_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glPointParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def point_parameter_fv!
      self.point_parameter_fv ||
        raise FunctionUnavailableError.new(Translations.point_parameter_fv)
    end

    # Checks if the OpenGL function *glPointParameterfv* is available.
    def point_parameter_fv?
      !get_address(78, Translations.point_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glPointParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def point_parameter_i
      get_proc(79, Translations.point_parameter_i, Procs.point_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glPointParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def point_parameter_i!
      self.point_parameter_i ||
        raise FunctionUnavailableError.new(Translations.point_parameter_i)
    end

    # Checks if the OpenGL function *glPointParameteri* is available.
    def point_parameter_i?
      !get_address(79, Translations.point_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glPointParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def point_parameter_iv
      get_proc(80, Translations.point_parameter_iv, Procs.point_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glPointParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def point_parameter_iv!
      self.point_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.point_parameter_iv)
    end

    # Checks if the OpenGL function *glPointParameteriv* is available.
    def point_parameter_iv?
      !get_address(80, Translations.point_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendColor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blend_color
      get_proc(81, Translations.blend_color, Procs.blend_color)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendColor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blend_color!
      self.blend_color ||
        raise FunctionUnavailableError.new(Translations.blend_color)
    end

    # Checks if the OpenGL function *glBlendColor* is available.
    def blend_color?
      !get_address(81, Translations.blend_color)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendEquation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blend_equation
      get_proc(82, Translations.blend_equation, Procs.blend_equation)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendEquation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blend_equation!
      self.blend_equation ||
        raise FunctionUnavailableError.new(Translations.blend_equation)
    end

    # Checks if the OpenGL function *glBlendEquation* is available.
    def blend_equation?
      !get_address(82, Translations.blend_equation)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenQueries*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def gen_queries
      get_proc(83, Translations.gen_queries, Procs.gen_queries)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenQueries*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def gen_queries!
      self.gen_queries ||
        raise FunctionUnavailableError.new(Translations.gen_queries)
    end

    # Checks if the OpenGL function *glGenQueries* is available.
    def gen_queries?
      !get_address(83, Translations.gen_queries)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteQueries*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_queries
      get_proc(84, Translations.delete_queries, Procs.delete_queries)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteQueries*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_queries!
      self.delete_queries ||
        raise FunctionUnavailableError.new(Translations.delete_queries)
    end

    # Checks if the OpenGL function *glDeleteQueries* is available.
    def delete_queries?
      !get_address(84, Translations.delete_queries)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsQuery*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_query
      get_proc(85, Translations.is_query, Procs.is_query)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsQuery*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_query!
      self.is_query ||
        raise FunctionUnavailableError.new(Translations.is_query)
    end

    # Checks if the OpenGL function *glIsQuery* is available.
    def is_query?
      !get_address(85, Translations.is_query)
    end

    # Retrieves a `Proc` for the OpenGL function *glBeginQuery*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def begin_query
      get_proc(86, Translations.begin_query, Procs.begin_query)
    end

    # Retrieves a `Proc` for the OpenGL function *glBeginQuery*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def begin_query!
      self.begin_query ||
        raise FunctionUnavailableError.new(Translations.begin_query)
    end

    # Checks if the OpenGL function *glBeginQuery* is available.
    def begin_query?
      !get_address(86, Translations.begin_query)
    end

    # Retrieves a `Proc` for the OpenGL function *glEndQuery*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def end_query
      get_proc(87, Translations.end_query, Procs.end_query)
    end

    # Retrieves a `Proc` for the OpenGL function *glEndQuery*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def end_query!
      self.end_query ||
        raise FunctionUnavailableError.new(Translations.end_query)
    end

    # Checks if the OpenGL function *glEndQuery* is available.
    def end_query?
      !get_address(87, Translations.end_query)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_query_iv
      get_proc(88, Translations.get_query_iv, Procs.get_query_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_query_iv!
      self.get_query_iv ||
        raise FunctionUnavailableError.new(Translations.get_query_iv)
    end

    # Checks if the OpenGL function *glGetQueryiv* is available.
    def get_query_iv?
      !get_address(88, Translations.get_query_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryObjectiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_query_object_iv
      get_proc(89, Translations.get_query_object_iv, Procs.get_query_object_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryObjectiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_query_object_iv!
      self.get_query_object_iv ||
        raise FunctionUnavailableError.new(Translations.get_query_object_iv)
    end

    # Checks if the OpenGL function *glGetQueryObjectiv* is available.
    def get_query_object_iv?
      !get_address(89, Translations.get_query_object_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryObjectuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_query_object_uiv
      get_proc(90, Translations.get_query_object_uiv, Procs.get_query_object_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryObjectuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_query_object_uiv!
      self.get_query_object_uiv ||
        raise FunctionUnavailableError.new(Translations.get_query_object_uiv)
    end

    # Checks if the OpenGL function *glGetQueryObjectuiv* is available.
    def get_query_object_uiv?
      !get_address(90, Translations.get_query_object_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_buffer
      get_proc(91, Translations.bind_buffer, Procs.bind_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_buffer!
      self.bind_buffer ||
        raise FunctionUnavailableError.new(Translations.bind_buffer)
    end

    # Checks if the OpenGL function *glBindBuffer* is available.
    def bind_buffer?
      !get_address(91, Translations.bind_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_buffers
      get_proc(92, Translations.delete_buffers, Procs.delete_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_buffers!
      self.delete_buffers ||
        raise FunctionUnavailableError.new(Translations.delete_buffers)
    end

    # Checks if the OpenGL function *glDeleteBuffers* is available.
    def delete_buffers?
      !get_address(92, Translations.delete_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def gen_buffers
      get_proc(93, Translations.gen_buffers, Procs.gen_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def gen_buffers!
      self.gen_buffers ||
        raise FunctionUnavailableError.new(Translations.gen_buffers)
    end

    # Checks if the OpenGL function *glGenBuffers* is available.
    def gen_buffers?
      !get_address(93, Translations.gen_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_buffer
      get_proc(94, Translations.is_buffer, Procs.is_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_buffer!
      self.is_buffer ||
        raise FunctionUnavailableError.new(Translations.is_buffer)
    end

    # Checks if the OpenGL function *glIsBuffer* is available.
    def is_buffer?
      !get_address(94, Translations.is_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glBufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def buffer_data
      get_proc(95, Translations.buffer_data, Procs.buffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glBufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def buffer_data!
      self.buffer_data ||
        raise FunctionUnavailableError.new(Translations.buffer_data)
    end

    # Checks if the OpenGL function *glBufferData* is available.
    def buffer_data?
      !get_address(95, Translations.buffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def buffer_sub_data
      get_proc(96, Translations.buffer_sub_data, Procs.buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def buffer_sub_data!
      self.buffer_sub_data ||
        raise FunctionUnavailableError.new(Translations.buffer_sub_data)
    end

    # Checks if the OpenGL function *glBufferSubData* is available.
    def buffer_sub_data?
      !get_address(96, Translations.buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_buffer_sub_data
      get_proc(97, Translations.get_buffer_sub_data, Procs.get_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_buffer_sub_data!
      self.get_buffer_sub_data ||
        raise FunctionUnavailableError.new(Translations.get_buffer_sub_data)
    end

    # Checks if the OpenGL function *glGetBufferSubData* is available.
    def get_buffer_sub_data?
      !get_address(97, Translations.get_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glMapBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def map_buffer
      get_proc(98, Translations.map_buffer, Procs.map_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glMapBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def map_buffer!
      self.map_buffer ||
        raise FunctionUnavailableError.new(Translations.map_buffer)
    end

    # Checks if the OpenGL function *glMapBuffer* is available.
    def map_buffer?
      !get_address(98, Translations.map_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glUnmapBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def unmap_buffer
      get_proc(99, Translations.unmap_buffer, Procs.unmap_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glUnmapBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def unmap_buffer!
      self.unmap_buffer ||
        raise FunctionUnavailableError.new(Translations.unmap_buffer)
    end

    # Checks if the OpenGL function *glUnmapBuffer* is available.
    def unmap_buffer?
      !get_address(99, Translations.unmap_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_buffer_parameter_iv
      get_proc(100, Translations.get_buffer_parameter_iv, Procs.get_buffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_buffer_parameter_iv!
      self.get_buffer_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_buffer_parameter_iv)
    end

    # Checks if the OpenGL function *glGetBufferParameteriv* is available.
    def get_buffer_parameter_iv?
      !get_address(100, Translations.get_buffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBufferPointerv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_buffer_pointer_v
      get_proc(101, Translations.get_buffer_pointer_v, Procs.get_buffer_pointer_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBufferPointerv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_buffer_pointer_v!
      self.get_buffer_pointer_v ||
        raise FunctionUnavailableError.new(Translations.get_buffer_pointer_v)
    end

    # Checks if the OpenGL function *glGetBufferPointerv* is available.
    def get_buffer_pointer_v?
      !get_address(101, Translations.get_buffer_pointer_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendEquationSeparate*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blend_equation_separate
      get_proc(102, Translations.blend_equation_separate, Procs.blend_equation_separate)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendEquationSeparate*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blend_equation_separate!
      self.blend_equation_separate ||
        raise FunctionUnavailableError.new(Translations.blend_equation_separate)
    end

    # Checks if the OpenGL function *glBlendEquationSeparate* is available.
    def blend_equation_separate?
      !get_address(102, Translations.blend_equation_separate)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_buffers
      get_proc(103, Translations.draw_buffers, Procs.draw_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_buffers!
      self.draw_buffers ||
        raise FunctionUnavailableError.new(Translations.draw_buffers)
    end

    # Checks if the OpenGL function *glDrawBuffers* is available.
    def draw_buffers?
      !get_address(103, Translations.draw_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilOpSeparate*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def stencil_op_separate
      get_proc(104, Translations.stencil_op_separate, Procs.stencil_op_separate)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilOpSeparate*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def stencil_op_separate!
      self.stencil_op_separate ||
        raise FunctionUnavailableError.new(Translations.stencil_op_separate)
    end

    # Checks if the OpenGL function *glStencilOpSeparate* is available.
    def stencil_op_separate?
      !get_address(104, Translations.stencil_op_separate)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilFuncSeparate*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def stencil_func_separate
      get_proc(105, Translations.stencil_func_separate, Procs.stencil_func_separate)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilFuncSeparate*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def stencil_func_separate!
      self.stencil_func_separate ||
        raise FunctionUnavailableError.new(Translations.stencil_func_separate)
    end

    # Checks if the OpenGL function *glStencilFuncSeparate* is available.
    def stencil_func_separate?
      !get_address(105, Translations.stencil_func_separate)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilMaskSeparate*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def stencil_mask_separate
      get_proc(106, Translations.stencil_mask_separate, Procs.stencil_mask_separate)
    end

    # Retrieves a `Proc` for the OpenGL function *glStencilMaskSeparate*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def stencil_mask_separate!
      self.stencil_mask_separate ||
        raise FunctionUnavailableError.new(Translations.stencil_mask_separate)
    end

    # Checks if the OpenGL function *glStencilMaskSeparate* is available.
    def stencil_mask_separate?
      !get_address(106, Translations.stencil_mask_separate)
    end

    # Retrieves a `Proc` for the OpenGL function *glAttachShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def attach_shader
      get_proc(107, Translations.attach_shader, Procs.attach_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glAttachShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def attach_shader!
      self.attach_shader ||
        raise FunctionUnavailableError.new(Translations.attach_shader)
    end

    # Checks if the OpenGL function *glAttachShader* is available.
    def attach_shader?
      !get_address(107, Translations.attach_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindAttribLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_attrib_location
      get_proc(108, Translations.bind_attrib_location, Procs.bind_attrib_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindAttribLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_attrib_location!
      self.bind_attrib_location ||
        raise FunctionUnavailableError.new(Translations.bind_attrib_location)
    end

    # Checks if the OpenGL function *glBindAttribLocation* is available.
    def bind_attrib_location?
      !get_address(108, Translations.bind_attrib_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompileShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def compile_shader
      get_proc(109, Translations.compile_shader, Procs.compile_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompileShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def compile_shader!
      self.compile_shader ||
        raise FunctionUnavailableError.new(Translations.compile_shader)
    end

    # Checks if the OpenGL function *glCompileShader* is available.
    def compile_shader?
      !get_address(109, Translations.compile_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_program
      get_proc(110, Translations.create_program, Procs.create_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_program!
      self.create_program ||
        raise FunctionUnavailableError.new(Translations.create_program)
    end

    # Checks if the OpenGL function *glCreateProgram* is available.
    def create_program?
      !get_address(110, Translations.create_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_shader
      get_proc(111, Translations.create_shader, Procs.create_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_shader!
      self.create_shader ||
        raise FunctionUnavailableError.new(Translations.create_shader)
    end

    # Checks if the OpenGL function *glCreateShader* is available.
    def create_shader?
      !get_address(111, Translations.create_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_program
      get_proc(112, Translations.delete_program, Procs.delete_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_program!
      self.delete_program ||
        raise FunctionUnavailableError.new(Translations.delete_program)
    end

    # Checks if the OpenGL function *glDeleteProgram* is available.
    def delete_program?
      !get_address(112, Translations.delete_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_shader
      get_proc(113, Translations.delete_shader, Procs.delete_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_shader!
      self.delete_shader ||
        raise FunctionUnavailableError.new(Translations.delete_shader)
    end

    # Checks if the OpenGL function *glDeleteShader* is available.
    def delete_shader?
      !get_address(113, Translations.delete_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glDetachShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def detach_shader
      get_proc(114, Translations.detach_shader, Procs.detach_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glDetachShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def detach_shader!
      self.detach_shader ||
        raise FunctionUnavailableError.new(Translations.detach_shader)
    end

    # Checks if the OpenGL function *glDetachShader* is available.
    def detach_shader?
      !get_address(114, Translations.detach_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glDisableVertexAttribArray*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def disable_vertex_attrib_array
      get_proc(115, Translations.disable_vertex_attrib_array, Procs.disable_vertex_attrib_array)
    end

    # Retrieves a `Proc` for the OpenGL function *glDisableVertexAttribArray*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def disable_vertex_attrib_array!
      self.disable_vertex_attrib_array ||
        raise FunctionUnavailableError.new(Translations.disable_vertex_attrib_array)
    end

    # Checks if the OpenGL function *glDisableVertexAttribArray* is available.
    def disable_vertex_attrib_array?
      !get_address(115, Translations.disable_vertex_attrib_array)
    end

    # Retrieves a `Proc` for the OpenGL function *glEnableVertexAttribArray*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def enable_vertex_attrib_array
      get_proc(116, Translations.enable_vertex_attrib_array, Procs.enable_vertex_attrib_array)
    end

    # Retrieves a `Proc` for the OpenGL function *glEnableVertexAttribArray*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def enable_vertex_attrib_array!
      self.enable_vertex_attrib_array ||
        raise FunctionUnavailableError.new(Translations.enable_vertex_attrib_array)
    end

    # Checks if the OpenGL function *glEnableVertexAttribArray* is available.
    def enable_vertex_attrib_array?
      !get_address(116, Translations.enable_vertex_attrib_array)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveAttrib*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_active_attrib
      get_proc(117, Translations.get_active_attrib, Procs.get_active_attrib)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveAttrib*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_active_attrib!
      self.get_active_attrib ||
        raise FunctionUnavailableError.new(Translations.get_active_attrib)
    end

    # Checks if the OpenGL function *glGetActiveAttrib* is available.
    def get_active_attrib?
      !get_address(117, Translations.get_active_attrib)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveUniform*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_active_uniform
      get_proc(118, Translations.get_active_uniform, Procs.get_active_uniform)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveUniform*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_active_uniform!
      self.get_active_uniform ||
        raise FunctionUnavailableError.new(Translations.get_active_uniform)
    end

    # Checks if the OpenGL function *glGetActiveUniform* is available.
    def get_active_uniform?
      !get_address(118, Translations.get_active_uniform)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetAttachedShaders*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_attached_shaders
      get_proc(119, Translations.get_attached_shaders, Procs.get_attached_shaders)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetAttachedShaders*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_attached_shaders!
      self.get_attached_shaders ||
        raise FunctionUnavailableError.new(Translations.get_attached_shaders)
    end

    # Checks if the OpenGL function *glGetAttachedShaders* is available.
    def get_attached_shaders?
      !get_address(119, Translations.get_attached_shaders)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetAttribLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_attrib_location
      get_proc(120, Translations.get_attrib_location, Procs.get_attrib_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetAttribLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_attrib_location!
      self.get_attrib_location ||
        raise FunctionUnavailableError.new(Translations.get_attrib_location)
    end

    # Checks if the OpenGL function *glGetAttribLocation* is available.
    def get_attrib_location?
      !get_address(120, Translations.get_attrib_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_iv
      get_proc(121, Translations.get_program_iv, Procs.get_program_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_iv!
      self.get_program_iv ||
        raise FunctionUnavailableError.new(Translations.get_program_iv)
    end

    # Checks if the OpenGL function *glGetProgramiv* is available.
    def get_program_iv?
      !get_address(121, Translations.get_program_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramInfoLog*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_info_log
      get_proc(122, Translations.get_program_info_log, Procs.get_program_info_log)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramInfoLog*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_info_log!
      self.get_program_info_log ||
        raise FunctionUnavailableError.new(Translations.get_program_info_log)
    end

    # Checks if the OpenGL function *glGetProgramInfoLog* is available.
    def get_program_info_log?
      !get_address(122, Translations.get_program_info_log)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetShaderiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_shader_iv
      get_proc(123, Translations.get_shader_iv, Procs.get_shader_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetShaderiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_shader_iv!
      self.get_shader_iv ||
        raise FunctionUnavailableError.new(Translations.get_shader_iv)
    end

    # Checks if the OpenGL function *glGetShaderiv* is available.
    def get_shader_iv?
      !get_address(123, Translations.get_shader_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetShaderInfoLog*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_shader_info_log
      get_proc(124, Translations.get_shader_info_log, Procs.get_shader_info_log)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetShaderInfoLog*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_shader_info_log!
      self.get_shader_info_log ||
        raise FunctionUnavailableError.new(Translations.get_shader_info_log)
    end

    # Checks if the OpenGL function *glGetShaderInfoLog* is available.
    def get_shader_info_log?
      !get_address(124, Translations.get_shader_info_log)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetShaderSource*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_shader_source
      get_proc(125, Translations.get_shader_source, Procs.get_shader_source)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetShaderSource*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_shader_source!
      self.get_shader_source ||
        raise FunctionUnavailableError.new(Translations.get_shader_source)
    end

    # Checks if the OpenGL function *glGetShaderSource* is available.
    def get_shader_source?
      !get_address(125, Translations.get_shader_source)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_uniform_location
      get_proc(126, Translations.get_uniform_location, Procs.get_uniform_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_uniform_location!
      self.get_uniform_location ||
        raise FunctionUnavailableError.new(Translations.get_uniform_location)
    end

    # Checks if the OpenGL function *glGetUniformLocation* is available.
    def get_uniform_location?
      !get_address(126, Translations.get_uniform_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_uniform_fv
      get_proc(127, Translations.get_uniform_fv, Procs.get_uniform_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_uniform_fv!
      self.get_uniform_fv ||
        raise FunctionUnavailableError.new(Translations.get_uniform_fv)
    end

    # Checks if the OpenGL function *glGetUniformfv* is available.
    def get_uniform_fv?
      !get_address(127, Translations.get_uniform_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_uniform_iv
      get_proc(128, Translations.get_uniform_iv, Procs.get_uniform_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_uniform_iv!
      self.get_uniform_iv ||
        raise FunctionUnavailableError.new(Translations.get_uniform_iv)
    end

    # Checks if the OpenGL function *glGetUniformiv* is available.
    def get_uniform_iv?
      !get_address(128, Translations.get_uniform_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribdv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_vertex_attrib_dv
      get_proc(129, Translations.get_vertex_attrib_dv, Procs.get_vertex_attrib_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribdv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_vertex_attrib_dv!
      self.get_vertex_attrib_dv ||
        raise FunctionUnavailableError.new(Translations.get_vertex_attrib_dv)
    end

    # Checks if the OpenGL function *glGetVertexAttribdv* is available.
    def get_vertex_attrib_dv?
      !get_address(129, Translations.get_vertex_attrib_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_vertex_attrib_fv
      get_proc(130, Translations.get_vertex_attrib_fv, Procs.get_vertex_attrib_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_vertex_attrib_fv!
      self.get_vertex_attrib_fv ||
        raise FunctionUnavailableError.new(Translations.get_vertex_attrib_fv)
    end

    # Checks if the OpenGL function *glGetVertexAttribfv* is available.
    def get_vertex_attrib_fv?
      !get_address(130, Translations.get_vertex_attrib_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_vertex_attrib_iv
      get_proc(131, Translations.get_vertex_attrib_iv, Procs.get_vertex_attrib_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_vertex_attrib_iv!
      self.get_vertex_attrib_iv ||
        raise FunctionUnavailableError.new(Translations.get_vertex_attrib_iv)
    end

    # Checks if the OpenGL function *glGetVertexAttribiv* is available.
    def get_vertex_attrib_iv?
      !get_address(131, Translations.get_vertex_attrib_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribPointerv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_vertex_attrib_pointer_v
      get_proc(132, Translations.get_vertex_attrib_pointer_v, Procs.get_vertex_attrib_pointer_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribPointerv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_vertex_attrib_pointer_v!
      self.get_vertex_attrib_pointer_v ||
        raise FunctionUnavailableError.new(Translations.get_vertex_attrib_pointer_v)
    end

    # Checks if the OpenGL function *glGetVertexAttribPointerv* is available.
    def get_vertex_attrib_pointer_v?
      !get_address(132, Translations.get_vertex_attrib_pointer_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_program
      get_proc(133, Translations.is_program, Procs.is_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_program!
      self.is_program ||
        raise FunctionUnavailableError.new(Translations.is_program)
    end

    # Checks if the OpenGL function *glIsProgram* is available.
    def is_program?
      !get_address(133, Translations.is_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_shader
      get_proc(134, Translations.is_shader, Procs.is_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_shader!
      self.is_shader ||
        raise FunctionUnavailableError.new(Translations.is_shader)
    end

    # Checks if the OpenGL function *glIsShader* is available.
    def is_shader?
      !get_address(134, Translations.is_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glLinkProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def link_program
      get_proc(135, Translations.link_program, Procs.link_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glLinkProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def link_program!
      self.link_program ||
        raise FunctionUnavailableError.new(Translations.link_program)
    end

    # Checks if the OpenGL function *glLinkProgram* is available.
    def link_program?
      !get_address(135, Translations.link_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glShaderSource*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def shader_source
      get_proc(136, Translations.shader_source, Procs.shader_source)
    end

    # Retrieves a `Proc` for the OpenGL function *glShaderSource*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def shader_source!
      self.shader_source ||
        raise FunctionUnavailableError.new(Translations.shader_source)
    end

    # Checks if the OpenGL function *glShaderSource* is available.
    def shader_source?
      !get_address(136, Translations.shader_source)
    end

    # Retrieves a `Proc` for the OpenGL function *glUseProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def use_program
      get_proc(137, Translations.use_program, Procs.use_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glUseProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def use_program!
      self.use_program ||
        raise FunctionUnavailableError.new(Translations.use_program)
    end

    # Checks if the OpenGL function *glUseProgram* is available.
    def use_program?
      !get_address(137, Translations.use_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_1f
      get_proc(138, Translations.uniform_1f, Procs.uniform_1f)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_1f!
      self.uniform_1f ||
        raise FunctionUnavailableError.new(Translations.uniform_1f)
    end

    # Checks if the OpenGL function *glUniform1f* is available.
    def uniform_1f?
      !get_address(138, Translations.uniform_1f)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_2f
      get_proc(139, Translations.uniform_2f, Procs.uniform_2f)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_2f!
      self.uniform_2f ||
        raise FunctionUnavailableError.new(Translations.uniform_2f)
    end

    # Checks if the OpenGL function *glUniform2f* is available.
    def uniform_2f?
      !get_address(139, Translations.uniform_2f)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_3f
      get_proc(140, Translations.uniform_3f, Procs.uniform_3f)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_3f!
      self.uniform_3f ||
        raise FunctionUnavailableError.new(Translations.uniform_3f)
    end

    # Checks if the OpenGL function *glUniform3f* is available.
    def uniform_3f?
      !get_address(140, Translations.uniform_3f)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_4f
      get_proc(141, Translations.uniform_4f, Procs.uniform_4f)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_4f!
      self.uniform_4f ||
        raise FunctionUnavailableError.new(Translations.uniform_4f)
    end

    # Checks if the OpenGL function *glUniform4f* is available.
    def uniform_4f?
      !get_address(141, Translations.uniform_4f)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_1i
      get_proc(142, Translations.uniform_1i, Procs.uniform_1i)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_1i!
      self.uniform_1i ||
        raise FunctionUnavailableError.new(Translations.uniform_1i)
    end

    # Checks if the OpenGL function *glUniform1i* is available.
    def uniform_1i?
      !get_address(142, Translations.uniform_1i)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_2i
      get_proc(143, Translations.uniform_2i, Procs.uniform_2i)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_2i!
      self.uniform_2i ||
        raise FunctionUnavailableError.new(Translations.uniform_2i)
    end

    # Checks if the OpenGL function *glUniform2i* is available.
    def uniform_2i?
      !get_address(143, Translations.uniform_2i)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_3i
      get_proc(144, Translations.uniform_3i, Procs.uniform_3i)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_3i!
      self.uniform_3i ||
        raise FunctionUnavailableError.new(Translations.uniform_3i)
    end

    # Checks if the OpenGL function *glUniform3i* is available.
    def uniform_3i?
      !get_address(144, Translations.uniform_3i)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_4i
      get_proc(145, Translations.uniform_4i, Procs.uniform_4i)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_4i!
      self.uniform_4i ||
        raise FunctionUnavailableError.new(Translations.uniform_4i)
    end

    # Checks if the OpenGL function *glUniform4i* is available.
    def uniform_4i?
      !get_address(145, Translations.uniform_4i)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_1fv
      get_proc(146, Translations.uniform_1fv, Procs.uniform_1fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_1fv!
      self.uniform_1fv ||
        raise FunctionUnavailableError.new(Translations.uniform_1fv)
    end

    # Checks if the OpenGL function *glUniform1fv* is available.
    def uniform_1fv?
      !get_address(146, Translations.uniform_1fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_2fv
      get_proc(147, Translations.uniform_2fv, Procs.uniform_2fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_2fv!
      self.uniform_2fv ||
        raise FunctionUnavailableError.new(Translations.uniform_2fv)
    end

    # Checks if the OpenGL function *glUniform2fv* is available.
    def uniform_2fv?
      !get_address(147, Translations.uniform_2fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_3fv
      get_proc(148, Translations.uniform_3fv, Procs.uniform_3fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_3fv!
      self.uniform_3fv ||
        raise FunctionUnavailableError.new(Translations.uniform_3fv)
    end

    # Checks if the OpenGL function *glUniform3fv* is available.
    def uniform_3fv?
      !get_address(148, Translations.uniform_3fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_4fv
      get_proc(149, Translations.uniform_4fv, Procs.uniform_4fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_4fv!
      self.uniform_4fv ||
        raise FunctionUnavailableError.new(Translations.uniform_4fv)
    end

    # Checks if the OpenGL function *glUniform4fv* is available.
    def uniform_4fv?
      !get_address(149, Translations.uniform_4fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_1iv
      get_proc(150, Translations.uniform_1iv, Procs.uniform_1iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_1iv!
      self.uniform_1iv ||
        raise FunctionUnavailableError.new(Translations.uniform_1iv)
    end

    # Checks if the OpenGL function *glUniform1iv* is available.
    def uniform_1iv?
      !get_address(150, Translations.uniform_1iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_2iv
      get_proc(151, Translations.uniform_2iv, Procs.uniform_2iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_2iv!
      self.uniform_2iv ||
        raise FunctionUnavailableError.new(Translations.uniform_2iv)
    end

    # Checks if the OpenGL function *glUniform2iv* is available.
    def uniform_2iv?
      !get_address(151, Translations.uniform_2iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_3iv
      get_proc(152, Translations.uniform_3iv, Procs.uniform_3iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_3iv!
      self.uniform_3iv ||
        raise FunctionUnavailableError.new(Translations.uniform_3iv)
    end

    # Checks if the OpenGL function *glUniform3iv* is available.
    def uniform_3iv?
      !get_address(152, Translations.uniform_3iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_4iv
      get_proc(153, Translations.uniform_4iv, Procs.uniform_4iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_4iv!
      self.uniform_4iv ||
        raise FunctionUnavailableError.new(Translations.uniform_4iv)
    end

    # Checks if the OpenGL function *glUniform4iv* is available.
    def uniform_4iv?
      !get_address(153, Translations.uniform_4iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix2_fv
      get_proc(154, Translations.uniform_matrix2_fv, Procs.uniform_matrix2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix2_fv!
      self.uniform_matrix2_fv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix2_fv)
    end

    # Checks if the OpenGL function *glUniformMatrix2fv* is available.
    def uniform_matrix2_fv?
      !get_address(154, Translations.uniform_matrix2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix3_fv
      get_proc(155, Translations.uniform_matrix3_fv, Procs.uniform_matrix3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix3_fv!
      self.uniform_matrix3_fv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix3_fv)
    end

    # Checks if the OpenGL function *glUniformMatrix3fv* is available.
    def uniform_matrix3_fv?
      !get_address(155, Translations.uniform_matrix3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix4_fv
      get_proc(156, Translations.uniform_matrix4_fv, Procs.uniform_matrix4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix4_fv!
      self.uniform_matrix4_fv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix4_fv)
    end

    # Checks if the OpenGL function *glUniformMatrix4fv* is available.
    def uniform_matrix4_fv?
      !get_address(156, Translations.uniform_matrix4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glValidateProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def validate_program
      get_proc(157, Translations.validate_program, Procs.validate_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glValidateProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def validate_program!
      self.validate_program ||
        raise FunctionUnavailableError.new(Translations.validate_program)
    end

    # Checks if the OpenGL function *glValidateProgram* is available.
    def validate_program?
      !get_address(157, Translations.validate_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_1d
      get_proc(158, Translations.vertex_attrib_1d, Procs.vertex_attrib_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_1d!
      self.vertex_attrib_1d ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_1d)
    end

    # Checks if the OpenGL function *glVertexAttrib1d* is available.
    def vertex_attrib_1d?
      !get_address(158, Translations.vertex_attrib_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_1dv
      get_proc(159, Translations.vertex_attrib_1dv, Procs.vertex_attrib_1dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_1dv!
      self.vertex_attrib_1dv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_1dv)
    end

    # Checks if the OpenGL function *glVertexAttrib1dv* is available.
    def vertex_attrib_1dv?
      !get_address(159, Translations.vertex_attrib_1dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_1f
      get_proc(160, Translations.vertex_attrib_1f, Procs.vertex_attrib_1f)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_1f!
      self.vertex_attrib_1f ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_1f)
    end

    # Checks if the OpenGL function *glVertexAttrib1f* is available.
    def vertex_attrib_1f?
      !get_address(160, Translations.vertex_attrib_1f)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_1fv
      get_proc(161, Translations.vertex_attrib_1fv, Procs.vertex_attrib_1fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_1fv!
      self.vertex_attrib_1fv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_1fv)
    end

    # Checks if the OpenGL function *glVertexAttrib1fv* is available.
    def vertex_attrib_1fv?
      !get_address(161, Translations.vertex_attrib_1fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1s*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_1s
      get_proc(162, Translations.vertex_attrib_1s, Procs.vertex_attrib_1s)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1s*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_1s!
      self.vertex_attrib_1s ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_1s)
    end

    # Checks if the OpenGL function *glVertexAttrib1s* is available.
    def vertex_attrib_1s?
      !get_address(162, Translations.vertex_attrib_1s)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1sv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_1sv
      get_proc(163, Translations.vertex_attrib_1sv, Procs.vertex_attrib_1sv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib1sv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_1sv!
      self.vertex_attrib_1sv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_1sv)
    end

    # Checks if the OpenGL function *glVertexAttrib1sv* is available.
    def vertex_attrib_1sv?
      !get_address(163, Translations.vertex_attrib_1sv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_2d
      get_proc(164, Translations.vertex_attrib_2d, Procs.vertex_attrib_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_2d!
      self.vertex_attrib_2d ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_2d)
    end

    # Checks if the OpenGL function *glVertexAttrib2d* is available.
    def vertex_attrib_2d?
      !get_address(164, Translations.vertex_attrib_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_2dv
      get_proc(165, Translations.vertex_attrib_2dv, Procs.vertex_attrib_2dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_2dv!
      self.vertex_attrib_2dv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_2dv)
    end

    # Checks if the OpenGL function *glVertexAttrib2dv* is available.
    def vertex_attrib_2dv?
      !get_address(165, Translations.vertex_attrib_2dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_2f
      get_proc(166, Translations.vertex_attrib_2f, Procs.vertex_attrib_2f)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_2f!
      self.vertex_attrib_2f ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_2f)
    end

    # Checks if the OpenGL function *glVertexAttrib2f* is available.
    def vertex_attrib_2f?
      !get_address(166, Translations.vertex_attrib_2f)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_2fv
      get_proc(167, Translations.vertex_attrib_2fv, Procs.vertex_attrib_2fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_2fv!
      self.vertex_attrib_2fv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_2fv)
    end

    # Checks if the OpenGL function *glVertexAttrib2fv* is available.
    def vertex_attrib_2fv?
      !get_address(167, Translations.vertex_attrib_2fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2s*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_2s
      get_proc(168, Translations.vertex_attrib_2s, Procs.vertex_attrib_2s)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2s*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_2s!
      self.vertex_attrib_2s ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_2s)
    end

    # Checks if the OpenGL function *glVertexAttrib2s* is available.
    def vertex_attrib_2s?
      !get_address(168, Translations.vertex_attrib_2s)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2sv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_2sv
      get_proc(169, Translations.vertex_attrib_2sv, Procs.vertex_attrib_2sv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib2sv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_2sv!
      self.vertex_attrib_2sv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_2sv)
    end

    # Checks if the OpenGL function *glVertexAttrib2sv* is available.
    def vertex_attrib_2sv?
      !get_address(169, Translations.vertex_attrib_2sv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_3d
      get_proc(170, Translations.vertex_attrib_3d, Procs.vertex_attrib_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_3d!
      self.vertex_attrib_3d ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_3d)
    end

    # Checks if the OpenGL function *glVertexAttrib3d* is available.
    def vertex_attrib_3d?
      !get_address(170, Translations.vertex_attrib_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_3dv
      get_proc(171, Translations.vertex_attrib_3dv, Procs.vertex_attrib_3dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_3dv!
      self.vertex_attrib_3dv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_3dv)
    end

    # Checks if the OpenGL function *glVertexAttrib3dv* is available.
    def vertex_attrib_3dv?
      !get_address(171, Translations.vertex_attrib_3dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_3f
      get_proc(172, Translations.vertex_attrib_3f, Procs.vertex_attrib_3f)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_3f!
      self.vertex_attrib_3f ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_3f)
    end

    # Checks if the OpenGL function *glVertexAttrib3f* is available.
    def vertex_attrib_3f?
      !get_address(172, Translations.vertex_attrib_3f)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_3fv
      get_proc(173, Translations.vertex_attrib_3fv, Procs.vertex_attrib_3fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_3fv!
      self.vertex_attrib_3fv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_3fv)
    end

    # Checks if the OpenGL function *glVertexAttrib3fv* is available.
    def vertex_attrib_3fv?
      !get_address(173, Translations.vertex_attrib_3fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3s*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_3s
      get_proc(174, Translations.vertex_attrib_3s, Procs.vertex_attrib_3s)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3s*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_3s!
      self.vertex_attrib_3s ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_3s)
    end

    # Checks if the OpenGL function *glVertexAttrib3s* is available.
    def vertex_attrib_3s?
      !get_address(174, Translations.vertex_attrib_3s)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3sv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_3sv
      get_proc(175, Translations.vertex_attrib_3sv, Procs.vertex_attrib_3sv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib3sv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_3sv!
      self.vertex_attrib_3sv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_3sv)
    end

    # Checks if the OpenGL function *glVertexAttrib3sv* is available.
    def vertex_attrib_3sv?
      !get_address(175, Translations.vertex_attrib_3sv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nbv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4nbv
      get_proc(176, Translations.vertex_attrib_4nbv, Procs.vertex_attrib_4nbv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nbv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4nbv!
      self.vertex_attrib_4nbv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4nbv)
    end

    # Checks if the OpenGL function *glVertexAttrib4Nbv* is available.
    def vertex_attrib_4nbv?
      !get_address(176, Translations.vertex_attrib_4nbv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Niv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4niv
      get_proc(177, Translations.vertex_attrib_4niv, Procs.vertex_attrib_4niv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Niv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4niv!
      self.vertex_attrib_4niv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4niv)
    end

    # Checks if the OpenGL function *glVertexAttrib4Niv* is available.
    def vertex_attrib_4niv?
      !get_address(177, Translations.vertex_attrib_4niv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nsv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4nsv
      get_proc(178, Translations.vertex_attrib_4nsv, Procs.vertex_attrib_4nsv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nsv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4nsv!
      self.vertex_attrib_4nsv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4nsv)
    end

    # Checks if the OpenGL function *glVertexAttrib4Nsv* is available.
    def vertex_attrib_4nsv?
      !get_address(178, Translations.vertex_attrib_4nsv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nub*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4nub
      get_proc(179, Translations.vertex_attrib_4nub, Procs.vertex_attrib_4nub)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nub*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4nub!
      self.vertex_attrib_4nub ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4nub)
    end

    # Checks if the OpenGL function *glVertexAttrib4Nub* is available.
    def vertex_attrib_4nub?
      !get_address(179, Translations.vertex_attrib_4nub)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nubv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4nubv
      get_proc(180, Translations.vertex_attrib_4nubv, Procs.vertex_attrib_4nubv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nubv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4nubv!
      self.vertex_attrib_4nubv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4nubv)
    end

    # Checks if the OpenGL function *glVertexAttrib4Nubv* is available.
    def vertex_attrib_4nubv?
      !get_address(180, Translations.vertex_attrib_4nubv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4nuiv
      get_proc(181, Translations.vertex_attrib_4nuiv, Procs.vertex_attrib_4nuiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4nuiv!
      self.vertex_attrib_4nuiv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4nuiv)
    end

    # Checks if the OpenGL function *glVertexAttrib4Nuiv* is available.
    def vertex_attrib_4nuiv?
      !get_address(181, Translations.vertex_attrib_4nuiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nusv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4nusv
      get_proc(182, Translations.vertex_attrib_4nusv, Procs.vertex_attrib_4nusv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4Nusv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4nusv!
      self.vertex_attrib_4nusv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4nusv)
    end

    # Checks if the OpenGL function *glVertexAttrib4Nusv* is available.
    def vertex_attrib_4nusv?
      !get_address(182, Translations.vertex_attrib_4nusv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4bv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4bv
      get_proc(183, Translations.vertex_attrib_4bv, Procs.vertex_attrib_4bv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4bv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4bv!
      self.vertex_attrib_4bv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4bv)
    end

    # Checks if the OpenGL function *glVertexAttrib4bv* is available.
    def vertex_attrib_4bv?
      !get_address(183, Translations.vertex_attrib_4bv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4d
      get_proc(184, Translations.vertex_attrib_4d, Procs.vertex_attrib_4d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4d!
      self.vertex_attrib_4d ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4d)
    end

    # Checks if the OpenGL function *glVertexAttrib4d* is available.
    def vertex_attrib_4d?
      !get_address(184, Translations.vertex_attrib_4d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4dv
      get_proc(185, Translations.vertex_attrib_4dv, Procs.vertex_attrib_4dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4dv!
      self.vertex_attrib_4dv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4dv)
    end

    # Checks if the OpenGL function *glVertexAttrib4dv* is available.
    def vertex_attrib_4dv?
      !get_address(185, Translations.vertex_attrib_4dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4f
      get_proc(186, Translations.vertex_attrib_4f, Procs.vertex_attrib_4f)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4f!
      self.vertex_attrib_4f ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4f)
    end

    # Checks if the OpenGL function *glVertexAttrib4f* is available.
    def vertex_attrib_4f?
      !get_address(186, Translations.vertex_attrib_4f)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4fv
      get_proc(187, Translations.vertex_attrib_4fv, Procs.vertex_attrib_4fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4fv!
      self.vertex_attrib_4fv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4fv)
    end

    # Checks if the OpenGL function *glVertexAttrib4fv* is available.
    def vertex_attrib_4fv?
      !get_address(187, Translations.vertex_attrib_4fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4iv
      get_proc(188, Translations.vertex_attrib_4iv, Procs.vertex_attrib_4iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4iv!
      self.vertex_attrib_4iv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4iv)
    end

    # Checks if the OpenGL function *glVertexAttrib4iv* is available.
    def vertex_attrib_4iv?
      !get_address(188, Translations.vertex_attrib_4iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4s*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4s
      get_proc(189, Translations.vertex_attrib_4s, Procs.vertex_attrib_4s)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4s*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4s!
      self.vertex_attrib_4s ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4s)
    end

    # Checks if the OpenGL function *glVertexAttrib4s* is available.
    def vertex_attrib_4s?
      !get_address(189, Translations.vertex_attrib_4s)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4sv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4sv
      get_proc(190, Translations.vertex_attrib_4sv, Procs.vertex_attrib_4sv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4sv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4sv!
      self.vertex_attrib_4sv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4sv)
    end

    # Checks if the OpenGL function *glVertexAttrib4sv* is available.
    def vertex_attrib_4sv?
      !get_address(190, Translations.vertex_attrib_4sv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4ubv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4ubv
      get_proc(191, Translations.vertex_attrib_4ubv, Procs.vertex_attrib_4ubv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4ubv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4ubv!
      self.vertex_attrib_4ubv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4ubv)
    end

    # Checks if the OpenGL function *glVertexAttrib4ubv* is available.
    def vertex_attrib_4ubv?
      !get_address(191, Translations.vertex_attrib_4ubv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4uiv
      get_proc(192, Translations.vertex_attrib_4uiv, Procs.vertex_attrib_4uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4uiv!
      self.vertex_attrib_4uiv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4uiv)
    end

    # Checks if the OpenGL function *glVertexAttrib4uiv* is available.
    def vertex_attrib_4uiv?
      !get_address(192, Translations.vertex_attrib_4uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4usv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_4usv
      get_proc(193, Translations.vertex_attrib_4usv, Procs.vertex_attrib_4usv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttrib4usv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_4usv!
      self.vertex_attrib_4usv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_4usv)
    end

    # Checks if the OpenGL function *glVertexAttrib4usv* is available.
    def vertex_attrib_4usv?
      !get_address(193, Translations.vertex_attrib_4usv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribPointer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_pointer
      get_proc(194, Translations.vertex_attrib_pointer, Procs.vertex_attrib_pointer)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribPointer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_pointer!
      self.vertex_attrib_pointer ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_pointer)
    end

    # Checks if the OpenGL function *glVertexAttribPointer* is available.
    def vertex_attrib_pointer?
      !get_address(194, Translations.vertex_attrib_pointer)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2x3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix2x3_fv
      get_proc(195, Translations.uniform_matrix2x3_fv, Procs.uniform_matrix2x3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2x3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix2x3_fv!
      self.uniform_matrix2x3_fv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix2x3_fv)
    end

    # Checks if the OpenGL function *glUniformMatrix2x3fv* is available.
    def uniform_matrix2x3_fv?
      !get_address(195, Translations.uniform_matrix2x3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3x2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix3x2_fv
      get_proc(196, Translations.uniform_matrix3x2_fv, Procs.uniform_matrix3x2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3x2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix3x2_fv!
      self.uniform_matrix3x2_fv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix3x2_fv)
    end

    # Checks if the OpenGL function *glUniformMatrix3x2fv* is available.
    def uniform_matrix3x2_fv?
      !get_address(196, Translations.uniform_matrix3x2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2x4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix2x4_fv
      get_proc(197, Translations.uniform_matrix2x4_fv, Procs.uniform_matrix2x4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2x4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix2x4_fv!
      self.uniform_matrix2x4_fv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix2x4_fv)
    end

    # Checks if the OpenGL function *glUniformMatrix2x4fv* is available.
    def uniform_matrix2x4_fv?
      !get_address(197, Translations.uniform_matrix2x4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4x2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix4x2_fv
      get_proc(198, Translations.uniform_matrix4x2_fv, Procs.uniform_matrix4x2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4x2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix4x2_fv!
      self.uniform_matrix4x2_fv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix4x2_fv)
    end

    # Checks if the OpenGL function *glUniformMatrix4x2fv* is available.
    def uniform_matrix4x2_fv?
      !get_address(198, Translations.uniform_matrix4x2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3x4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix3x4_fv
      get_proc(199, Translations.uniform_matrix3x4_fv, Procs.uniform_matrix3x4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3x4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix3x4_fv!
      self.uniform_matrix3x4_fv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix3x4_fv)
    end

    # Checks if the OpenGL function *glUniformMatrix3x4fv* is available.
    def uniform_matrix3x4_fv?
      !get_address(199, Translations.uniform_matrix3x4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4x3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix4x3_fv
      get_proc(200, Translations.uniform_matrix4x3_fv, Procs.uniform_matrix4x3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4x3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix4x3_fv!
      self.uniform_matrix4x3_fv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix4x3_fv)
    end

    # Checks if the OpenGL function *glUniformMatrix4x3fv* is available.
    def uniform_matrix4x3_fv?
      !get_address(200, Translations.uniform_matrix4x3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glColorMaski*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def color_mask_i
      get_proc(201, Translations.color_mask_i, Procs.color_mask_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glColorMaski*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def color_mask_i!
      self.color_mask_i ||
        raise FunctionUnavailableError.new(Translations.color_mask_i)
    end

    # Checks if the OpenGL function *glColorMaski* is available.
    def color_mask_i?
      !get_address(201, Translations.color_mask_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBooleani_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_boolean_i_v
      get_proc(202, Translations.get_boolean_i_v, Procs.get_boolean_i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBooleani_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_boolean_i_v!
      self.get_boolean_i_v ||
        raise FunctionUnavailableError.new(Translations.get_boolean_i_v)
    end

    # Checks if the OpenGL function *glGetBooleani_v* is available.
    def get_boolean_i_v?
      !get_address(202, Translations.get_boolean_i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetIntegeri_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_integer_i_v
      get_proc(203, Translations.get_integer_i_v, Procs.get_integer_i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetIntegeri_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_integer_i_v!
      self.get_integer_i_v ||
        raise FunctionUnavailableError.new(Translations.get_integer_i_v)
    end

    # Checks if the OpenGL function *glGetIntegeri_v* is available.
    def get_integer_i_v?
      !get_address(203, Translations.get_integer_i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glEnablei*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def enable_i
      get_proc(204, Translations.enable_i, Procs.enable_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glEnablei*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def enable_i!
      self.enable_i ||
        raise FunctionUnavailableError.new(Translations.enable_i)
    end

    # Checks if the OpenGL function *glEnablei* is available.
    def enable_i?
      !get_address(204, Translations.enable_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glDisablei*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def disable_i
      get_proc(205, Translations.disable_i, Procs.disable_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glDisablei*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def disable_i!
      self.disable_i ||
        raise FunctionUnavailableError.new(Translations.disable_i)
    end

    # Checks if the OpenGL function *glDisablei* is available.
    def disable_i?
      !get_address(205, Translations.disable_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsEnabledi*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_enabled_i
      get_proc(206, Translations.is_enabled_i, Procs.is_enabled_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsEnabledi*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_enabled_i!
      self.is_enabled_i ||
        raise FunctionUnavailableError.new(Translations.is_enabled_i)
    end

    # Checks if the OpenGL function *glIsEnabledi* is available.
    def is_enabled_i?
      !get_address(206, Translations.is_enabled_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glBeginTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def begin_transform_feedback
      get_proc(207, Translations.begin_transform_feedback, Procs.begin_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glBeginTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def begin_transform_feedback!
      self.begin_transform_feedback ||
        raise FunctionUnavailableError.new(Translations.begin_transform_feedback)
    end

    # Checks if the OpenGL function *glBeginTransformFeedback* is available.
    def begin_transform_feedback?
      !get_address(207, Translations.begin_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glEndTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def end_transform_feedback
      get_proc(208, Translations.end_transform_feedback, Procs.end_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glEndTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def end_transform_feedback!
      self.end_transform_feedback ||
        raise FunctionUnavailableError.new(Translations.end_transform_feedback)
    end

    # Checks if the OpenGL function *glEndTransformFeedback* is available.
    def end_transform_feedback?
      !get_address(208, Translations.end_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_buffer_range
      get_proc(209, Translations.bind_buffer_range, Procs.bind_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_buffer_range!
      self.bind_buffer_range ||
        raise FunctionUnavailableError.new(Translations.bind_buffer_range)
    end

    # Checks if the OpenGL function *glBindBufferRange* is available.
    def bind_buffer_range?
      !get_address(209, Translations.bind_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindBufferBase*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_buffer_base
      get_proc(210, Translations.bind_buffer_base, Procs.bind_buffer_base)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindBufferBase*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_buffer_base!
      self.bind_buffer_base ||
        raise FunctionUnavailableError.new(Translations.bind_buffer_base)
    end

    # Checks if the OpenGL function *glBindBufferBase* is available.
    def bind_buffer_base?
      !get_address(210, Translations.bind_buffer_base)
    end

    # Retrieves a `Proc` for the OpenGL function *glTransformFeedbackVaryings*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def transform_feedback_varyings
      get_proc(211, Translations.transform_feedback_varyings, Procs.transform_feedback_varyings)
    end

    # Retrieves a `Proc` for the OpenGL function *glTransformFeedbackVaryings*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def transform_feedback_varyings!
      self.transform_feedback_varyings ||
        raise FunctionUnavailableError.new(Translations.transform_feedback_varyings)
    end

    # Checks if the OpenGL function *glTransformFeedbackVaryings* is available.
    def transform_feedback_varyings?
      !get_address(211, Translations.transform_feedback_varyings)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTransformFeedbackVarying*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_transform_feedback_varying
      get_proc(212, Translations.get_transform_feedback_varying, Procs.get_transform_feedback_varying)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTransformFeedbackVarying*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_transform_feedback_varying!
      self.get_transform_feedback_varying ||
        raise FunctionUnavailableError.new(Translations.get_transform_feedback_varying)
    end

    # Checks if the OpenGL function *glGetTransformFeedbackVarying* is available.
    def get_transform_feedback_varying?
      !get_address(212, Translations.get_transform_feedback_varying)
    end

    # Retrieves a `Proc` for the OpenGL function *glClampColor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clamp_color
      get_proc(213, Translations.clamp_color, Procs.clamp_color)
    end

    # Retrieves a `Proc` for the OpenGL function *glClampColor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clamp_color!
      self.clamp_color ||
        raise FunctionUnavailableError.new(Translations.clamp_color)
    end

    # Checks if the OpenGL function *glClampColor* is available.
    def clamp_color?
      !get_address(213, Translations.clamp_color)
    end

    # Retrieves a `Proc` for the OpenGL function *glBeginConditionalRender*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def begin_conditional_render
      get_proc(214, Translations.begin_conditional_render, Procs.begin_conditional_render)
    end

    # Retrieves a `Proc` for the OpenGL function *glBeginConditionalRender*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def begin_conditional_render!
      self.begin_conditional_render ||
        raise FunctionUnavailableError.new(Translations.begin_conditional_render)
    end

    # Checks if the OpenGL function *glBeginConditionalRender* is available.
    def begin_conditional_render?
      !get_address(214, Translations.begin_conditional_render)
    end

    # Retrieves a `Proc` for the OpenGL function *glEndConditionalRender*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def end_conditional_render
      get_proc(215, Translations.end_conditional_render, Procs.end_conditional_render)
    end

    # Retrieves a `Proc` for the OpenGL function *glEndConditionalRender*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def end_conditional_render!
      self.end_conditional_render ||
        raise FunctionUnavailableError.new(Translations.end_conditional_render)
    end

    # Checks if the OpenGL function *glEndConditionalRender* is available.
    def end_conditional_render?
      !get_address(215, Translations.end_conditional_render)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribIPointer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_pointer
      get_proc(216, Translations.vertex_attrib_i_pointer, Procs.vertex_attrib_i_pointer)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribIPointer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_pointer!
      self.vertex_attrib_i_pointer ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_pointer)
    end

    # Checks if the OpenGL function *glVertexAttribIPointer* is available.
    def vertex_attrib_i_pointer?
      !get_address(216, Translations.vertex_attrib_i_pointer)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_vertex_attrib_i_iv
      get_proc(217, Translations.get_vertex_attrib_i_iv, Procs.get_vertex_attrib_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_vertex_attrib_i_iv!
      self.get_vertex_attrib_i_iv ||
        raise FunctionUnavailableError.new(Translations.get_vertex_attrib_i_iv)
    end

    # Checks if the OpenGL function *glGetVertexAttribIiv* is available.
    def get_vertex_attrib_i_iv?
      !get_address(217, Translations.get_vertex_attrib_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_vertex_attrib_i_uiv
      get_proc(218, Translations.get_vertex_attrib_i_uiv, Procs.get_vertex_attrib_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_vertex_attrib_i_uiv!
      self.get_vertex_attrib_i_uiv ||
        raise FunctionUnavailableError.new(Translations.get_vertex_attrib_i_uiv)
    end

    # Checks if the OpenGL function *glGetVertexAttribIuiv* is available.
    def get_vertex_attrib_i_uiv?
      !get_address(218, Translations.get_vertex_attrib_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI1i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_1i
      get_proc(219, Translations.vertex_attrib_i_1i, Procs.vertex_attrib_i_1i)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI1i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_1i!
      self.vertex_attrib_i_1i ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_1i)
    end

    # Checks if the OpenGL function *glVertexAttribI1i* is available.
    def vertex_attrib_i_1i?
      !get_address(219, Translations.vertex_attrib_i_1i)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI2i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_2i
      get_proc(220, Translations.vertex_attrib_i_2i, Procs.vertex_attrib_i_2i)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI2i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_2i!
      self.vertex_attrib_i_2i ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_2i)
    end

    # Checks if the OpenGL function *glVertexAttribI2i* is available.
    def vertex_attrib_i_2i?
      !get_address(220, Translations.vertex_attrib_i_2i)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI3i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_3i
      get_proc(221, Translations.vertex_attrib_i_3i, Procs.vertex_attrib_i_3i)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI3i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_3i!
      self.vertex_attrib_i_3i ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_3i)
    end

    # Checks if the OpenGL function *glVertexAttribI3i* is available.
    def vertex_attrib_i_3i?
      !get_address(221, Translations.vertex_attrib_i_3i)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_4i
      get_proc(222, Translations.vertex_attrib_i_4i, Procs.vertex_attrib_i_4i)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_4i!
      self.vertex_attrib_i_4i ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_4i)
    end

    # Checks if the OpenGL function *glVertexAttribI4i* is available.
    def vertex_attrib_i_4i?
      !get_address(222, Translations.vertex_attrib_i_4i)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI1ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_1ui
      get_proc(223, Translations.vertex_attrib_i_1ui, Procs.vertex_attrib_i_1ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI1ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_1ui!
      self.vertex_attrib_i_1ui ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_1ui)
    end

    # Checks if the OpenGL function *glVertexAttribI1ui* is available.
    def vertex_attrib_i_1ui?
      !get_address(223, Translations.vertex_attrib_i_1ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI2ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_2ui
      get_proc(224, Translations.vertex_attrib_i_2ui, Procs.vertex_attrib_i_2ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI2ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_2ui!
      self.vertex_attrib_i_2ui ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_2ui)
    end

    # Checks if the OpenGL function *glVertexAttribI2ui* is available.
    def vertex_attrib_i_2ui?
      !get_address(224, Translations.vertex_attrib_i_2ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI3ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_3ui
      get_proc(225, Translations.vertex_attrib_i_3ui, Procs.vertex_attrib_i_3ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI3ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_3ui!
      self.vertex_attrib_i_3ui ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_3ui)
    end

    # Checks if the OpenGL function *glVertexAttribI3ui* is available.
    def vertex_attrib_i_3ui?
      !get_address(225, Translations.vertex_attrib_i_3ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_4ui
      get_proc(226, Translations.vertex_attrib_i_4ui, Procs.vertex_attrib_i_4ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_4ui!
      self.vertex_attrib_i_4ui ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_4ui)
    end

    # Checks if the OpenGL function *glVertexAttribI4ui* is available.
    def vertex_attrib_i_4ui?
      !get_address(226, Translations.vertex_attrib_i_4ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI1iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_1iv
      get_proc(227, Translations.vertex_attrib_i_1iv, Procs.vertex_attrib_i_1iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI1iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_1iv!
      self.vertex_attrib_i_1iv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_1iv)
    end

    # Checks if the OpenGL function *glVertexAttribI1iv* is available.
    def vertex_attrib_i_1iv?
      !get_address(227, Translations.vertex_attrib_i_1iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI2iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_2iv
      get_proc(228, Translations.vertex_attrib_i_2iv, Procs.vertex_attrib_i_2iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI2iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_2iv!
      self.vertex_attrib_i_2iv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_2iv)
    end

    # Checks if the OpenGL function *glVertexAttribI2iv* is available.
    def vertex_attrib_i_2iv?
      !get_address(228, Translations.vertex_attrib_i_2iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI3iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_3iv
      get_proc(229, Translations.vertex_attrib_i_3iv, Procs.vertex_attrib_i_3iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI3iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_3iv!
      self.vertex_attrib_i_3iv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_3iv)
    end

    # Checks if the OpenGL function *glVertexAttribI3iv* is available.
    def vertex_attrib_i_3iv?
      !get_address(229, Translations.vertex_attrib_i_3iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_4iv
      get_proc(230, Translations.vertex_attrib_i_4iv, Procs.vertex_attrib_i_4iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_4iv!
      self.vertex_attrib_i_4iv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_4iv)
    end

    # Checks if the OpenGL function *glVertexAttribI4iv* is available.
    def vertex_attrib_i_4iv?
      !get_address(230, Translations.vertex_attrib_i_4iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI1uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_1uiv
      get_proc(231, Translations.vertex_attrib_i_1uiv, Procs.vertex_attrib_i_1uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI1uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_1uiv!
      self.vertex_attrib_i_1uiv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_1uiv)
    end

    # Checks if the OpenGL function *glVertexAttribI1uiv* is available.
    def vertex_attrib_i_1uiv?
      !get_address(231, Translations.vertex_attrib_i_1uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI2uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_2uiv
      get_proc(232, Translations.vertex_attrib_i_2uiv, Procs.vertex_attrib_i_2uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI2uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_2uiv!
      self.vertex_attrib_i_2uiv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_2uiv)
    end

    # Checks if the OpenGL function *glVertexAttribI2uiv* is available.
    def vertex_attrib_i_2uiv?
      !get_address(232, Translations.vertex_attrib_i_2uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI3uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_3uiv
      get_proc(233, Translations.vertex_attrib_i_3uiv, Procs.vertex_attrib_i_3uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI3uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_3uiv!
      self.vertex_attrib_i_3uiv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_3uiv)
    end

    # Checks if the OpenGL function *glVertexAttribI3uiv* is available.
    def vertex_attrib_i_3uiv?
      !get_address(233, Translations.vertex_attrib_i_3uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_4uiv
      get_proc(234, Translations.vertex_attrib_i_4uiv, Procs.vertex_attrib_i_4uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_4uiv!
      self.vertex_attrib_i_4uiv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_4uiv)
    end

    # Checks if the OpenGL function *glVertexAttribI4uiv* is available.
    def vertex_attrib_i_4uiv?
      !get_address(234, Translations.vertex_attrib_i_4uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4bv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_4bv
      get_proc(235, Translations.vertex_attrib_i_4bv, Procs.vertex_attrib_i_4bv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4bv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_4bv!
      self.vertex_attrib_i_4bv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_4bv)
    end

    # Checks if the OpenGL function *glVertexAttribI4bv* is available.
    def vertex_attrib_i_4bv?
      !get_address(235, Translations.vertex_attrib_i_4bv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4sv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_4sv
      get_proc(236, Translations.vertex_attrib_i_4sv, Procs.vertex_attrib_i_4sv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4sv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_4sv!
      self.vertex_attrib_i_4sv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_4sv)
    end

    # Checks if the OpenGL function *glVertexAttribI4sv* is available.
    def vertex_attrib_i_4sv?
      !get_address(236, Translations.vertex_attrib_i_4sv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4ubv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_4ubv
      get_proc(237, Translations.vertex_attrib_i_4ubv, Procs.vertex_attrib_i_4ubv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4ubv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_4ubv!
      self.vertex_attrib_i_4ubv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_4ubv)
    end

    # Checks if the OpenGL function *glVertexAttribI4ubv* is available.
    def vertex_attrib_i_4ubv?
      !get_address(237, Translations.vertex_attrib_i_4ubv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4usv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_4usv
      get_proc(238, Translations.vertex_attrib_i_4usv, Procs.vertex_attrib_i_4usv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribI4usv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_4usv!
      self.vertex_attrib_i_4usv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_4usv)
    end

    # Checks if the OpenGL function *glVertexAttribI4usv* is available.
    def vertex_attrib_i_4usv?
      !get_address(238, Translations.vertex_attrib_i_4usv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_uniform_uiv
      get_proc(239, Translations.get_uniform_uiv, Procs.get_uniform_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_uniform_uiv!
      self.get_uniform_uiv ||
        raise FunctionUnavailableError.new(Translations.get_uniform_uiv)
    end

    # Checks if the OpenGL function *glGetUniformuiv* is available.
    def get_uniform_uiv?
      !get_address(239, Translations.get_uniform_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindFragDataLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_frag_data_location
      get_proc(240, Translations.bind_frag_data_location, Procs.bind_frag_data_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindFragDataLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_frag_data_location!
      self.bind_frag_data_location ||
        raise FunctionUnavailableError.new(Translations.bind_frag_data_location)
    end

    # Checks if the OpenGL function *glBindFragDataLocation* is available.
    def bind_frag_data_location?
      !get_address(240, Translations.bind_frag_data_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFragDataLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_frag_data_location
      get_proc(241, Translations.get_frag_data_location, Procs.get_frag_data_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFragDataLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_frag_data_location!
      self.get_frag_data_location ||
        raise FunctionUnavailableError.new(Translations.get_frag_data_location)
    end

    # Checks if the OpenGL function *glGetFragDataLocation* is available.
    def get_frag_data_location?
      !get_address(241, Translations.get_frag_data_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_1ui
      get_proc(242, Translations.uniform_1ui, Procs.uniform_1ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_1ui!
      self.uniform_1ui ||
        raise FunctionUnavailableError.new(Translations.uniform_1ui)
    end

    # Checks if the OpenGL function *glUniform1ui* is available.
    def uniform_1ui?
      !get_address(242, Translations.uniform_1ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_2ui
      get_proc(243, Translations.uniform_2ui, Procs.uniform_2ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_2ui!
      self.uniform_2ui ||
        raise FunctionUnavailableError.new(Translations.uniform_2ui)
    end

    # Checks if the OpenGL function *glUniform2ui* is available.
    def uniform_2ui?
      !get_address(243, Translations.uniform_2ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_3ui
      get_proc(244, Translations.uniform_3ui, Procs.uniform_3ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_3ui!
      self.uniform_3ui ||
        raise FunctionUnavailableError.new(Translations.uniform_3ui)
    end

    # Checks if the OpenGL function *glUniform3ui* is available.
    def uniform_3ui?
      !get_address(244, Translations.uniform_3ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_4ui
      get_proc(245, Translations.uniform_4ui, Procs.uniform_4ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_4ui!
      self.uniform_4ui ||
        raise FunctionUnavailableError.new(Translations.uniform_4ui)
    end

    # Checks if the OpenGL function *glUniform4ui* is available.
    def uniform_4ui?
      !get_address(245, Translations.uniform_4ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_1uiv
      get_proc(246, Translations.uniform_1uiv, Procs.uniform_1uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_1uiv!
      self.uniform_1uiv ||
        raise FunctionUnavailableError.new(Translations.uniform_1uiv)
    end

    # Checks if the OpenGL function *glUniform1uiv* is available.
    def uniform_1uiv?
      !get_address(246, Translations.uniform_1uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_2uiv
      get_proc(247, Translations.uniform_2uiv, Procs.uniform_2uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_2uiv!
      self.uniform_2uiv ||
        raise FunctionUnavailableError.new(Translations.uniform_2uiv)
    end

    # Checks if the OpenGL function *glUniform2uiv* is available.
    def uniform_2uiv?
      !get_address(247, Translations.uniform_2uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_3uiv
      get_proc(248, Translations.uniform_3uiv, Procs.uniform_3uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_3uiv!
      self.uniform_3uiv ||
        raise FunctionUnavailableError.new(Translations.uniform_3uiv)
    end

    # Checks if the OpenGL function *glUniform3uiv* is available.
    def uniform_3uiv?
      !get_address(248, Translations.uniform_3uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_4uiv
      get_proc(249, Translations.uniform_4uiv, Procs.uniform_4uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_4uiv!
      self.uniform_4uiv ||
        raise FunctionUnavailableError.new(Translations.uniform_4uiv)
    end

    # Checks if the OpenGL function *glUniform4uiv* is available.
    def uniform_4uiv?
      !get_address(249, Translations.uniform_4uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_parameter_i_iv
      get_proc(250, Translations.tex_parameter_i_iv, Procs.tex_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_parameter_i_iv!
      self.tex_parameter_i_iv ||
        raise FunctionUnavailableError.new(Translations.tex_parameter_i_iv)
    end

    # Checks if the OpenGL function *glTexParameterIiv* is available.
    def tex_parameter_i_iv?
      !get_address(250, Translations.tex_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_parameter_i_uiv
      get_proc(251, Translations.tex_parameter_i_uiv, Procs.tex_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_parameter_i_uiv!
      self.tex_parameter_i_uiv ||
        raise FunctionUnavailableError.new(Translations.tex_parameter_i_uiv)
    end

    # Checks if the OpenGL function *glTexParameterIuiv* is available.
    def tex_parameter_i_uiv?
      !get_address(251, Translations.tex_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_tex_parameter_i_iv
      get_proc(252, Translations.get_tex_parameter_i_iv, Procs.get_tex_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_tex_parameter_i_iv!
      self.get_tex_parameter_i_iv ||
        raise FunctionUnavailableError.new(Translations.get_tex_parameter_i_iv)
    end

    # Checks if the OpenGL function *glGetTexParameterIiv* is available.
    def get_tex_parameter_i_iv?
      !get_address(252, Translations.get_tex_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_tex_parameter_i_uiv
      get_proc(253, Translations.get_tex_parameter_i_uiv, Procs.get_tex_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTexParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_tex_parameter_i_uiv!
      self.get_tex_parameter_i_uiv ||
        raise FunctionUnavailableError.new(Translations.get_tex_parameter_i_uiv)
    end

    # Checks if the OpenGL function *glGetTexParameterIuiv* is available.
    def get_tex_parameter_i_uiv?
      !get_address(253, Translations.get_tex_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_buffer_iv
      get_proc(254, Translations.clear_buffer_iv, Procs.clear_buffer_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_buffer_iv!
      self.clear_buffer_iv ||
        raise FunctionUnavailableError.new(Translations.clear_buffer_iv)
    end

    # Checks if the OpenGL function *glClearBufferiv* is available.
    def clear_buffer_iv?
      !get_address(254, Translations.clear_buffer_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_buffer_uiv
      get_proc(255, Translations.clear_buffer_uiv, Procs.clear_buffer_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_buffer_uiv!
      self.clear_buffer_uiv ||
        raise FunctionUnavailableError.new(Translations.clear_buffer_uiv)
    end

    # Checks if the OpenGL function *glClearBufferuiv* is available.
    def clear_buffer_uiv?
      !get_address(255, Translations.clear_buffer_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_buffer_fv
      get_proc(256, Translations.clear_buffer_fv, Procs.clear_buffer_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_buffer_fv!
      self.clear_buffer_fv ||
        raise FunctionUnavailableError.new(Translations.clear_buffer_fv)
    end

    # Checks if the OpenGL function *glClearBufferfv* is available.
    def clear_buffer_fv?
      !get_address(256, Translations.clear_buffer_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferfi*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_buffer_fi
      get_proc(257, Translations.clear_buffer_fi, Procs.clear_buffer_fi)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferfi*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_buffer_fi!
      self.clear_buffer_fi ||
        raise FunctionUnavailableError.new(Translations.clear_buffer_fi)
    end

    # Checks if the OpenGL function *glClearBufferfi* is available.
    def clear_buffer_fi?
      !get_address(257, Translations.clear_buffer_fi)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetStringi*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_string_i
      get_proc(258, Translations.get_string_i, Procs.get_string_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetStringi*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_string_i!
      self.get_string_i ||
        raise FunctionUnavailableError.new(Translations.get_string_i)
    end

    # Checks if the OpenGL function *glGetStringi* is available.
    def get_string_i?
      !get_address(258, Translations.get_string_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsRenderbuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_renderbuffer
      get_proc(259, Translations.is_renderbuffer, Procs.is_renderbuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsRenderbuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_renderbuffer!
      self.is_renderbuffer ||
        raise FunctionUnavailableError.new(Translations.is_renderbuffer)
    end

    # Checks if the OpenGL function *glIsRenderbuffer* is available.
    def is_renderbuffer?
      !get_address(259, Translations.is_renderbuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindRenderbuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_renderbuffer
      get_proc(260, Translations.bind_renderbuffer, Procs.bind_renderbuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindRenderbuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_renderbuffer!
      self.bind_renderbuffer ||
        raise FunctionUnavailableError.new(Translations.bind_renderbuffer)
    end

    # Checks if the OpenGL function *glBindRenderbuffer* is available.
    def bind_renderbuffer?
      !get_address(260, Translations.bind_renderbuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteRenderbuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_renderbuffers
      get_proc(261, Translations.delete_renderbuffers, Procs.delete_renderbuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteRenderbuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_renderbuffers!
      self.delete_renderbuffers ||
        raise FunctionUnavailableError.new(Translations.delete_renderbuffers)
    end

    # Checks if the OpenGL function *glDeleteRenderbuffers* is available.
    def delete_renderbuffers?
      !get_address(261, Translations.delete_renderbuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenRenderbuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def gen_renderbuffers
      get_proc(262, Translations.gen_renderbuffers, Procs.gen_renderbuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenRenderbuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def gen_renderbuffers!
      self.gen_renderbuffers ||
        raise FunctionUnavailableError.new(Translations.gen_renderbuffers)
    end

    # Checks if the OpenGL function *glGenRenderbuffers* is available.
    def gen_renderbuffers?
      !get_address(262, Translations.gen_renderbuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glRenderbufferStorage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def renderbuffer_storage
      get_proc(263, Translations.renderbuffer_storage, Procs.renderbuffer_storage)
    end

    # Retrieves a `Proc` for the OpenGL function *glRenderbufferStorage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def renderbuffer_storage!
      self.renderbuffer_storage ||
        raise FunctionUnavailableError.new(Translations.renderbuffer_storage)
    end

    # Checks if the OpenGL function *glRenderbufferStorage* is available.
    def renderbuffer_storage?
      !get_address(263, Translations.renderbuffer_storage)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetRenderbufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_renderbuffer_parameter_iv
      get_proc(264, Translations.get_renderbuffer_parameter_iv, Procs.get_renderbuffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetRenderbufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_renderbuffer_parameter_iv!
      self.get_renderbuffer_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_renderbuffer_parameter_iv)
    end

    # Checks if the OpenGL function *glGetRenderbufferParameteriv* is available.
    def get_renderbuffer_parameter_iv?
      !get_address(264, Translations.get_renderbuffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_framebuffer
      get_proc(265, Translations.is_framebuffer, Procs.is_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_framebuffer!
      self.is_framebuffer ||
        raise FunctionUnavailableError.new(Translations.is_framebuffer)
    end

    # Checks if the OpenGL function *glIsFramebuffer* is available.
    def is_framebuffer?
      !get_address(265, Translations.is_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_framebuffer
      get_proc(266, Translations.bind_framebuffer, Procs.bind_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_framebuffer!
      self.bind_framebuffer ||
        raise FunctionUnavailableError.new(Translations.bind_framebuffer)
    end

    # Checks if the OpenGL function *glBindFramebuffer* is available.
    def bind_framebuffer?
      !get_address(266, Translations.bind_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteFramebuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_framebuffers
      get_proc(267, Translations.delete_framebuffers, Procs.delete_framebuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteFramebuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_framebuffers!
      self.delete_framebuffers ||
        raise FunctionUnavailableError.new(Translations.delete_framebuffers)
    end

    # Checks if the OpenGL function *glDeleteFramebuffers* is available.
    def delete_framebuffers?
      !get_address(267, Translations.delete_framebuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenFramebuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def gen_framebuffers
      get_proc(268, Translations.gen_framebuffers, Procs.gen_framebuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenFramebuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def gen_framebuffers!
      self.gen_framebuffers ||
        raise FunctionUnavailableError.new(Translations.gen_framebuffers)
    end

    # Checks if the OpenGL function *glGenFramebuffers* is available.
    def gen_framebuffers?
      !get_address(268, Translations.gen_framebuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glCheckFramebufferStatus*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def check_framebuffer_status
      get_proc(269, Translations.check_framebuffer_status, Procs.check_framebuffer_status)
    end

    # Retrieves a `Proc` for the OpenGL function *glCheckFramebufferStatus*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def check_framebuffer_status!
      self.check_framebuffer_status ||
        raise FunctionUnavailableError.new(Translations.check_framebuffer_status)
    end

    # Checks if the OpenGL function *glCheckFramebufferStatus* is available.
    def check_framebuffer_status?
      !get_address(269, Translations.check_framebuffer_status)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferTexture1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def framebuffer_texture_1d
      get_proc(270, Translations.framebuffer_texture_1d, Procs.framebuffer_texture_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferTexture1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def framebuffer_texture_1d!
      self.framebuffer_texture_1d ||
        raise FunctionUnavailableError.new(Translations.framebuffer_texture_1d)
    end

    # Checks if the OpenGL function *glFramebufferTexture1D* is available.
    def framebuffer_texture_1d?
      !get_address(270, Translations.framebuffer_texture_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferTexture2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def framebuffer_texture_2d
      get_proc(271, Translations.framebuffer_texture_2d, Procs.framebuffer_texture_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferTexture2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def framebuffer_texture_2d!
      self.framebuffer_texture_2d ||
        raise FunctionUnavailableError.new(Translations.framebuffer_texture_2d)
    end

    # Checks if the OpenGL function *glFramebufferTexture2D* is available.
    def framebuffer_texture_2d?
      !get_address(271, Translations.framebuffer_texture_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferTexture3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def framebuffer_texture_3d
      get_proc(272, Translations.framebuffer_texture_3d, Procs.framebuffer_texture_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferTexture3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def framebuffer_texture_3d!
      self.framebuffer_texture_3d ||
        raise FunctionUnavailableError.new(Translations.framebuffer_texture_3d)
    end

    # Checks if the OpenGL function *glFramebufferTexture3D* is available.
    def framebuffer_texture_3d?
      !get_address(272, Translations.framebuffer_texture_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferRenderbuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def framebuffer_renderbuffer
      get_proc(273, Translations.framebuffer_renderbuffer, Procs.framebuffer_renderbuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferRenderbuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def framebuffer_renderbuffer!
      self.framebuffer_renderbuffer ||
        raise FunctionUnavailableError.new(Translations.framebuffer_renderbuffer)
    end

    # Checks if the OpenGL function *glFramebufferRenderbuffer* is available.
    def framebuffer_renderbuffer?
      !get_address(273, Translations.framebuffer_renderbuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFramebufferAttachmentParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_framebuffer_attachment_parameter_iv
      get_proc(274, Translations.get_framebuffer_attachment_parameter_iv, Procs.get_framebuffer_attachment_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFramebufferAttachmentParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_framebuffer_attachment_parameter_iv!
      self.get_framebuffer_attachment_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_framebuffer_attachment_parameter_iv)
    end

    # Checks if the OpenGL function *glGetFramebufferAttachmentParameteriv* is available.
    def get_framebuffer_attachment_parameter_iv?
      !get_address(274, Translations.get_framebuffer_attachment_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenerateMipmap*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def generate_mipmap
      get_proc(275, Translations.generate_mipmap, Procs.generate_mipmap)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenerateMipmap*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def generate_mipmap!
      self.generate_mipmap ||
        raise FunctionUnavailableError.new(Translations.generate_mipmap)
    end

    # Checks if the OpenGL function *glGenerateMipmap* is available.
    def generate_mipmap?
      !get_address(275, Translations.generate_mipmap)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlitFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blit_framebuffer
      get_proc(276, Translations.blit_framebuffer, Procs.blit_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlitFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blit_framebuffer!
      self.blit_framebuffer ||
        raise FunctionUnavailableError.new(Translations.blit_framebuffer)
    end

    # Checks if the OpenGL function *glBlitFramebuffer* is available.
    def blit_framebuffer?
      !get_address(276, Translations.blit_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glRenderbufferStorageMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def renderbuffer_storage_multisample
      get_proc(277, Translations.renderbuffer_storage_multisample, Procs.renderbuffer_storage_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glRenderbufferStorageMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def renderbuffer_storage_multisample!
      self.renderbuffer_storage_multisample ||
        raise FunctionUnavailableError.new(Translations.renderbuffer_storage_multisample)
    end

    # Checks if the OpenGL function *glRenderbufferStorageMultisample* is available.
    def renderbuffer_storage_multisample?
      !get_address(277, Translations.renderbuffer_storage_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferTextureLayer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def framebuffer_texture_layer
      get_proc(278, Translations.framebuffer_texture_layer, Procs.framebuffer_texture_layer)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferTextureLayer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def framebuffer_texture_layer!
      self.framebuffer_texture_layer ||
        raise FunctionUnavailableError.new(Translations.framebuffer_texture_layer)
    end

    # Checks if the OpenGL function *glFramebufferTextureLayer* is available.
    def framebuffer_texture_layer?
      !get_address(278, Translations.framebuffer_texture_layer)
    end

    # Retrieves a `Proc` for the OpenGL function *glMapBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def map_buffer_range
      get_proc(279, Translations.map_buffer_range, Procs.map_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glMapBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def map_buffer_range!
      self.map_buffer_range ||
        raise FunctionUnavailableError.new(Translations.map_buffer_range)
    end

    # Checks if the OpenGL function *glMapBufferRange* is available.
    def map_buffer_range?
      !get_address(279, Translations.map_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glFlushMappedBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def flush_mapped_buffer_range
      get_proc(280, Translations.flush_mapped_buffer_range, Procs.flush_mapped_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glFlushMappedBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def flush_mapped_buffer_range!
      self.flush_mapped_buffer_range ||
        raise FunctionUnavailableError.new(Translations.flush_mapped_buffer_range)
    end

    # Checks if the OpenGL function *glFlushMappedBufferRange* is available.
    def flush_mapped_buffer_range?
      !get_address(280, Translations.flush_mapped_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindVertexArray*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_vertex_array
      get_proc(281, Translations.bind_vertex_array, Procs.bind_vertex_array)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindVertexArray*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_vertex_array!
      self.bind_vertex_array ||
        raise FunctionUnavailableError.new(Translations.bind_vertex_array)
    end

    # Checks if the OpenGL function *glBindVertexArray* is available.
    def bind_vertex_array?
      !get_address(281, Translations.bind_vertex_array)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteVertexArrays*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_vertex_arrays
      get_proc(282, Translations.delete_vertex_arrays, Procs.delete_vertex_arrays)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteVertexArrays*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_vertex_arrays!
      self.delete_vertex_arrays ||
        raise FunctionUnavailableError.new(Translations.delete_vertex_arrays)
    end

    # Checks if the OpenGL function *glDeleteVertexArrays* is available.
    def delete_vertex_arrays?
      !get_address(282, Translations.delete_vertex_arrays)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenVertexArrays*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def gen_vertex_arrays
      get_proc(283, Translations.gen_vertex_arrays, Procs.gen_vertex_arrays)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenVertexArrays*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def gen_vertex_arrays!
      self.gen_vertex_arrays ||
        raise FunctionUnavailableError.new(Translations.gen_vertex_arrays)
    end

    # Checks if the OpenGL function *glGenVertexArrays* is available.
    def gen_vertex_arrays?
      !get_address(283, Translations.gen_vertex_arrays)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsVertexArray*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_vertex_array
      get_proc(284, Translations.is_vertex_array, Procs.is_vertex_array)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsVertexArray*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_vertex_array!
      self.is_vertex_array ||
        raise FunctionUnavailableError.new(Translations.is_vertex_array)
    end

    # Checks if the OpenGL function *glIsVertexArray* is available.
    def is_vertex_array?
      !get_address(284, Translations.is_vertex_array)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawArraysInstanced*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_arrays_instanced
      get_proc(285, Translations.draw_arrays_instanced, Procs.draw_arrays_instanced)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawArraysInstanced*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_arrays_instanced!
      self.draw_arrays_instanced ||
        raise FunctionUnavailableError.new(Translations.draw_arrays_instanced)
    end

    # Checks if the OpenGL function *glDrawArraysInstanced* is available.
    def draw_arrays_instanced?
      !get_address(285, Translations.draw_arrays_instanced)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsInstanced*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_elements_instanced
      get_proc(286, Translations.draw_elements_instanced, Procs.draw_elements_instanced)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsInstanced*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_elements_instanced!
      self.draw_elements_instanced ||
        raise FunctionUnavailableError.new(Translations.draw_elements_instanced)
    end

    # Checks if the OpenGL function *glDrawElementsInstanced* is available.
    def draw_elements_instanced?
      !get_address(286, Translations.draw_elements_instanced)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_buffer
      get_proc(287, Translations.tex_buffer, Procs.tex_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_buffer!
      self.tex_buffer ||
        raise FunctionUnavailableError.new(Translations.tex_buffer)
    end

    # Checks if the OpenGL function *glTexBuffer* is available.
    def tex_buffer?
      !get_address(287, Translations.tex_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glPrimitiveRestartIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def primitive_restart_index
      get_proc(288, Translations.primitive_restart_index, Procs.primitive_restart_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glPrimitiveRestartIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def primitive_restart_index!
      self.primitive_restart_index ||
        raise FunctionUnavailableError.new(Translations.primitive_restart_index)
    end

    # Checks if the OpenGL function *glPrimitiveRestartIndex* is available.
    def primitive_restart_index?
      !get_address(288, Translations.primitive_restart_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_buffer_sub_data
      get_proc(289, Translations.copy_buffer_sub_data, Procs.copy_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_buffer_sub_data!
      self.copy_buffer_sub_data ||
        raise FunctionUnavailableError.new(Translations.copy_buffer_sub_data)
    end

    # Checks if the OpenGL function *glCopyBufferSubData* is available.
    def copy_buffer_sub_data?
      !get_address(289, Translations.copy_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformIndices*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_uniform_indices
      get_proc(290, Translations.get_uniform_indices, Procs.get_uniform_indices)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformIndices*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_uniform_indices!
      self.get_uniform_indices ||
        raise FunctionUnavailableError.new(Translations.get_uniform_indices)
    end

    # Checks if the OpenGL function *glGetUniformIndices* is available.
    def get_uniform_indices?
      !get_address(290, Translations.get_uniform_indices)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveUniformsiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_active_uniforms_iv
      get_proc(291, Translations.get_active_uniforms_iv, Procs.get_active_uniforms_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveUniformsiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_active_uniforms_iv!
      self.get_active_uniforms_iv ||
        raise FunctionUnavailableError.new(Translations.get_active_uniforms_iv)
    end

    # Checks if the OpenGL function *glGetActiveUniformsiv* is available.
    def get_active_uniforms_iv?
      !get_address(291, Translations.get_active_uniforms_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveUniformName*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_active_uniform_name
      get_proc(292, Translations.get_active_uniform_name, Procs.get_active_uniform_name)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveUniformName*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_active_uniform_name!
      self.get_active_uniform_name ||
        raise FunctionUnavailableError.new(Translations.get_active_uniform_name)
    end

    # Checks if the OpenGL function *glGetActiveUniformName* is available.
    def get_active_uniform_name?
      !get_address(292, Translations.get_active_uniform_name)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformBlockIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_uniform_block_index
      get_proc(293, Translations.get_uniform_block_index, Procs.get_uniform_block_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformBlockIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_uniform_block_index!
      self.get_uniform_block_index ||
        raise FunctionUnavailableError.new(Translations.get_uniform_block_index)
    end

    # Checks if the OpenGL function *glGetUniformBlockIndex* is available.
    def get_uniform_block_index?
      !get_address(293, Translations.get_uniform_block_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveUniformBlockiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_active_uniform_block_iv
      get_proc(294, Translations.get_active_uniform_block_iv, Procs.get_active_uniform_block_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveUniformBlockiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_active_uniform_block_iv!
      self.get_active_uniform_block_iv ||
        raise FunctionUnavailableError.new(Translations.get_active_uniform_block_iv)
    end

    # Checks if the OpenGL function *glGetActiveUniformBlockiv* is available.
    def get_active_uniform_block_iv?
      !get_address(294, Translations.get_active_uniform_block_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveUniformBlockName*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_active_uniform_block_name
      get_proc(295, Translations.get_active_uniform_block_name, Procs.get_active_uniform_block_name)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveUniformBlockName*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_active_uniform_block_name!
      self.get_active_uniform_block_name ||
        raise FunctionUnavailableError.new(Translations.get_active_uniform_block_name)
    end

    # Checks if the OpenGL function *glGetActiveUniformBlockName* is available.
    def get_active_uniform_block_name?
      !get_address(295, Translations.get_active_uniform_block_name)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformBlockBinding*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_block_binding
      get_proc(296, Translations.uniform_block_binding, Procs.uniform_block_binding)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformBlockBinding*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_block_binding!
      self.uniform_block_binding ||
        raise FunctionUnavailableError.new(Translations.uniform_block_binding)
    end

    # Checks if the OpenGL function *glUniformBlockBinding* is available.
    def uniform_block_binding?
      !get_address(296, Translations.uniform_block_binding)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsBaseVertex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_elements_base_vertex
      get_proc(297, Translations.draw_elements_base_vertex, Procs.draw_elements_base_vertex)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsBaseVertex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_elements_base_vertex!
      self.draw_elements_base_vertex ||
        raise FunctionUnavailableError.new(Translations.draw_elements_base_vertex)
    end

    # Checks if the OpenGL function *glDrawElementsBaseVertex* is available.
    def draw_elements_base_vertex?
      !get_address(297, Translations.draw_elements_base_vertex)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawRangeElementsBaseVertex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_range_elements_base_vertex
      get_proc(298, Translations.draw_range_elements_base_vertex, Procs.draw_range_elements_base_vertex)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawRangeElementsBaseVertex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_range_elements_base_vertex!
      self.draw_range_elements_base_vertex ||
        raise FunctionUnavailableError.new(Translations.draw_range_elements_base_vertex)
    end

    # Checks if the OpenGL function *glDrawRangeElementsBaseVertex* is available.
    def draw_range_elements_base_vertex?
      !get_address(298, Translations.draw_range_elements_base_vertex)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsInstancedBaseVertex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_elements_instanced_base_vertex
      get_proc(299, Translations.draw_elements_instanced_base_vertex, Procs.draw_elements_instanced_base_vertex)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsInstancedBaseVertex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_elements_instanced_base_vertex!
      self.draw_elements_instanced_base_vertex ||
        raise FunctionUnavailableError.new(Translations.draw_elements_instanced_base_vertex)
    end

    # Checks if the OpenGL function *glDrawElementsInstancedBaseVertex* is available.
    def draw_elements_instanced_base_vertex?
      !get_address(299, Translations.draw_elements_instanced_base_vertex)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawElementsBaseVertex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def multi_draw_elements_base_vertex
      get_proc(300, Translations.multi_draw_elements_base_vertex, Procs.multi_draw_elements_base_vertex)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawElementsBaseVertex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def multi_draw_elements_base_vertex!
      self.multi_draw_elements_base_vertex ||
        raise FunctionUnavailableError.new(Translations.multi_draw_elements_base_vertex)
    end

    # Checks if the OpenGL function *glMultiDrawElementsBaseVertex* is available.
    def multi_draw_elements_base_vertex?
      !get_address(300, Translations.multi_draw_elements_base_vertex)
    end

    # Retrieves a `Proc` for the OpenGL function *glProvokingVertex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def provoking_vertex
      get_proc(301, Translations.provoking_vertex, Procs.provoking_vertex)
    end

    # Retrieves a `Proc` for the OpenGL function *glProvokingVertex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def provoking_vertex!
      self.provoking_vertex ||
        raise FunctionUnavailableError.new(Translations.provoking_vertex)
    end

    # Checks if the OpenGL function *glProvokingVertex* is available.
    def provoking_vertex?
      !get_address(301, Translations.provoking_vertex)
    end

    # Retrieves a `Proc` for the OpenGL function *glFenceSync*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def fence_sync
      get_proc(302, Translations.fence_sync, Procs.fence_sync)
    end

    # Retrieves a `Proc` for the OpenGL function *glFenceSync*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def fence_sync!
      self.fence_sync ||
        raise FunctionUnavailableError.new(Translations.fence_sync)
    end

    # Checks if the OpenGL function *glFenceSync* is available.
    def fence_sync?
      !get_address(302, Translations.fence_sync)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsSync*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_sync
      get_proc(303, Translations.is_sync, Procs.is_sync)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsSync*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_sync!
      self.is_sync ||
        raise FunctionUnavailableError.new(Translations.is_sync)
    end

    # Checks if the OpenGL function *glIsSync* is available.
    def is_sync?
      !get_address(303, Translations.is_sync)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteSync*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_sync
      get_proc(304, Translations.delete_sync, Procs.delete_sync)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteSync*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_sync!
      self.delete_sync ||
        raise FunctionUnavailableError.new(Translations.delete_sync)
    end

    # Checks if the OpenGL function *glDeleteSync* is available.
    def delete_sync?
      !get_address(304, Translations.delete_sync)
    end

    # Retrieves a `Proc` for the OpenGL function *glClientWaitSync*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def client_wait_sync
      get_proc(305, Translations.client_wait_sync, Procs.client_wait_sync)
    end

    # Retrieves a `Proc` for the OpenGL function *glClientWaitSync*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def client_wait_sync!
      self.client_wait_sync ||
        raise FunctionUnavailableError.new(Translations.client_wait_sync)
    end

    # Checks if the OpenGL function *glClientWaitSync* is available.
    def client_wait_sync?
      !get_address(305, Translations.client_wait_sync)
    end

    # Retrieves a `Proc` for the OpenGL function *glWaitSync*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def wait_sync
      get_proc(306, Translations.wait_sync, Procs.wait_sync)
    end

    # Retrieves a `Proc` for the OpenGL function *glWaitSync*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def wait_sync!
      self.wait_sync ||
        raise FunctionUnavailableError.new(Translations.wait_sync)
    end

    # Checks if the OpenGL function *glWaitSync* is available.
    def wait_sync?
      !get_address(306, Translations.wait_sync)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetInteger64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_integer_64v
      get_proc(307, Translations.get_integer_64v, Procs.get_integer_64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetInteger64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_integer_64v!
      self.get_integer_64v ||
        raise FunctionUnavailableError.new(Translations.get_integer_64v)
    end

    # Checks if the OpenGL function *glGetInteger64v* is available.
    def get_integer_64v?
      !get_address(307, Translations.get_integer_64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSynciv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_sync_iv
      get_proc(308, Translations.get_sync_iv, Procs.get_sync_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSynciv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_sync_iv!
      self.get_sync_iv ||
        raise FunctionUnavailableError.new(Translations.get_sync_iv)
    end

    # Checks if the OpenGL function *glGetSynciv* is available.
    def get_sync_iv?
      !get_address(308, Translations.get_sync_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetInteger64i_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_integer_64i_v
      get_proc(309, Translations.get_integer_64i_v, Procs.get_integer_64i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetInteger64i_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_integer_64i_v!
      self.get_integer_64i_v ||
        raise FunctionUnavailableError.new(Translations.get_integer_64i_v)
    end

    # Checks if the OpenGL function *glGetInteger64i_v* is available.
    def get_integer_64i_v?
      !get_address(309, Translations.get_integer_64i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBufferParameteri64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_buffer_parameter_i64v
      get_proc(310, Translations.get_buffer_parameter_i64v, Procs.get_buffer_parameter_i64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetBufferParameteri64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_buffer_parameter_i64v!
      self.get_buffer_parameter_i64v ||
        raise FunctionUnavailableError.new(Translations.get_buffer_parameter_i64v)
    end

    # Checks if the OpenGL function *glGetBufferParameteri64v* is available.
    def get_buffer_parameter_i64v?
      !get_address(310, Translations.get_buffer_parameter_i64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def framebuffer_texture
      get_proc(311, Translations.framebuffer_texture, Procs.framebuffer_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def framebuffer_texture!
      self.framebuffer_texture ||
        raise FunctionUnavailableError.new(Translations.framebuffer_texture)
    end

    # Checks if the OpenGL function *glFramebufferTexture* is available.
    def framebuffer_texture?
      !get_address(311, Translations.framebuffer_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexImage2DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_image_2d_multisample
      get_proc(312, Translations.tex_image_2d_multisample, Procs.tex_image_2d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexImage2DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_image_2d_multisample!
      self.tex_image_2d_multisample ||
        raise FunctionUnavailableError.new(Translations.tex_image_2d_multisample)
    end

    # Checks if the OpenGL function *glTexImage2DMultisample* is available.
    def tex_image_2d_multisample?
      !get_address(312, Translations.tex_image_2d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexImage3DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_image_3d_multisample
      get_proc(313, Translations.tex_image_3d_multisample, Procs.tex_image_3d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexImage3DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_image_3d_multisample!
      self.tex_image_3d_multisample ||
        raise FunctionUnavailableError.new(Translations.tex_image_3d_multisample)
    end

    # Checks if the OpenGL function *glTexImage3DMultisample* is available.
    def tex_image_3d_multisample?
      !get_address(313, Translations.tex_image_3d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetMultisamplefv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_multisample_fv
      get_proc(314, Translations.get_multisample_fv, Procs.get_multisample_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetMultisamplefv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_multisample_fv!
      self.get_multisample_fv ||
        raise FunctionUnavailableError.new(Translations.get_multisample_fv)
    end

    # Checks if the OpenGL function *glGetMultisamplefv* is available.
    def get_multisample_fv?
      !get_address(314, Translations.get_multisample_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glSampleMaski*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def sample_mask_i
      get_proc(315, Translations.sample_mask_i, Procs.sample_mask_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glSampleMaski*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def sample_mask_i!
      self.sample_mask_i ||
        raise FunctionUnavailableError.new(Translations.sample_mask_i)
    end

    # Checks if the OpenGL function *glSampleMaski* is available.
    def sample_mask_i?
      !get_address(315, Translations.sample_mask_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindFragDataLocationIndexed*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_frag_data_location_indexed
      get_proc(316, Translations.bind_frag_data_location_indexed, Procs.bind_frag_data_location_indexed)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindFragDataLocationIndexed*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_frag_data_location_indexed!
      self.bind_frag_data_location_indexed ||
        raise FunctionUnavailableError.new(Translations.bind_frag_data_location_indexed)
    end

    # Checks if the OpenGL function *glBindFragDataLocationIndexed* is available.
    def bind_frag_data_location_indexed?
      !get_address(316, Translations.bind_frag_data_location_indexed)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFragDataIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_frag_data_index
      get_proc(317, Translations.get_frag_data_index, Procs.get_frag_data_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFragDataIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_frag_data_index!
      self.get_frag_data_index ||
        raise FunctionUnavailableError.new(Translations.get_frag_data_index)
    end

    # Checks if the OpenGL function *glGetFragDataIndex* is available.
    def get_frag_data_index?
      !get_address(317, Translations.get_frag_data_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenSamplers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def gen_samplers
      get_proc(318, Translations.gen_samplers, Procs.gen_samplers)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenSamplers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def gen_samplers!
      self.gen_samplers ||
        raise FunctionUnavailableError.new(Translations.gen_samplers)
    end

    # Checks if the OpenGL function *glGenSamplers* is available.
    def gen_samplers?
      !get_address(318, Translations.gen_samplers)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteSamplers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_samplers
      get_proc(319, Translations.delete_samplers, Procs.delete_samplers)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteSamplers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_samplers!
      self.delete_samplers ||
        raise FunctionUnavailableError.new(Translations.delete_samplers)
    end

    # Checks if the OpenGL function *glDeleteSamplers* is available.
    def delete_samplers?
      !get_address(319, Translations.delete_samplers)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsSampler*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_sampler
      get_proc(320, Translations.is_sampler, Procs.is_sampler)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsSampler*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_sampler!
      self.is_sampler ||
        raise FunctionUnavailableError.new(Translations.is_sampler)
    end

    # Checks if the OpenGL function *glIsSampler* is available.
    def is_sampler?
      !get_address(320, Translations.is_sampler)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindSampler*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_sampler
      get_proc(321, Translations.bind_sampler, Procs.bind_sampler)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindSampler*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_sampler!
      self.bind_sampler ||
        raise FunctionUnavailableError.new(Translations.bind_sampler)
    end

    # Checks if the OpenGL function *glBindSampler* is available.
    def bind_sampler?
      !get_address(321, Translations.bind_sampler)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def sampler_parameter_i
      get_proc(322, Translations.sampler_parameter_i, Procs.sampler_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def sampler_parameter_i!
      self.sampler_parameter_i ||
        raise FunctionUnavailableError.new(Translations.sampler_parameter_i)
    end

    # Checks if the OpenGL function *glSamplerParameteri* is available.
    def sampler_parameter_i?
      !get_address(322, Translations.sampler_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def sampler_parameter_iv
      get_proc(323, Translations.sampler_parameter_iv, Procs.sampler_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def sampler_parameter_iv!
      self.sampler_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.sampler_parameter_iv)
    end

    # Checks if the OpenGL function *glSamplerParameteriv* is available.
    def sampler_parameter_iv?
      !get_address(323, Translations.sampler_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameterf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def sampler_parameter_f
      get_proc(324, Translations.sampler_parameter_f, Procs.sampler_parameter_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameterf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def sampler_parameter_f!
      self.sampler_parameter_f ||
        raise FunctionUnavailableError.new(Translations.sampler_parameter_f)
    end

    # Checks if the OpenGL function *glSamplerParameterf* is available.
    def sampler_parameter_f?
      !get_address(324, Translations.sampler_parameter_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def sampler_parameter_fv
      get_proc(325, Translations.sampler_parameter_fv, Procs.sampler_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def sampler_parameter_fv!
      self.sampler_parameter_fv ||
        raise FunctionUnavailableError.new(Translations.sampler_parameter_fv)
    end

    # Checks if the OpenGL function *glSamplerParameterfv* is available.
    def sampler_parameter_fv?
      !get_address(325, Translations.sampler_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def sampler_parameter_i_iv
      get_proc(326, Translations.sampler_parameter_i_iv, Procs.sampler_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def sampler_parameter_i_iv!
      self.sampler_parameter_i_iv ||
        raise FunctionUnavailableError.new(Translations.sampler_parameter_i_iv)
    end

    # Checks if the OpenGL function *glSamplerParameterIiv* is available.
    def sampler_parameter_i_iv?
      !get_address(326, Translations.sampler_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def sampler_parameter_i_uiv
      get_proc(327, Translations.sampler_parameter_i_uiv, Procs.sampler_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glSamplerParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def sampler_parameter_i_uiv!
      self.sampler_parameter_i_uiv ||
        raise FunctionUnavailableError.new(Translations.sampler_parameter_i_uiv)
    end

    # Checks if the OpenGL function *glSamplerParameterIuiv* is available.
    def sampler_parameter_i_uiv?
      !get_address(327, Translations.sampler_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSamplerParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_sampler_parameter_iv
      get_proc(328, Translations.get_sampler_parameter_iv, Procs.get_sampler_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSamplerParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_sampler_parameter_iv!
      self.get_sampler_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_sampler_parameter_iv)
    end

    # Checks if the OpenGL function *glGetSamplerParameteriv* is available.
    def get_sampler_parameter_iv?
      !get_address(328, Translations.get_sampler_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSamplerParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_sampler_parameter_i_iv
      get_proc(329, Translations.get_sampler_parameter_i_iv, Procs.get_sampler_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSamplerParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_sampler_parameter_i_iv!
      self.get_sampler_parameter_i_iv ||
        raise FunctionUnavailableError.new(Translations.get_sampler_parameter_i_iv)
    end

    # Checks if the OpenGL function *glGetSamplerParameterIiv* is available.
    def get_sampler_parameter_i_iv?
      !get_address(329, Translations.get_sampler_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSamplerParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_sampler_parameter_fv
      get_proc(330, Translations.get_sampler_parameter_fv, Procs.get_sampler_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSamplerParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_sampler_parameter_fv!
      self.get_sampler_parameter_fv ||
        raise FunctionUnavailableError.new(Translations.get_sampler_parameter_fv)
    end

    # Checks if the OpenGL function *glGetSamplerParameterfv* is available.
    def get_sampler_parameter_fv?
      !get_address(330, Translations.get_sampler_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSamplerParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_sampler_parameter_i_uiv
      get_proc(331, Translations.get_sampler_parameter_i_uiv, Procs.get_sampler_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSamplerParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_sampler_parameter_i_uiv!
      self.get_sampler_parameter_i_uiv ||
        raise FunctionUnavailableError.new(Translations.get_sampler_parameter_i_uiv)
    end

    # Checks if the OpenGL function *glGetSamplerParameterIuiv* is available.
    def get_sampler_parameter_i_uiv?
      !get_address(331, Translations.get_sampler_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glQueryCounter*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def query_counter
      get_proc(332, Translations.query_counter, Procs.query_counter)
    end

    # Retrieves a `Proc` for the OpenGL function *glQueryCounter*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def query_counter!
      self.query_counter ||
        raise FunctionUnavailableError.new(Translations.query_counter)
    end

    # Checks if the OpenGL function *glQueryCounter* is available.
    def query_counter?
      !get_address(332, Translations.query_counter)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryObjecti64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_query_object_i64v
      get_proc(333, Translations.get_query_object_i64v, Procs.get_query_object_i64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryObjecti64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_query_object_i64v!
      self.get_query_object_i64v ||
        raise FunctionUnavailableError.new(Translations.get_query_object_i64v)
    end

    # Checks if the OpenGL function *glGetQueryObjecti64v* is available.
    def get_query_object_i64v?
      !get_address(333, Translations.get_query_object_i64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryObjectui64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_query_object_ui64v
      get_proc(334, Translations.get_query_object_ui64v, Procs.get_query_object_ui64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryObjectui64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_query_object_ui64v!
      self.get_query_object_ui64v ||
        raise FunctionUnavailableError.new(Translations.get_query_object_ui64v)
    end

    # Checks if the OpenGL function *glGetQueryObjectui64v* is available.
    def get_query_object_ui64v?
      !get_address(334, Translations.get_query_object_ui64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribDivisor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_divisor
      get_proc(335, Translations.vertex_attrib_divisor, Procs.vertex_attrib_divisor)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribDivisor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_divisor!
      self.vertex_attrib_divisor ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_divisor)
    end

    # Checks if the OpenGL function *glVertexAttribDivisor* is available.
    def vertex_attrib_divisor?
      !get_address(335, Translations.vertex_attrib_divisor)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP1ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_p_1ui
      get_proc(336, Translations.vertex_attrib_p_1ui, Procs.vertex_attrib_p_1ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP1ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_p_1ui!
      self.vertex_attrib_p_1ui ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_p_1ui)
    end

    # Checks if the OpenGL function *glVertexAttribP1ui* is available.
    def vertex_attrib_p_1ui?
      !get_address(336, Translations.vertex_attrib_p_1ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP1uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_p_1uiv
      get_proc(337, Translations.vertex_attrib_p_1uiv, Procs.vertex_attrib_p_1uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP1uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_p_1uiv!
      self.vertex_attrib_p_1uiv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_p_1uiv)
    end

    # Checks if the OpenGL function *glVertexAttribP1uiv* is available.
    def vertex_attrib_p_1uiv?
      !get_address(337, Translations.vertex_attrib_p_1uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP2ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_p_2ui
      get_proc(338, Translations.vertex_attrib_p_2ui, Procs.vertex_attrib_p_2ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP2ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_p_2ui!
      self.vertex_attrib_p_2ui ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_p_2ui)
    end

    # Checks if the OpenGL function *glVertexAttribP2ui* is available.
    def vertex_attrib_p_2ui?
      !get_address(338, Translations.vertex_attrib_p_2ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP2uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_p_2uiv
      get_proc(339, Translations.vertex_attrib_p_2uiv, Procs.vertex_attrib_p_2uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP2uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_p_2uiv!
      self.vertex_attrib_p_2uiv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_p_2uiv)
    end

    # Checks if the OpenGL function *glVertexAttribP2uiv* is available.
    def vertex_attrib_p_2uiv?
      !get_address(339, Translations.vertex_attrib_p_2uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP3ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_p_3ui
      get_proc(340, Translations.vertex_attrib_p_3ui, Procs.vertex_attrib_p_3ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP3ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_p_3ui!
      self.vertex_attrib_p_3ui ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_p_3ui)
    end

    # Checks if the OpenGL function *glVertexAttribP3ui* is available.
    def vertex_attrib_p_3ui?
      !get_address(340, Translations.vertex_attrib_p_3ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP3uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_p_3uiv
      get_proc(341, Translations.vertex_attrib_p_3uiv, Procs.vertex_attrib_p_3uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP3uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_p_3uiv!
      self.vertex_attrib_p_3uiv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_p_3uiv)
    end

    # Checks if the OpenGL function *glVertexAttribP3uiv* is available.
    def vertex_attrib_p_3uiv?
      !get_address(341, Translations.vertex_attrib_p_3uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP4ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_p_4ui
      get_proc(342, Translations.vertex_attrib_p_4ui, Procs.vertex_attrib_p_4ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP4ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_p_4ui!
      self.vertex_attrib_p_4ui ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_p_4ui)
    end

    # Checks if the OpenGL function *glVertexAttribP4ui* is available.
    def vertex_attrib_p_4ui?
      !get_address(342, Translations.vertex_attrib_p_4ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP4uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_p_4uiv
      get_proc(343, Translations.vertex_attrib_p_4uiv, Procs.vertex_attrib_p_4uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribP4uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_p_4uiv!
      self.vertex_attrib_p_4uiv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_p_4uiv)
    end

    # Checks if the OpenGL function *glVertexAttribP4uiv* is available.
    def vertex_attrib_p_4uiv?
      !get_address(343, Translations.vertex_attrib_p_4uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glMinSampleShading*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def min_sample_shading
      get_proc(344, Translations.min_sample_shading, Procs.min_sample_shading)
    end

    # Retrieves a `Proc` for the OpenGL function *glMinSampleShading*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def min_sample_shading!
      self.min_sample_shading ||
        raise FunctionUnavailableError.new(Translations.min_sample_shading)
    end

    # Checks if the OpenGL function *glMinSampleShading* is available.
    def min_sample_shading?
      !get_address(344, Translations.min_sample_shading)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendEquationi*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blend_equation_i
      get_proc(345, Translations.blend_equation_i, Procs.blend_equation_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendEquationi*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blend_equation_i!
      self.blend_equation_i ||
        raise FunctionUnavailableError.new(Translations.blend_equation_i)
    end

    # Checks if the OpenGL function *glBlendEquationi* is available.
    def blend_equation_i?
      !get_address(345, Translations.blend_equation_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendEquationSeparatei*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blend_equation_separate_i
      get_proc(346, Translations.blend_equation_separate_i, Procs.blend_equation_separate_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendEquationSeparatei*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blend_equation_separate_i!
      self.blend_equation_separate_i ||
        raise FunctionUnavailableError.new(Translations.blend_equation_separate_i)
    end

    # Checks if the OpenGL function *glBlendEquationSeparatei* is available.
    def blend_equation_separate_i?
      !get_address(346, Translations.blend_equation_separate_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendFunci*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blend_func_i
      get_proc(347, Translations.blend_func_i, Procs.blend_func_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendFunci*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blend_func_i!
      self.blend_func_i ||
        raise FunctionUnavailableError.new(Translations.blend_func_i)
    end

    # Checks if the OpenGL function *glBlendFunci* is available.
    def blend_func_i?
      !get_address(347, Translations.blend_func_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendFuncSeparatei*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blend_func_separate_i
      get_proc(348, Translations.blend_func_separate_i, Procs.blend_func_separate_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlendFuncSeparatei*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blend_func_separate_i!
      self.blend_func_separate_i ||
        raise FunctionUnavailableError.new(Translations.blend_func_separate_i)
    end

    # Checks if the OpenGL function *glBlendFuncSeparatei* is available.
    def blend_func_separate_i?
      !get_address(348, Translations.blend_func_separate_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawArraysIndirect*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_arrays_indirect
      get_proc(349, Translations.draw_arrays_indirect, Procs.draw_arrays_indirect)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawArraysIndirect*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_arrays_indirect!
      self.draw_arrays_indirect ||
        raise FunctionUnavailableError.new(Translations.draw_arrays_indirect)
    end

    # Checks if the OpenGL function *glDrawArraysIndirect* is available.
    def draw_arrays_indirect?
      !get_address(349, Translations.draw_arrays_indirect)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsIndirect*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_elements_indirect
      get_proc(350, Translations.draw_elements_indirect, Procs.draw_elements_indirect)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsIndirect*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_elements_indirect!
      self.draw_elements_indirect ||
        raise FunctionUnavailableError.new(Translations.draw_elements_indirect)
    end

    # Checks if the OpenGL function *glDrawElementsIndirect* is available.
    def draw_elements_indirect?
      !get_address(350, Translations.draw_elements_indirect)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_1d
      get_proc(351, Translations.uniform_1d, Procs.uniform_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_1d!
      self.uniform_1d ||
        raise FunctionUnavailableError.new(Translations.uniform_1d)
    end

    # Checks if the OpenGL function *glUniform1d* is available.
    def uniform_1d?
      !get_address(351, Translations.uniform_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_2d
      get_proc(352, Translations.uniform_2d, Procs.uniform_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_2d!
      self.uniform_2d ||
        raise FunctionUnavailableError.new(Translations.uniform_2d)
    end

    # Checks if the OpenGL function *glUniform2d* is available.
    def uniform_2d?
      !get_address(352, Translations.uniform_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_3d
      get_proc(353, Translations.uniform_3d, Procs.uniform_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_3d!
      self.uniform_3d ||
        raise FunctionUnavailableError.new(Translations.uniform_3d)
    end

    # Checks if the OpenGL function *glUniform3d* is available.
    def uniform_3d?
      !get_address(353, Translations.uniform_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_4d
      get_proc(354, Translations.uniform_4d, Procs.uniform_4d)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_4d!
      self.uniform_4d ||
        raise FunctionUnavailableError.new(Translations.uniform_4d)
    end

    # Checks if the OpenGL function *glUniform4d* is available.
    def uniform_4d?
      !get_address(354, Translations.uniform_4d)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_1dv
      get_proc(355, Translations.uniform_1dv, Procs.uniform_1dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform1dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_1dv!
      self.uniform_1dv ||
        raise FunctionUnavailableError.new(Translations.uniform_1dv)
    end

    # Checks if the OpenGL function *glUniform1dv* is available.
    def uniform_1dv?
      !get_address(355, Translations.uniform_1dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_2dv
      get_proc(356, Translations.uniform_2dv, Procs.uniform_2dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_2dv!
      self.uniform_2dv ||
        raise FunctionUnavailableError.new(Translations.uniform_2dv)
    end

    # Checks if the OpenGL function *glUniform2dv* is available.
    def uniform_2dv?
      !get_address(356, Translations.uniform_2dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_3dv
      get_proc(357, Translations.uniform_3dv, Procs.uniform_3dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_3dv!
      self.uniform_3dv ||
        raise FunctionUnavailableError.new(Translations.uniform_3dv)
    end

    # Checks if the OpenGL function *glUniform3dv* is available.
    def uniform_3dv?
      !get_address(357, Translations.uniform_3dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_4dv
      get_proc(358, Translations.uniform_4dv, Procs.uniform_4dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniform4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_4dv!
      self.uniform_4dv ||
        raise FunctionUnavailableError.new(Translations.uniform_4dv)
    end

    # Checks if the OpenGL function *glUniform4dv* is available.
    def uniform_4dv?
      !get_address(358, Translations.uniform_4dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix2_dv
      get_proc(359, Translations.uniform_matrix2_dv, Procs.uniform_matrix2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix2_dv!
      self.uniform_matrix2_dv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix2_dv)
    end

    # Checks if the OpenGL function *glUniformMatrix2dv* is available.
    def uniform_matrix2_dv?
      !get_address(359, Translations.uniform_matrix2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix3_dv
      get_proc(360, Translations.uniform_matrix3_dv, Procs.uniform_matrix3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix3_dv!
      self.uniform_matrix3_dv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix3_dv)
    end

    # Checks if the OpenGL function *glUniformMatrix3dv* is available.
    def uniform_matrix3_dv?
      !get_address(360, Translations.uniform_matrix3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix4_dv
      get_proc(361, Translations.uniform_matrix4_dv, Procs.uniform_matrix4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix4_dv!
      self.uniform_matrix4_dv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix4_dv)
    end

    # Checks if the OpenGL function *glUniformMatrix4dv* is available.
    def uniform_matrix4_dv?
      !get_address(361, Translations.uniform_matrix4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2x3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix2x3_dv
      get_proc(362, Translations.uniform_matrix2x3_dv, Procs.uniform_matrix2x3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2x3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix2x3_dv!
      self.uniform_matrix2x3_dv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix2x3_dv)
    end

    # Checks if the OpenGL function *glUniformMatrix2x3dv* is available.
    def uniform_matrix2x3_dv?
      !get_address(362, Translations.uniform_matrix2x3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2x4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix2x4_dv
      get_proc(363, Translations.uniform_matrix2x4_dv, Procs.uniform_matrix2x4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix2x4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix2x4_dv!
      self.uniform_matrix2x4_dv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix2x4_dv)
    end

    # Checks if the OpenGL function *glUniformMatrix2x4dv* is available.
    def uniform_matrix2x4_dv?
      !get_address(363, Translations.uniform_matrix2x4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3x2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix3x2_dv
      get_proc(364, Translations.uniform_matrix3x2_dv, Procs.uniform_matrix3x2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3x2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix3x2_dv!
      self.uniform_matrix3x2_dv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix3x2_dv)
    end

    # Checks if the OpenGL function *glUniformMatrix3x2dv* is available.
    def uniform_matrix3x2_dv?
      !get_address(364, Translations.uniform_matrix3x2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3x4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix3x4_dv
      get_proc(365, Translations.uniform_matrix3x4_dv, Procs.uniform_matrix3x4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix3x4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix3x4_dv!
      self.uniform_matrix3x4_dv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix3x4_dv)
    end

    # Checks if the OpenGL function *glUniformMatrix3x4dv* is available.
    def uniform_matrix3x4_dv?
      !get_address(365, Translations.uniform_matrix3x4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4x2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix4x2_dv
      get_proc(366, Translations.uniform_matrix4x2_dv, Procs.uniform_matrix4x2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4x2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix4x2_dv!
      self.uniform_matrix4x2_dv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix4x2_dv)
    end

    # Checks if the OpenGL function *glUniformMatrix4x2dv* is available.
    def uniform_matrix4x2_dv?
      !get_address(366, Translations.uniform_matrix4x2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4x3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_matrix4x3_dv
      get_proc(367, Translations.uniform_matrix4x3_dv, Procs.uniform_matrix4x3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformMatrix4x3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_matrix4x3_dv!
      self.uniform_matrix4x3_dv ||
        raise FunctionUnavailableError.new(Translations.uniform_matrix4x3_dv)
    end

    # Checks if the OpenGL function *glUniformMatrix4x3dv* is available.
    def uniform_matrix4x3_dv?
      !get_address(367, Translations.uniform_matrix4x3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformdv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_uniform_dv
      get_proc(368, Translations.get_uniform_dv, Procs.get_uniform_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformdv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_uniform_dv!
      self.get_uniform_dv ||
        raise FunctionUnavailableError.new(Translations.get_uniform_dv)
    end

    # Checks if the OpenGL function *glGetUniformdv* is available.
    def get_uniform_dv?
      !get_address(368, Translations.get_uniform_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSubroutineUniformLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_subroutine_uniform_location
      get_proc(369, Translations.get_subroutine_uniform_location, Procs.get_subroutine_uniform_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSubroutineUniformLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_subroutine_uniform_location!
      self.get_subroutine_uniform_location ||
        raise FunctionUnavailableError.new(Translations.get_subroutine_uniform_location)
    end

    # Checks if the OpenGL function *glGetSubroutineUniformLocation* is available.
    def get_subroutine_uniform_location?
      !get_address(369, Translations.get_subroutine_uniform_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSubroutineIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_subroutine_index
      get_proc(370, Translations.get_subroutine_index, Procs.get_subroutine_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetSubroutineIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_subroutine_index!
      self.get_subroutine_index ||
        raise FunctionUnavailableError.new(Translations.get_subroutine_index)
    end

    # Checks if the OpenGL function *glGetSubroutineIndex* is available.
    def get_subroutine_index?
      !get_address(370, Translations.get_subroutine_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveSubroutineUniformiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_active_subroutine_uniform_iv
      get_proc(371, Translations.get_active_subroutine_uniform_iv, Procs.get_active_subroutine_uniform_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveSubroutineUniformiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_active_subroutine_uniform_iv!
      self.get_active_subroutine_uniform_iv ||
        raise FunctionUnavailableError.new(Translations.get_active_subroutine_uniform_iv)
    end

    # Checks if the OpenGL function *glGetActiveSubroutineUniformiv* is available.
    def get_active_subroutine_uniform_iv?
      !get_address(371, Translations.get_active_subroutine_uniform_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveSubroutineUniformName*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_active_subroutine_uniform_name
      get_proc(372, Translations.get_active_subroutine_uniform_name, Procs.get_active_subroutine_uniform_name)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveSubroutineUniformName*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_active_subroutine_uniform_name!
      self.get_active_subroutine_uniform_name ||
        raise FunctionUnavailableError.new(Translations.get_active_subroutine_uniform_name)
    end

    # Checks if the OpenGL function *glGetActiveSubroutineUniformName* is available.
    def get_active_subroutine_uniform_name?
      !get_address(372, Translations.get_active_subroutine_uniform_name)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveSubroutineName*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_active_subroutine_name
      get_proc(373, Translations.get_active_subroutine_name, Procs.get_active_subroutine_name)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveSubroutineName*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_active_subroutine_name!
      self.get_active_subroutine_name ||
        raise FunctionUnavailableError.new(Translations.get_active_subroutine_name)
    end

    # Checks if the OpenGL function *glGetActiveSubroutineName* is available.
    def get_active_subroutine_name?
      !get_address(373, Translations.get_active_subroutine_name)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformSubroutinesuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def uniform_subroutines_uiv
      get_proc(374, Translations.uniform_subroutines_uiv, Procs.uniform_subroutines_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glUniformSubroutinesuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def uniform_subroutines_uiv!
      self.uniform_subroutines_uiv ||
        raise FunctionUnavailableError.new(Translations.uniform_subroutines_uiv)
    end

    # Checks if the OpenGL function *glUniformSubroutinesuiv* is available.
    def uniform_subroutines_uiv?
      !get_address(374, Translations.uniform_subroutines_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformSubroutineuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_uniform_subroutine_uiv
      get_proc(375, Translations.get_uniform_subroutine_uiv, Procs.get_uniform_subroutine_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetUniformSubroutineuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_uniform_subroutine_uiv!
      self.get_uniform_subroutine_uiv ||
        raise FunctionUnavailableError.new(Translations.get_uniform_subroutine_uiv)
    end

    # Checks if the OpenGL function *glGetUniformSubroutineuiv* is available.
    def get_uniform_subroutine_uiv?
      !get_address(375, Translations.get_uniform_subroutine_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramStageiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_stage_iv
      get_proc(376, Translations.get_program_stage_iv, Procs.get_program_stage_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramStageiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_stage_iv!
      self.get_program_stage_iv ||
        raise FunctionUnavailableError.new(Translations.get_program_stage_iv)
    end

    # Checks if the OpenGL function *glGetProgramStageiv* is available.
    def get_program_stage_iv?
      !get_address(376, Translations.get_program_stage_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glPatchParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def patch_parameter_i
      get_proc(377, Translations.patch_parameter_i, Procs.patch_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glPatchParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def patch_parameter_i!
      self.patch_parameter_i ||
        raise FunctionUnavailableError.new(Translations.patch_parameter_i)
    end

    # Checks if the OpenGL function *glPatchParameteri* is available.
    def patch_parameter_i?
      !get_address(377, Translations.patch_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glPatchParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def patch_parameter_fv
      get_proc(378, Translations.patch_parameter_fv, Procs.patch_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glPatchParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def patch_parameter_fv!
      self.patch_parameter_fv ||
        raise FunctionUnavailableError.new(Translations.patch_parameter_fv)
    end

    # Checks if the OpenGL function *glPatchParameterfv* is available.
    def patch_parameter_fv?
      !get_address(378, Translations.patch_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_transform_feedback
      get_proc(379, Translations.bind_transform_feedback, Procs.bind_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_transform_feedback!
      self.bind_transform_feedback ||
        raise FunctionUnavailableError.new(Translations.bind_transform_feedback)
    end

    # Checks if the OpenGL function *glBindTransformFeedback* is available.
    def bind_transform_feedback?
      !get_address(379, Translations.bind_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteTransformFeedbacks*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_transform_feedbacks
      get_proc(380, Translations.delete_transform_feedbacks, Procs.delete_transform_feedbacks)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteTransformFeedbacks*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_transform_feedbacks!
      self.delete_transform_feedbacks ||
        raise FunctionUnavailableError.new(Translations.delete_transform_feedbacks)
    end

    # Checks if the OpenGL function *glDeleteTransformFeedbacks* is available.
    def delete_transform_feedbacks?
      !get_address(380, Translations.delete_transform_feedbacks)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenTransformFeedbacks*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def gen_transform_feedbacks
      get_proc(381, Translations.gen_transform_feedbacks, Procs.gen_transform_feedbacks)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenTransformFeedbacks*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def gen_transform_feedbacks!
      self.gen_transform_feedbacks ||
        raise FunctionUnavailableError.new(Translations.gen_transform_feedbacks)
    end

    # Checks if the OpenGL function *glGenTransformFeedbacks* is available.
    def gen_transform_feedbacks?
      !get_address(381, Translations.gen_transform_feedbacks)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_transform_feedback
      get_proc(382, Translations.is_transform_feedback, Procs.is_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_transform_feedback!
      self.is_transform_feedback ||
        raise FunctionUnavailableError.new(Translations.is_transform_feedback)
    end

    # Checks if the OpenGL function *glIsTransformFeedback* is available.
    def is_transform_feedback?
      !get_address(382, Translations.is_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glPauseTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def pause_transform_feedback
      get_proc(383, Translations.pause_transform_feedback, Procs.pause_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glPauseTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def pause_transform_feedback!
      self.pause_transform_feedback ||
        raise FunctionUnavailableError.new(Translations.pause_transform_feedback)
    end

    # Checks if the OpenGL function *glPauseTransformFeedback* is available.
    def pause_transform_feedback?
      !get_address(383, Translations.pause_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glResumeTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def resume_transform_feedback
      get_proc(384, Translations.resume_transform_feedback, Procs.resume_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glResumeTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def resume_transform_feedback!
      self.resume_transform_feedback ||
        raise FunctionUnavailableError.new(Translations.resume_transform_feedback)
    end

    # Checks if the OpenGL function *glResumeTransformFeedback* is available.
    def resume_transform_feedback?
      !get_address(384, Translations.resume_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_transform_feedback
      get_proc(385, Translations.draw_transform_feedback, Procs.draw_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawTransformFeedback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_transform_feedback!
      self.draw_transform_feedback ||
        raise FunctionUnavailableError.new(Translations.draw_transform_feedback)
    end

    # Checks if the OpenGL function *glDrawTransformFeedback* is available.
    def draw_transform_feedback?
      !get_address(385, Translations.draw_transform_feedback)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawTransformFeedbackStream*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_transform_feedback_stream
      get_proc(386, Translations.draw_transform_feedback_stream, Procs.draw_transform_feedback_stream)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawTransformFeedbackStream*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_transform_feedback_stream!
      self.draw_transform_feedback_stream ||
        raise FunctionUnavailableError.new(Translations.draw_transform_feedback_stream)
    end

    # Checks if the OpenGL function *glDrawTransformFeedbackStream* is available.
    def draw_transform_feedback_stream?
      !get_address(386, Translations.draw_transform_feedback_stream)
    end

    # Retrieves a `Proc` for the OpenGL function *glBeginQueryIndexed*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def begin_query_indexed
      get_proc(387, Translations.begin_query_indexed, Procs.begin_query_indexed)
    end

    # Retrieves a `Proc` for the OpenGL function *glBeginQueryIndexed*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def begin_query_indexed!
      self.begin_query_indexed ||
        raise FunctionUnavailableError.new(Translations.begin_query_indexed)
    end

    # Checks if the OpenGL function *glBeginQueryIndexed* is available.
    def begin_query_indexed?
      !get_address(387, Translations.begin_query_indexed)
    end

    # Retrieves a `Proc` for the OpenGL function *glEndQueryIndexed*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def end_query_indexed
      get_proc(388, Translations.end_query_indexed, Procs.end_query_indexed)
    end

    # Retrieves a `Proc` for the OpenGL function *glEndQueryIndexed*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def end_query_indexed!
      self.end_query_indexed ||
        raise FunctionUnavailableError.new(Translations.end_query_indexed)
    end

    # Checks if the OpenGL function *glEndQueryIndexed* is available.
    def end_query_indexed?
      !get_address(388, Translations.end_query_indexed)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryIndexediv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_query_indexed_iv
      get_proc(389, Translations.get_query_indexed_iv, Procs.get_query_indexed_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryIndexediv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_query_indexed_iv!
      self.get_query_indexed_iv ||
        raise FunctionUnavailableError.new(Translations.get_query_indexed_iv)
    end

    # Checks if the OpenGL function *glGetQueryIndexediv* is available.
    def get_query_indexed_iv?
      !get_address(389, Translations.get_query_indexed_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glReleaseShaderCompiler*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def release_shader_compiler
      get_proc(390, Translations.release_shader_compiler, Procs.release_shader_compiler)
    end

    # Retrieves a `Proc` for the OpenGL function *glReleaseShaderCompiler*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def release_shader_compiler!
      self.release_shader_compiler ||
        raise FunctionUnavailableError.new(Translations.release_shader_compiler)
    end

    # Checks if the OpenGL function *glReleaseShaderCompiler* is available.
    def release_shader_compiler?
      !get_address(390, Translations.release_shader_compiler)
    end

    # Retrieves a `Proc` for the OpenGL function *glShaderBinary*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def shader_binary
      get_proc(391, Translations.shader_binary, Procs.shader_binary)
    end

    # Retrieves a `Proc` for the OpenGL function *glShaderBinary*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def shader_binary!
      self.shader_binary ||
        raise FunctionUnavailableError.new(Translations.shader_binary)
    end

    # Checks if the OpenGL function *glShaderBinary* is available.
    def shader_binary?
      !get_address(391, Translations.shader_binary)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetShaderPrecisionFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_shader_precision_format
      get_proc(392, Translations.get_shader_precision_format, Procs.get_shader_precision_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetShaderPrecisionFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_shader_precision_format!
      self.get_shader_precision_format ||
        raise FunctionUnavailableError.new(Translations.get_shader_precision_format)
    end

    # Checks if the OpenGL function *glGetShaderPrecisionFormat* is available.
    def get_shader_precision_format?
      !get_address(392, Translations.get_shader_precision_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthRangef*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def depth_range_f
      get_proc(393, Translations.depth_range_f, Procs.depth_range_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthRangef*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def depth_range_f!
      self.depth_range_f ||
        raise FunctionUnavailableError.new(Translations.depth_range_f)
    end

    # Checks if the OpenGL function *glDepthRangef* is available.
    def depth_range_f?
      !get_address(393, Translations.depth_range_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearDepthf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_depth_f
      get_proc(394, Translations.clear_depth_f, Procs.clear_depth_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearDepthf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_depth_f!
      self.clear_depth_f ||
        raise FunctionUnavailableError.new(Translations.clear_depth_f)
    end

    # Checks if the OpenGL function *glClearDepthf* is available.
    def clear_depth_f?
      !get_address(394, Translations.clear_depth_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramBinary*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_binary
      get_proc(395, Translations.get_program_binary, Procs.get_program_binary)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramBinary*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_binary!
      self.get_program_binary ||
        raise FunctionUnavailableError.new(Translations.get_program_binary)
    end

    # Checks if the OpenGL function *glGetProgramBinary* is available.
    def get_program_binary?
      !get_address(395, Translations.get_program_binary)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramBinary*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_binary
      get_proc(396, Translations.program_binary, Procs.program_binary)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramBinary*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_binary!
      self.program_binary ||
        raise FunctionUnavailableError.new(Translations.program_binary)
    end

    # Checks if the OpenGL function *glProgramBinary* is available.
    def program_binary?
      !get_address(396, Translations.program_binary)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_parameter_i
      get_proc(397, Translations.program_parameter_i, Procs.program_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_parameter_i!
      self.program_parameter_i ||
        raise FunctionUnavailableError.new(Translations.program_parameter_i)
    end

    # Checks if the OpenGL function *glProgramParameteri* is available.
    def program_parameter_i?
      !get_address(397, Translations.program_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glUseProgramStages*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def use_program_stages
      get_proc(398, Translations.use_program_stages, Procs.use_program_stages)
    end

    # Retrieves a `Proc` for the OpenGL function *glUseProgramStages*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def use_program_stages!
      self.use_program_stages ||
        raise FunctionUnavailableError.new(Translations.use_program_stages)
    end

    # Checks if the OpenGL function *glUseProgramStages* is available.
    def use_program_stages?
      !get_address(398, Translations.use_program_stages)
    end

    # Retrieves a `Proc` for the OpenGL function *glActiveShaderProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def active_shader_program
      get_proc(399, Translations.active_shader_program, Procs.active_shader_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glActiveShaderProgram*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def active_shader_program!
      self.active_shader_program ||
        raise FunctionUnavailableError.new(Translations.active_shader_program)
    end

    # Checks if the OpenGL function *glActiveShaderProgram* is available.
    def active_shader_program?
      !get_address(399, Translations.active_shader_program)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateShaderProgramv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_shader_program_v
      get_proc(400, Translations.create_shader_program_v, Procs.create_shader_program_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateShaderProgramv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_shader_program_v!
      self.create_shader_program_v ||
        raise FunctionUnavailableError.new(Translations.create_shader_program_v)
    end

    # Checks if the OpenGL function *glCreateShaderProgramv* is available.
    def create_shader_program_v?
      !get_address(400, Translations.create_shader_program_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindProgramPipeline*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_program_pipeline
      get_proc(401, Translations.bind_program_pipeline, Procs.bind_program_pipeline)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindProgramPipeline*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_program_pipeline!
      self.bind_program_pipeline ||
        raise FunctionUnavailableError.new(Translations.bind_program_pipeline)
    end

    # Checks if the OpenGL function *glBindProgramPipeline* is available.
    def bind_program_pipeline?
      !get_address(401, Translations.bind_program_pipeline)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteProgramPipelines*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def delete_program_pipelines
      get_proc(402, Translations.delete_program_pipelines, Procs.delete_program_pipelines)
    end

    # Retrieves a `Proc` for the OpenGL function *glDeleteProgramPipelines*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def delete_program_pipelines!
      self.delete_program_pipelines ||
        raise FunctionUnavailableError.new(Translations.delete_program_pipelines)
    end

    # Checks if the OpenGL function *glDeleteProgramPipelines* is available.
    def delete_program_pipelines?
      !get_address(402, Translations.delete_program_pipelines)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenProgramPipelines*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def gen_program_pipelines
      get_proc(403, Translations.gen_program_pipelines, Procs.gen_program_pipelines)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenProgramPipelines*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def gen_program_pipelines!
      self.gen_program_pipelines ||
        raise FunctionUnavailableError.new(Translations.gen_program_pipelines)
    end

    # Checks if the OpenGL function *glGenProgramPipelines* is available.
    def gen_program_pipelines?
      !get_address(403, Translations.gen_program_pipelines)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsProgramPipeline*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def is_program_pipeline
      get_proc(404, Translations.is_program_pipeline, Procs.is_program_pipeline)
    end

    # Retrieves a `Proc` for the OpenGL function *glIsProgramPipeline*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def is_program_pipeline!
      self.is_program_pipeline ||
        raise FunctionUnavailableError.new(Translations.is_program_pipeline)
    end

    # Checks if the OpenGL function *glIsProgramPipeline* is available.
    def is_program_pipeline?
      !get_address(404, Translations.is_program_pipeline)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramPipelineiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_pipeline_iv
      get_proc(405, Translations.get_program_pipeline_iv, Procs.get_program_pipeline_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramPipelineiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_pipeline_iv!
      self.get_program_pipeline_iv ||
        raise FunctionUnavailableError.new(Translations.get_program_pipeline_iv)
    end

    # Checks if the OpenGL function *glGetProgramPipelineiv* is available.
    def get_program_pipeline_iv?
      !get_address(405, Translations.get_program_pipeline_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_1i
      get_proc(406, Translations.program_uniform_1i, Procs.program_uniform_1i)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_1i!
      self.program_uniform_1i ||
        raise FunctionUnavailableError.new(Translations.program_uniform_1i)
    end

    # Checks if the OpenGL function *glProgramUniform1i* is available.
    def program_uniform_1i?
      !get_address(406, Translations.program_uniform_1i)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_1iv
      get_proc(407, Translations.program_uniform_1iv, Procs.program_uniform_1iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_1iv!
      self.program_uniform_1iv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_1iv)
    end

    # Checks if the OpenGL function *glProgramUniform1iv* is available.
    def program_uniform_1iv?
      !get_address(407, Translations.program_uniform_1iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_1f
      get_proc(408, Translations.program_uniform_1f, Procs.program_uniform_1f)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_1f!
      self.program_uniform_1f ||
        raise FunctionUnavailableError.new(Translations.program_uniform_1f)
    end

    # Checks if the OpenGL function *glProgramUniform1f* is available.
    def program_uniform_1f?
      !get_address(408, Translations.program_uniform_1f)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_1fv
      get_proc(409, Translations.program_uniform_1fv, Procs.program_uniform_1fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_1fv!
      self.program_uniform_1fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_1fv)
    end

    # Checks if the OpenGL function *glProgramUniform1fv* is available.
    def program_uniform_1fv?
      !get_address(409, Translations.program_uniform_1fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_1d
      get_proc(410, Translations.program_uniform_1d, Procs.program_uniform_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_1d!
      self.program_uniform_1d ||
        raise FunctionUnavailableError.new(Translations.program_uniform_1d)
    end

    # Checks if the OpenGL function *glProgramUniform1d* is available.
    def program_uniform_1d?
      !get_address(410, Translations.program_uniform_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_1dv
      get_proc(411, Translations.program_uniform_1dv, Procs.program_uniform_1dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_1dv!
      self.program_uniform_1dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_1dv)
    end

    # Checks if the OpenGL function *glProgramUniform1dv* is available.
    def program_uniform_1dv?
      !get_address(411, Translations.program_uniform_1dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_1ui
      get_proc(412, Translations.program_uniform_1ui, Procs.program_uniform_1ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_1ui!
      self.program_uniform_1ui ||
        raise FunctionUnavailableError.new(Translations.program_uniform_1ui)
    end

    # Checks if the OpenGL function *glProgramUniform1ui* is available.
    def program_uniform_1ui?
      !get_address(412, Translations.program_uniform_1ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_1uiv
      get_proc(413, Translations.program_uniform_1uiv, Procs.program_uniform_1uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform1uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_1uiv!
      self.program_uniform_1uiv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_1uiv)
    end

    # Checks if the OpenGL function *glProgramUniform1uiv* is available.
    def program_uniform_1uiv?
      !get_address(413, Translations.program_uniform_1uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_2i
      get_proc(414, Translations.program_uniform_2i, Procs.program_uniform_2i)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_2i!
      self.program_uniform_2i ||
        raise FunctionUnavailableError.new(Translations.program_uniform_2i)
    end

    # Checks if the OpenGL function *glProgramUniform2i* is available.
    def program_uniform_2i?
      !get_address(414, Translations.program_uniform_2i)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_2iv
      get_proc(415, Translations.program_uniform_2iv, Procs.program_uniform_2iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_2iv!
      self.program_uniform_2iv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_2iv)
    end

    # Checks if the OpenGL function *glProgramUniform2iv* is available.
    def program_uniform_2iv?
      !get_address(415, Translations.program_uniform_2iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_2f
      get_proc(416, Translations.program_uniform_2f, Procs.program_uniform_2f)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_2f!
      self.program_uniform_2f ||
        raise FunctionUnavailableError.new(Translations.program_uniform_2f)
    end

    # Checks if the OpenGL function *glProgramUniform2f* is available.
    def program_uniform_2f?
      !get_address(416, Translations.program_uniform_2f)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_2fv
      get_proc(417, Translations.program_uniform_2fv, Procs.program_uniform_2fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_2fv!
      self.program_uniform_2fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_2fv)
    end

    # Checks if the OpenGL function *glProgramUniform2fv* is available.
    def program_uniform_2fv?
      !get_address(417, Translations.program_uniform_2fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_2d
      get_proc(418, Translations.program_uniform_2d, Procs.program_uniform_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_2d!
      self.program_uniform_2d ||
        raise FunctionUnavailableError.new(Translations.program_uniform_2d)
    end

    # Checks if the OpenGL function *glProgramUniform2d* is available.
    def program_uniform_2d?
      !get_address(418, Translations.program_uniform_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_2dv
      get_proc(419, Translations.program_uniform_2dv, Procs.program_uniform_2dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_2dv!
      self.program_uniform_2dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_2dv)
    end

    # Checks if the OpenGL function *glProgramUniform2dv* is available.
    def program_uniform_2dv?
      !get_address(419, Translations.program_uniform_2dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_2ui
      get_proc(420, Translations.program_uniform_2ui, Procs.program_uniform_2ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_2ui!
      self.program_uniform_2ui ||
        raise FunctionUnavailableError.new(Translations.program_uniform_2ui)
    end

    # Checks if the OpenGL function *glProgramUniform2ui* is available.
    def program_uniform_2ui?
      !get_address(420, Translations.program_uniform_2ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_2uiv
      get_proc(421, Translations.program_uniform_2uiv, Procs.program_uniform_2uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform2uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_2uiv!
      self.program_uniform_2uiv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_2uiv)
    end

    # Checks if the OpenGL function *glProgramUniform2uiv* is available.
    def program_uniform_2uiv?
      !get_address(421, Translations.program_uniform_2uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_3i
      get_proc(422, Translations.program_uniform_3i, Procs.program_uniform_3i)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_3i!
      self.program_uniform_3i ||
        raise FunctionUnavailableError.new(Translations.program_uniform_3i)
    end

    # Checks if the OpenGL function *glProgramUniform3i* is available.
    def program_uniform_3i?
      !get_address(422, Translations.program_uniform_3i)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_3iv
      get_proc(423, Translations.program_uniform_3iv, Procs.program_uniform_3iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_3iv!
      self.program_uniform_3iv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_3iv)
    end

    # Checks if the OpenGL function *glProgramUniform3iv* is available.
    def program_uniform_3iv?
      !get_address(423, Translations.program_uniform_3iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_3f
      get_proc(424, Translations.program_uniform_3f, Procs.program_uniform_3f)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_3f!
      self.program_uniform_3f ||
        raise FunctionUnavailableError.new(Translations.program_uniform_3f)
    end

    # Checks if the OpenGL function *glProgramUniform3f* is available.
    def program_uniform_3f?
      !get_address(424, Translations.program_uniform_3f)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_3fv
      get_proc(425, Translations.program_uniform_3fv, Procs.program_uniform_3fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_3fv!
      self.program_uniform_3fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_3fv)
    end

    # Checks if the OpenGL function *glProgramUniform3fv* is available.
    def program_uniform_3fv?
      !get_address(425, Translations.program_uniform_3fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_3d
      get_proc(426, Translations.program_uniform_3d, Procs.program_uniform_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_3d!
      self.program_uniform_3d ||
        raise FunctionUnavailableError.new(Translations.program_uniform_3d)
    end

    # Checks if the OpenGL function *glProgramUniform3d* is available.
    def program_uniform_3d?
      !get_address(426, Translations.program_uniform_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_3dv
      get_proc(427, Translations.program_uniform_3dv, Procs.program_uniform_3dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_3dv!
      self.program_uniform_3dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_3dv)
    end

    # Checks if the OpenGL function *glProgramUniform3dv* is available.
    def program_uniform_3dv?
      !get_address(427, Translations.program_uniform_3dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_3ui
      get_proc(428, Translations.program_uniform_3ui, Procs.program_uniform_3ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_3ui!
      self.program_uniform_3ui ||
        raise FunctionUnavailableError.new(Translations.program_uniform_3ui)
    end

    # Checks if the OpenGL function *glProgramUniform3ui* is available.
    def program_uniform_3ui?
      !get_address(428, Translations.program_uniform_3ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_3uiv
      get_proc(429, Translations.program_uniform_3uiv, Procs.program_uniform_3uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform3uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_3uiv!
      self.program_uniform_3uiv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_3uiv)
    end

    # Checks if the OpenGL function *glProgramUniform3uiv* is available.
    def program_uniform_3uiv?
      !get_address(429, Translations.program_uniform_3uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_4i
      get_proc(430, Translations.program_uniform_4i, Procs.program_uniform_4i)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4i*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_4i!
      self.program_uniform_4i ||
        raise FunctionUnavailableError.new(Translations.program_uniform_4i)
    end

    # Checks if the OpenGL function *glProgramUniform4i* is available.
    def program_uniform_4i?
      !get_address(430, Translations.program_uniform_4i)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_4iv
      get_proc(431, Translations.program_uniform_4iv, Procs.program_uniform_4iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_4iv!
      self.program_uniform_4iv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_4iv)
    end

    # Checks if the OpenGL function *glProgramUniform4iv* is available.
    def program_uniform_4iv?
      !get_address(431, Translations.program_uniform_4iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_4f
      get_proc(432, Translations.program_uniform_4f, Procs.program_uniform_4f)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4f*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_4f!
      self.program_uniform_4f ||
        raise FunctionUnavailableError.new(Translations.program_uniform_4f)
    end

    # Checks if the OpenGL function *glProgramUniform4f* is available.
    def program_uniform_4f?
      !get_address(432, Translations.program_uniform_4f)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_4fv
      get_proc(433, Translations.program_uniform_4fv, Procs.program_uniform_4fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_4fv!
      self.program_uniform_4fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_4fv)
    end

    # Checks if the OpenGL function *glProgramUniform4fv* is available.
    def program_uniform_4fv?
      !get_address(433, Translations.program_uniform_4fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_4d
      get_proc(434, Translations.program_uniform_4d, Procs.program_uniform_4d)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_4d!
      self.program_uniform_4d ||
        raise FunctionUnavailableError.new(Translations.program_uniform_4d)
    end

    # Checks if the OpenGL function *glProgramUniform4d* is available.
    def program_uniform_4d?
      !get_address(434, Translations.program_uniform_4d)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_4dv
      get_proc(435, Translations.program_uniform_4dv, Procs.program_uniform_4dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_4dv!
      self.program_uniform_4dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_4dv)
    end

    # Checks if the OpenGL function *glProgramUniform4dv* is available.
    def program_uniform_4dv?
      !get_address(435, Translations.program_uniform_4dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_4ui
      get_proc(436, Translations.program_uniform_4ui, Procs.program_uniform_4ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4ui*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_4ui!
      self.program_uniform_4ui ||
        raise FunctionUnavailableError.new(Translations.program_uniform_4ui)
    end

    # Checks if the OpenGL function *glProgramUniform4ui* is available.
    def program_uniform_4ui?
      !get_address(436, Translations.program_uniform_4ui)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_4uiv
      get_proc(437, Translations.program_uniform_4uiv, Procs.program_uniform_4uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniform4uiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_4uiv!
      self.program_uniform_4uiv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_4uiv)
    end

    # Checks if the OpenGL function *glProgramUniform4uiv* is available.
    def program_uniform_4uiv?
      !get_address(437, Translations.program_uniform_4uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix2_fv
      get_proc(438, Translations.program_uniform_matrix2_fv, Procs.program_uniform_matrix2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix2_fv!
      self.program_uniform_matrix2_fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix2_fv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix2fv* is available.
    def program_uniform_matrix2_fv?
      !get_address(438, Translations.program_uniform_matrix2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix3_fv
      get_proc(439, Translations.program_uniform_matrix3_fv, Procs.program_uniform_matrix3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix3_fv!
      self.program_uniform_matrix3_fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix3_fv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix3fv* is available.
    def program_uniform_matrix3_fv?
      !get_address(439, Translations.program_uniform_matrix3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix4_fv
      get_proc(440, Translations.program_uniform_matrix4_fv, Procs.program_uniform_matrix4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix4_fv!
      self.program_uniform_matrix4_fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix4_fv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix4fv* is available.
    def program_uniform_matrix4_fv?
      !get_address(440, Translations.program_uniform_matrix4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix2_dv
      get_proc(441, Translations.program_uniform_matrix2_dv, Procs.program_uniform_matrix2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix2_dv!
      self.program_uniform_matrix2_dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix2_dv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix2dv* is available.
    def program_uniform_matrix2_dv?
      !get_address(441, Translations.program_uniform_matrix2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix3_dv
      get_proc(442, Translations.program_uniform_matrix3_dv, Procs.program_uniform_matrix3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix3_dv!
      self.program_uniform_matrix3_dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix3_dv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix3dv* is available.
    def program_uniform_matrix3_dv?
      !get_address(442, Translations.program_uniform_matrix3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix4_dv
      get_proc(443, Translations.program_uniform_matrix4_dv, Procs.program_uniform_matrix4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix4_dv!
      self.program_uniform_matrix4_dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix4_dv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix4dv* is available.
    def program_uniform_matrix4_dv?
      !get_address(443, Translations.program_uniform_matrix4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2x3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix2x3_fv
      get_proc(444, Translations.program_uniform_matrix2x3_fv, Procs.program_uniform_matrix2x3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2x3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix2x3_fv!
      self.program_uniform_matrix2x3_fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix2x3_fv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix2x3fv* is available.
    def program_uniform_matrix2x3_fv?
      !get_address(444, Translations.program_uniform_matrix2x3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3x2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix3x2_fv
      get_proc(445, Translations.program_uniform_matrix3x2_fv, Procs.program_uniform_matrix3x2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3x2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix3x2_fv!
      self.program_uniform_matrix3x2_fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix3x2_fv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix3x2fv* is available.
    def program_uniform_matrix3x2_fv?
      !get_address(445, Translations.program_uniform_matrix3x2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2x4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix2x4_fv
      get_proc(446, Translations.program_uniform_matrix2x4_fv, Procs.program_uniform_matrix2x4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2x4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix2x4_fv!
      self.program_uniform_matrix2x4_fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix2x4_fv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix2x4fv* is available.
    def program_uniform_matrix2x4_fv?
      !get_address(446, Translations.program_uniform_matrix2x4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4x2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix4x2_fv
      get_proc(447, Translations.program_uniform_matrix4x2_fv, Procs.program_uniform_matrix4x2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4x2fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix4x2_fv!
      self.program_uniform_matrix4x2_fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix4x2_fv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix4x2fv* is available.
    def program_uniform_matrix4x2_fv?
      !get_address(447, Translations.program_uniform_matrix4x2_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3x4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix3x4_fv
      get_proc(448, Translations.program_uniform_matrix3x4_fv, Procs.program_uniform_matrix3x4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3x4fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix3x4_fv!
      self.program_uniform_matrix3x4_fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix3x4_fv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix3x4fv* is available.
    def program_uniform_matrix3x4_fv?
      !get_address(448, Translations.program_uniform_matrix3x4_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4x3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix4x3_fv
      get_proc(449, Translations.program_uniform_matrix4x3_fv, Procs.program_uniform_matrix4x3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4x3fv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix4x3_fv!
      self.program_uniform_matrix4x3_fv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix4x3_fv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix4x3fv* is available.
    def program_uniform_matrix4x3_fv?
      !get_address(449, Translations.program_uniform_matrix4x3_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2x3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix2x3_dv
      get_proc(450, Translations.program_uniform_matrix2x3_dv, Procs.program_uniform_matrix2x3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2x3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix2x3_dv!
      self.program_uniform_matrix2x3_dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix2x3_dv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix2x3dv* is available.
    def program_uniform_matrix2x3_dv?
      !get_address(450, Translations.program_uniform_matrix2x3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3x2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix3x2_dv
      get_proc(451, Translations.program_uniform_matrix3x2_dv, Procs.program_uniform_matrix3x2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3x2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix3x2_dv!
      self.program_uniform_matrix3x2_dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix3x2_dv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix3x2dv* is available.
    def program_uniform_matrix3x2_dv?
      !get_address(451, Translations.program_uniform_matrix3x2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2x4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix2x4_dv
      get_proc(452, Translations.program_uniform_matrix2x4_dv, Procs.program_uniform_matrix2x4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix2x4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix2x4_dv!
      self.program_uniform_matrix2x4_dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix2x4_dv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix2x4dv* is available.
    def program_uniform_matrix2x4_dv?
      !get_address(452, Translations.program_uniform_matrix2x4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4x2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix4x2_dv
      get_proc(453, Translations.program_uniform_matrix4x2_dv, Procs.program_uniform_matrix4x2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4x2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix4x2_dv!
      self.program_uniform_matrix4x2_dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix4x2_dv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix4x2dv* is available.
    def program_uniform_matrix4x2_dv?
      !get_address(453, Translations.program_uniform_matrix4x2_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3x4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix3x4_dv
      get_proc(454, Translations.program_uniform_matrix3x4_dv, Procs.program_uniform_matrix3x4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix3x4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix3x4_dv!
      self.program_uniform_matrix3x4_dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix3x4_dv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix3x4dv* is available.
    def program_uniform_matrix3x4_dv?
      !get_address(454, Translations.program_uniform_matrix3x4_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4x3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def program_uniform_matrix4x3_dv
      get_proc(455, Translations.program_uniform_matrix4x3_dv, Procs.program_uniform_matrix4x3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glProgramUniformMatrix4x3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def program_uniform_matrix4x3_dv!
      self.program_uniform_matrix4x3_dv ||
        raise FunctionUnavailableError.new(Translations.program_uniform_matrix4x3_dv)
    end

    # Checks if the OpenGL function *glProgramUniformMatrix4x3dv* is available.
    def program_uniform_matrix4x3_dv?
      !get_address(455, Translations.program_uniform_matrix4x3_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glValidateProgramPipeline*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def validate_program_pipeline
      get_proc(456, Translations.validate_program_pipeline, Procs.validate_program_pipeline)
    end

    # Retrieves a `Proc` for the OpenGL function *glValidateProgramPipeline*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def validate_program_pipeline!
      self.validate_program_pipeline ||
        raise FunctionUnavailableError.new(Translations.validate_program_pipeline)
    end

    # Checks if the OpenGL function *glValidateProgramPipeline* is available.
    def validate_program_pipeline?
      !get_address(456, Translations.validate_program_pipeline)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramPipelineInfoLog*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_pipeline_info_log
      get_proc(457, Translations.get_program_pipeline_info_log, Procs.get_program_pipeline_info_log)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramPipelineInfoLog*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_pipeline_info_log!
      self.get_program_pipeline_info_log ||
        raise FunctionUnavailableError.new(Translations.get_program_pipeline_info_log)
    end

    # Checks if the OpenGL function *glGetProgramPipelineInfoLog* is available.
    def get_program_pipeline_info_log?
      !get_address(457, Translations.get_program_pipeline_info_log)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL1d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_l_1d
      get_proc(458, Translations.vertex_attrib_l_1d, Procs.vertex_attrib_l_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL1d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_l_1d!
      self.vertex_attrib_l_1d ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_l_1d)
    end

    # Checks if the OpenGL function *glVertexAttribL1d* is available.
    def vertex_attrib_l_1d?
      !get_address(458, Translations.vertex_attrib_l_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL2d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_l_2d
      get_proc(459, Translations.vertex_attrib_l_2d, Procs.vertex_attrib_l_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL2d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_l_2d!
      self.vertex_attrib_l_2d ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_l_2d)
    end

    # Checks if the OpenGL function *glVertexAttribL2d* is available.
    def vertex_attrib_l_2d?
      !get_address(459, Translations.vertex_attrib_l_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL3d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_l_3d
      get_proc(460, Translations.vertex_attrib_l_3d, Procs.vertex_attrib_l_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL3d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_l_3d!
      self.vertex_attrib_l_3d ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_l_3d)
    end

    # Checks if the OpenGL function *glVertexAttribL3d* is available.
    def vertex_attrib_l_3d?
      !get_address(460, Translations.vertex_attrib_l_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL4d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_l_4d
      get_proc(461, Translations.vertex_attrib_l_4d, Procs.vertex_attrib_l_4d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL4d*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_l_4d!
      self.vertex_attrib_l_4d ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_l_4d)
    end

    # Checks if the OpenGL function *glVertexAttribL4d* is available.
    def vertex_attrib_l_4d?
      !get_address(461, Translations.vertex_attrib_l_4d)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL1dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_l_1dv
      get_proc(462, Translations.vertex_attrib_l_1dv, Procs.vertex_attrib_l_1dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL1dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_l_1dv!
      self.vertex_attrib_l_1dv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_l_1dv)
    end

    # Checks if the OpenGL function *glVertexAttribL1dv* is available.
    def vertex_attrib_l_1dv?
      !get_address(462, Translations.vertex_attrib_l_1dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_l_2dv
      get_proc(463, Translations.vertex_attrib_l_2dv, Procs.vertex_attrib_l_2dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL2dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_l_2dv!
      self.vertex_attrib_l_2dv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_l_2dv)
    end

    # Checks if the OpenGL function *glVertexAttribL2dv* is available.
    def vertex_attrib_l_2dv?
      !get_address(463, Translations.vertex_attrib_l_2dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_l_3dv
      get_proc(464, Translations.vertex_attrib_l_3dv, Procs.vertex_attrib_l_3dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL3dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_l_3dv!
      self.vertex_attrib_l_3dv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_l_3dv)
    end

    # Checks if the OpenGL function *glVertexAttribL3dv* is available.
    def vertex_attrib_l_3dv?
      !get_address(464, Translations.vertex_attrib_l_3dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_l_4dv
      get_proc(465, Translations.vertex_attrib_l_4dv, Procs.vertex_attrib_l_4dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribL4dv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_l_4dv!
      self.vertex_attrib_l_4dv ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_l_4dv)
    end

    # Checks if the OpenGL function *glVertexAttribL4dv* is available.
    def vertex_attrib_l_4dv?
      !get_address(465, Translations.vertex_attrib_l_4dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribLPointer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_l_pointer
      get_proc(466, Translations.vertex_attrib_l_pointer, Procs.vertex_attrib_l_pointer)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribLPointer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_l_pointer!
      self.vertex_attrib_l_pointer ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_l_pointer)
    end

    # Checks if the OpenGL function *glVertexAttribLPointer* is available.
    def vertex_attrib_l_pointer?
      !get_address(466, Translations.vertex_attrib_l_pointer)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribLdv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_vertex_attrib_l_dv
      get_proc(467, Translations.get_vertex_attrib_l_dv, Procs.get_vertex_attrib_l_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexAttribLdv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_vertex_attrib_l_dv!
      self.get_vertex_attrib_l_dv ||
        raise FunctionUnavailableError.new(Translations.get_vertex_attrib_l_dv)
    end

    # Checks if the OpenGL function *glGetVertexAttribLdv* is available.
    def get_vertex_attrib_l_dv?
      !get_address(467, Translations.get_vertex_attrib_l_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glViewportArrayv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def viewport_array_v
      get_proc(468, Translations.viewport_array_v, Procs.viewport_array_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glViewportArrayv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def viewport_array_v!
      self.viewport_array_v ||
        raise FunctionUnavailableError.new(Translations.viewport_array_v)
    end

    # Checks if the OpenGL function *glViewportArrayv* is available.
    def viewport_array_v?
      !get_address(468, Translations.viewport_array_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glViewportIndexedf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def viewport_indexed_f
      get_proc(469, Translations.viewport_indexed_f, Procs.viewport_indexed_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glViewportIndexedf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def viewport_indexed_f!
      self.viewport_indexed_f ||
        raise FunctionUnavailableError.new(Translations.viewport_indexed_f)
    end

    # Checks if the OpenGL function *glViewportIndexedf* is available.
    def viewport_indexed_f?
      !get_address(469, Translations.viewport_indexed_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glViewportIndexedfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def viewport_indexed_fv
      get_proc(470, Translations.viewport_indexed_fv, Procs.viewport_indexed_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glViewportIndexedfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def viewport_indexed_fv!
      self.viewport_indexed_fv ||
        raise FunctionUnavailableError.new(Translations.viewport_indexed_fv)
    end

    # Checks if the OpenGL function *glViewportIndexedfv* is available.
    def viewport_indexed_fv?
      !get_address(470, Translations.viewport_indexed_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glScissorArrayv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def scissor_array_v
      get_proc(471, Translations.scissor_array_v, Procs.scissor_array_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glScissorArrayv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def scissor_array_v!
      self.scissor_array_v ||
        raise FunctionUnavailableError.new(Translations.scissor_array_v)
    end

    # Checks if the OpenGL function *glScissorArrayv* is available.
    def scissor_array_v?
      !get_address(471, Translations.scissor_array_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glScissorIndexed*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def scissor_indexed
      get_proc(472, Translations.scissor_indexed, Procs.scissor_indexed)
    end

    # Retrieves a `Proc` for the OpenGL function *glScissorIndexed*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def scissor_indexed!
      self.scissor_indexed ||
        raise FunctionUnavailableError.new(Translations.scissor_indexed)
    end

    # Checks if the OpenGL function *glScissorIndexed* is available.
    def scissor_indexed?
      !get_address(472, Translations.scissor_indexed)
    end

    # Retrieves a `Proc` for the OpenGL function *glScissorIndexedv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def scissor_indexedv
      get_proc(473, Translations.scissor_indexedv, Procs.scissor_indexedv)
    end

    # Retrieves a `Proc` for the OpenGL function *glScissorIndexedv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def scissor_indexedv!
      self.scissor_indexedv ||
        raise FunctionUnavailableError.new(Translations.scissor_indexedv)
    end

    # Checks if the OpenGL function *glScissorIndexedv* is available.
    def scissor_indexedv?
      !get_address(473, Translations.scissor_indexedv)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthRangeArrayv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def depth_range_array_v
      get_proc(474, Translations.depth_range_array_v, Procs.depth_range_array_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthRangeArrayv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def depth_range_array_v!
      self.depth_range_array_v ||
        raise FunctionUnavailableError.new(Translations.depth_range_array_v)
    end

    # Checks if the OpenGL function *glDepthRangeArrayv* is available.
    def depth_range_array_v?
      !get_address(474, Translations.depth_range_array_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthRangeIndexed*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def depth_range_indexed
      get_proc(475, Translations.depth_range_indexed, Procs.depth_range_indexed)
    end

    # Retrieves a `Proc` for the OpenGL function *glDepthRangeIndexed*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def depth_range_indexed!
      self.depth_range_indexed ||
        raise FunctionUnavailableError.new(Translations.depth_range_indexed)
    end

    # Checks if the OpenGL function *glDepthRangeIndexed* is available.
    def depth_range_indexed?
      !get_address(475, Translations.depth_range_indexed)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFloati_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_float_i_v
      get_proc(476, Translations.get_float_i_v, Procs.get_float_i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFloati_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_float_i_v!
      self.get_float_i_v ||
        raise FunctionUnavailableError.new(Translations.get_float_i_v)
    end

    # Checks if the OpenGL function *glGetFloati_v* is available.
    def get_float_i_v?
      !get_address(476, Translations.get_float_i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetDoublei_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_double_i_v
      get_proc(477, Translations.get_double_i_v, Procs.get_double_i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetDoublei_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_double_i_v!
      self.get_double_i_v ||
        raise FunctionUnavailableError.new(Translations.get_double_i_v)
    end

    # Checks if the OpenGL function *glGetDoublei_v* is available.
    def get_double_i_v?
      !get_address(477, Translations.get_double_i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawArraysInstancedBaseInstance*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_arrays_instanced_base_instance
      get_proc(478, Translations.draw_arrays_instanced_base_instance, Procs.draw_arrays_instanced_base_instance)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawArraysInstancedBaseInstance*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_arrays_instanced_base_instance!
      self.draw_arrays_instanced_base_instance ||
        raise FunctionUnavailableError.new(Translations.draw_arrays_instanced_base_instance)
    end

    # Checks if the OpenGL function *glDrawArraysInstancedBaseInstance* is available.
    def draw_arrays_instanced_base_instance?
      !get_address(478, Translations.draw_arrays_instanced_base_instance)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsInstancedBaseInstance*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_elements_instanced_base_instance
      get_proc(479, Translations.draw_elements_instanced_base_instance, Procs.draw_elements_instanced_base_instance)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsInstancedBaseInstance*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_elements_instanced_base_instance!
      self.draw_elements_instanced_base_instance ||
        raise FunctionUnavailableError.new(Translations.draw_elements_instanced_base_instance)
    end

    # Checks if the OpenGL function *glDrawElementsInstancedBaseInstance* is available.
    def draw_elements_instanced_base_instance?
      !get_address(479, Translations.draw_elements_instanced_base_instance)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsInstancedBaseVertexBaseInstance*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_elements_instanced_base_vertex_base_instance
      get_proc(480, Translations.draw_elements_instanced_base_vertex_base_instance, Procs.draw_elements_instanced_base_vertex_base_instance)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawElementsInstancedBaseVertexBaseInstance*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_elements_instanced_base_vertex_base_instance!
      self.draw_elements_instanced_base_vertex_base_instance ||
        raise FunctionUnavailableError.new(Translations.draw_elements_instanced_base_vertex_base_instance)
    end

    # Checks if the OpenGL function *glDrawElementsInstancedBaseVertexBaseInstance* is available.
    def draw_elements_instanced_base_vertex_base_instance?
      !get_address(480, Translations.draw_elements_instanced_base_vertex_base_instance)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetInternalformativ*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_internalformat_iv
      get_proc(481, Translations.get_internalformat_iv, Procs.get_internalformat_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetInternalformativ*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_internalformat_iv!
      self.get_internalformat_iv ||
        raise FunctionUnavailableError.new(Translations.get_internalformat_iv)
    end

    # Checks if the OpenGL function *glGetInternalformativ* is available.
    def get_internalformat_iv?
      !get_address(481, Translations.get_internalformat_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveAtomicCounterBufferiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_active_atomic_counter_buffer_iv
      get_proc(482, Translations.get_active_atomic_counter_buffer_iv, Procs.get_active_atomic_counter_buffer_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetActiveAtomicCounterBufferiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_active_atomic_counter_buffer_iv!
      self.get_active_atomic_counter_buffer_iv ||
        raise FunctionUnavailableError.new(Translations.get_active_atomic_counter_buffer_iv)
    end

    # Checks if the OpenGL function *glGetActiveAtomicCounterBufferiv* is available.
    def get_active_atomic_counter_buffer_iv?
      !get_address(482, Translations.get_active_atomic_counter_buffer_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindImageTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_image_texture
      get_proc(483, Translations.bind_image_texture, Procs.bind_image_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindImageTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_image_texture!
      self.bind_image_texture ||
        raise FunctionUnavailableError.new(Translations.bind_image_texture)
    end

    # Checks if the OpenGL function *glBindImageTexture* is available.
    def bind_image_texture?
      !get_address(483, Translations.bind_image_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glMemoryBarrier*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def memory_barrier
      get_proc(484, Translations.memory_barrier, Procs.memory_barrier)
    end

    # Retrieves a `Proc` for the OpenGL function *glMemoryBarrier*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def memory_barrier!
      self.memory_barrier ||
        raise FunctionUnavailableError.new(Translations.memory_barrier)
    end

    # Checks if the OpenGL function *glMemoryBarrier* is available.
    def memory_barrier?
      !get_address(484, Translations.memory_barrier)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexStorage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_storage_1d
      get_proc(485, Translations.tex_storage_1d, Procs.tex_storage_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexStorage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_storage_1d!
      self.tex_storage_1d ||
        raise FunctionUnavailableError.new(Translations.tex_storage_1d)
    end

    # Checks if the OpenGL function *glTexStorage1D* is available.
    def tex_storage_1d?
      !get_address(485, Translations.tex_storage_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexStorage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_storage_2d
      get_proc(486, Translations.tex_storage_2d, Procs.tex_storage_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexStorage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_storage_2d!
      self.tex_storage_2d ||
        raise FunctionUnavailableError.new(Translations.tex_storage_2d)
    end

    # Checks if the OpenGL function *glTexStorage2D* is available.
    def tex_storage_2d?
      !get_address(486, Translations.tex_storage_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexStorage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_storage_3d
      get_proc(487, Translations.tex_storage_3d, Procs.tex_storage_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexStorage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_storage_3d!
      self.tex_storage_3d ||
        raise FunctionUnavailableError.new(Translations.tex_storage_3d)
    end

    # Checks if the OpenGL function *glTexStorage3D* is available.
    def tex_storage_3d?
      !get_address(487, Translations.tex_storage_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawTransformFeedbackInstanced*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_transform_feedback_instanced
      get_proc(488, Translations.draw_transform_feedback_instanced, Procs.draw_transform_feedback_instanced)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawTransformFeedbackInstanced*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_transform_feedback_instanced!
      self.draw_transform_feedback_instanced ||
        raise FunctionUnavailableError.new(Translations.draw_transform_feedback_instanced)
    end

    # Checks if the OpenGL function *glDrawTransformFeedbackInstanced* is available.
    def draw_transform_feedback_instanced?
      !get_address(488, Translations.draw_transform_feedback_instanced)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawTransformFeedbackStreamInstanced*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def draw_transform_feedback_stream_instanced
      get_proc(489, Translations.draw_transform_feedback_stream_instanced, Procs.draw_transform_feedback_stream_instanced)
    end

    # Retrieves a `Proc` for the OpenGL function *glDrawTransformFeedbackStreamInstanced*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def draw_transform_feedback_stream_instanced!
      self.draw_transform_feedback_stream_instanced ||
        raise FunctionUnavailableError.new(Translations.draw_transform_feedback_stream_instanced)
    end

    # Checks if the OpenGL function *glDrawTransformFeedbackStreamInstanced* is available.
    def draw_transform_feedback_stream_instanced?
      !get_address(489, Translations.draw_transform_feedback_stream_instanced)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_buffer_data
      get_proc(490, Translations.clear_buffer_data, Procs.clear_buffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_buffer_data!
      self.clear_buffer_data ||
        raise FunctionUnavailableError.new(Translations.clear_buffer_data)
    end

    # Checks if the OpenGL function *glClearBufferData* is available.
    def clear_buffer_data?
      !get_address(490, Translations.clear_buffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_buffer_sub_data
      get_proc(491, Translations.clear_buffer_sub_data, Procs.clear_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_buffer_sub_data!
      self.clear_buffer_sub_data ||
        raise FunctionUnavailableError.new(Translations.clear_buffer_sub_data)
    end

    # Checks if the OpenGL function *glClearBufferSubData* is available.
    def clear_buffer_sub_data?
      !get_address(491, Translations.clear_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glDispatchCompute*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def dispatch_compute
      get_proc(492, Translations.dispatch_compute, Procs.dispatch_compute)
    end

    # Retrieves a `Proc` for the OpenGL function *glDispatchCompute*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def dispatch_compute!
      self.dispatch_compute ||
        raise FunctionUnavailableError.new(Translations.dispatch_compute)
    end

    # Checks if the OpenGL function *glDispatchCompute* is available.
    def dispatch_compute?
      !get_address(492, Translations.dispatch_compute)
    end

    # Retrieves a `Proc` for the OpenGL function *glDispatchComputeIndirect*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def dispatch_compute_indirect
      get_proc(493, Translations.dispatch_compute_indirect, Procs.dispatch_compute_indirect)
    end

    # Retrieves a `Proc` for the OpenGL function *glDispatchComputeIndirect*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def dispatch_compute_indirect!
      self.dispatch_compute_indirect ||
        raise FunctionUnavailableError.new(Translations.dispatch_compute_indirect)
    end

    # Checks if the OpenGL function *glDispatchComputeIndirect* is available.
    def dispatch_compute_indirect?
      !get_address(493, Translations.dispatch_compute_indirect)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyImageSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_image_sub_data
      get_proc(494, Translations.copy_image_sub_data, Procs.copy_image_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyImageSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_image_sub_data!
      self.copy_image_sub_data ||
        raise FunctionUnavailableError.new(Translations.copy_image_sub_data)
    end

    # Checks if the OpenGL function *glCopyImageSubData* is available.
    def copy_image_sub_data?
      !get_address(494, Translations.copy_image_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def framebuffer_parameter_i
      get_proc(495, Translations.framebuffer_parameter_i, Procs.framebuffer_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glFramebufferParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def framebuffer_parameter_i!
      self.framebuffer_parameter_i ||
        raise FunctionUnavailableError.new(Translations.framebuffer_parameter_i)
    end

    # Checks if the OpenGL function *glFramebufferParameteri* is available.
    def framebuffer_parameter_i?
      !get_address(495, Translations.framebuffer_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFramebufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_framebuffer_parameter_iv
      get_proc(496, Translations.get_framebuffer_parameter_iv, Procs.get_framebuffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetFramebufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_framebuffer_parameter_iv!
      self.get_framebuffer_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_framebuffer_parameter_iv)
    end

    # Checks if the OpenGL function *glGetFramebufferParameteriv* is available.
    def get_framebuffer_parameter_iv?
      !get_address(496, Translations.get_framebuffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetInternalformati64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_internalformat_i64v
      get_proc(497, Translations.get_internalformat_i64v, Procs.get_internalformat_i64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetInternalformati64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_internalformat_i64v!
      self.get_internalformat_i64v ||
        raise FunctionUnavailableError.new(Translations.get_internalformat_i64v)
    end

    # Checks if the OpenGL function *glGetInternalformati64v* is available.
    def get_internalformat_i64v?
      !get_address(497, Translations.get_internalformat_i64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateTexSubImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def invalidate_tex_sub_image
      get_proc(498, Translations.invalidate_tex_sub_image, Procs.invalidate_tex_sub_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateTexSubImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def invalidate_tex_sub_image!
      self.invalidate_tex_sub_image ||
        raise FunctionUnavailableError.new(Translations.invalidate_tex_sub_image)
    end

    # Checks if the OpenGL function *glInvalidateTexSubImage* is available.
    def invalidate_tex_sub_image?
      !get_address(498, Translations.invalidate_tex_sub_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def invalidate_tex_image
      get_proc(499, Translations.invalidate_tex_image, Procs.invalidate_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def invalidate_tex_image!
      self.invalidate_tex_image ||
        raise FunctionUnavailableError.new(Translations.invalidate_tex_image)
    end

    # Checks if the OpenGL function *glInvalidateTexImage* is available.
    def invalidate_tex_image?
      !get_address(499, Translations.invalidate_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def invalidate_buffer_sub_data
      get_proc(500, Translations.invalidate_buffer_sub_data, Procs.invalidate_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def invalidate_buffer_sub_data!
      self.invalidate_buffer_sub_data ||
        raise FunctionUnavailableError.new(Translations.invalidate_buffer_sub_data)
    end

    # Checks if the OpenGL function *glInvalidateBufferSubData* is available.
    def invalidate_buffer_sub_data?
      !get_address(500, Translations.invalidate_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateBufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def invalidate_buffer_data
      get_proc(501, Translations.invalidate_buffer_data, Procs.invalidate_buffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateBufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def invalidate_buffer_data!
      self.invalidate_buffer_data ||
        raise FunctionUnavailableError.new(Translations.invalidate_buffer_data)
    end

    # Checks if the OpenGL function *glInvalidateBufferData* is available.
    def invalidate_buffer_data?
      !get_address(501, Translations.invalidate_buffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def invalidate_framebuffer
      get_proc(502, Translations.invalidate_framebuffer, Procs.invalidate_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def invalidate_framebuffer!
      self.invalidate_framebuffer ||
        raise FunctionUnavailableError.new(Translations.invalidate_framebuffer)
    end

    # Checks if the OpenGL function *glInvalidateFramebuffer* is available.
    def invalidate_framebuffer?
      !get_address(502, Translations.invalidate_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateSubFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def invalidate_sub_framebuffer
      get_proc(503, Translations.invalidate_sub_framebuffer, Procs.invalidate_sub_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateSubFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def invalidate_sub_framebuffer!
      self.invalidate_sub_framebuffer ||
        raise FunctionUnavailableError.new(Translations.invalidate_sub_framebuffer)
    end

    # Checks if the OpenGL function *glInvalidateSubFramebuffer* is available.
    def invalidate_sub_framebuffer?
      !get_address(503, Translations.invalidate_sub_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawArraysIndirect*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def multi_draw_arrays_indirect
      get_proc(504, Translations.multi_draw_arrays_indirect, Procs.multi_draw_arrays_indirect)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawArraysIndirect*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def multi_draw_arrays_indirect!
      self.multi_draw_arrays_indirect ||
        raise FunctionUnavailableError.new(Translations.multi_draw_arrays_indirect)
    end

    # Checks if the OpenGL function *glMultiDrawArraysIndirect* is available.
    def multi_draw_arrays_indirect?
      !get_address(504, Translations.multi_draw_arrays_indirect)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawElementsIndirect*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def multi_draw_elements_indirect
      get_proc(505, Translations.multi_draw_elements_indirect, Procs.multi_draw_elements_indirect)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawElementsIndirect*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def multi_draw_elements_indirect!
      self.multi_draw_elements_indirect ||
        raise FunctionUnavailableError.new(Translations.multi_draw_elements_indirect)
    end

    # Checks if the OpenGL function *glMultiDrawElementsIndirect* is available.
    def multi_draw_elements_indirect?
      !get_address(505, Translations.multi_draw_elements_indirect)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramInterfaceiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_interface_iv
      get_proc(506, Translations.get_program_interface_iv, Procs.get_program_interface_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramInterfaceiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_interface_iv!
      self.get_program_interface_iv ||
        raise FunctionUnavailableError.new(Translations.get_program_interface_iv)
    end

    # Checks if the OpenGL function *glGetProgramInterfaceiv* is available.
    def get_program_interface_iv?
      !get_address(506, Translations.get_program_interface_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramResourceIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_resource_index
      get_proc(507, Translations.get_program_resource_index, Procs.get_program_resource_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramResourceIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_resource_index!
      self.get_program_resource_index ||
        raise FunctionUnavailableError.new(Translations.get_program_resource_index)
    end

    # Checks if the OpenGL function *glGetProgramResourceIndex* is available.
    def get_program_resource_index?
      !get_address(507, Translations.get_program_resource_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramResourceName*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_resource_name
      get_proc(508, Translations.get_program_resource_name, Procs.get_program_resource_name)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramResourceName*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_resource_name!
      self.get_program_resource_name ||
        raise FunctionUnavailableError.new(Translations.get_program_resource_name)
    end

    # Checks if the OpenGL function *glGetProgramResourceName* is available.
    def get_program_resource_name?
      !get_address(508, Translations.get_program_resource_name)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramResourceiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_resource_iv
      get_proc(509, Translations.get_program_resource_iv, Procs.get_program_resource_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramResourceiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_resource_iv!
      self.get_program_resource_iv ||
        raise FunctionUnavailableError.new(Translations.get_program_resource_iv)
    end

    # Checks if the OpenGL function *glGetProgramResourceiv* is available.
    def get_program_resource_iv?
      !get_address(509, Translations.get_program_resource_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramResourceLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_resource_location
      get_proc(510, Translations.get_program_resource_location, Procs.get_program_resource_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramResourceLocation*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_resource_location!
      self.get_program_resource_location ||
        raise FunctionUnavailableError.new(Translations.get_program_resource_location)
    end

    # Checks if the OpenGL function *glGetProgramResourceLocation* is available.
    def get_program_resource_location?
      !get_address(510, Translations.get_program_resource_location)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramResourceLocationIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_program_resource_location_index
      get_proc(511, Translations.get_program_resource_location_index, Procs.get_program_resource_location_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetProgramResourceLocationIndex*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_program_resource_location_index!
      self.get_program_resource_location_index ||
        raise FunctionUnavailableError.new(Translations.get_program_resource_location_index)
    end

    # Checks if the OpenGL function *glGetProgramResourceLocationIndex* is available.
    def get_program_resource_location_index?
      !get_address(511, Translations.get_program_resource_location_index)
    end

    # Retrieves a `Proc` for the OpenGL function *glShaderStorageBlockBinding*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def shader_storage_block_binding
      get_proc(512, Translations.shader_storage_block_binding, Procs.shader_storage_block_binding)
    end

    # Retrieves a `Proc` for the OpenGL function *glShaderStorageBlockBinding*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def shader_storage_block_binding!
      self.shader_storage_block_binding ||
        raise FunctionUnavailableError.new(Translations.shader_storage_block_binding)
    end

    # Checks if the OpenGL function *glShaderStorageBlockBinding* is available.
    def shader_storage_block_binding?
      !get_address(512, Translations.shader_storage_block_binding)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_buffer_range
      get_proc(513, Translations.tex_buffer_range, Procs.tex_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_buffer_range!
      self.tex_buffer_range ||
        raise FunctionUnavailableError.new(Translations.tex_buffer_range)
    end

    # Checks if the OpenGL function *glTexBufferRange* is available.
    def tex_buffer_range?
      !get_address(513, Translations.tex_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexStorage2DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_storage_2d_multisample
      get_proc(514, Translations.tex_storage_2d_multisample, Procs.tex_storage_2d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexStorage2DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_storage_2d_multisample!
      self.tex_storage_2d_multisample ||
        raise FunctionUnavailableError.new(Translations.tex_storage_2d_multisample)
    end

    # Checks if the OpenGL function *glTexStorage2DMultisample* is available.
    def tex_storage_2d_multisample?
      !get_address(514, Translations.tex_storage_2d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexStorage3DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def tex_storage_3d_multisample
      get_proc(515, Translations.tex_storage_3d_multisample, Procs.tex_storage_3d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTexStorage3DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def tex_storage_3d_multisample!
      self.tex_storage_3d_multisample ||
        raise FunctionUnavailableError.new(Translations.tex_storage_3d_multisample)
    end

    # Checks if the OpenGL function *glTexStorage3DMultisample* is available.
    def tex_storage_3d_multisample?
      !get_address(515, Translations.tex_storage_3d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureView*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_view
      get_proc(516, Translations.texture_view, Procs.texture_view)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureView*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_view!
      self.texture_view ||
        raise FunctionUnavailableError.new(Translations.texture_view)
    end

    # Checks if the OpenGL function *glTextureView* is available.
    def texture_view?
      !get_address(516, Translations.texture_view)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindVertexBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_vertex_buffer
      get_proc(517, Translations.bind_vertex_buffer, Procs.bind_vertex_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindVertexBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_vertex_buffer!
      self.bind_vertex_buffer ||
        raise FunctionUnavailableError.new(Translations.bind_vertex_buffer)
    end

    # Checks if the OpenGL function *glBindVertexBuffer* is available.
    def bind_vertex_buffer?
      !get_address(517, Translations.bind_vertex_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_format
      get_proc(518, Translations.vertex_attrib_format, Procs.vertex_attrib_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_format!
      self.vertex_attrib_format ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_format)
    end

    # Checks if the OpenGL function *glVertexAttribFormat* is available.
    def vertex_attrib_format?
      !get_address(518, Translations.vertex_attrib_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribIFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_i_format
      get_proc(519, Translations.vertex_attrib_i_format, Procs.vertex_attrib_i_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribIFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_i_format!
      self.vertex_attrib_i_format ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_i_format)
    end

    # Checks if the OpenGL function *glVertexAttribIFormat* is available.
    def vertex_attrib_i_format?
      !get_address(519, Translations.vertex_attrib_i_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribLFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_l_format
      get_proc(520, Translations.vertex_attrib_l_format, Procs.vertex_attrib_l_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribLFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_l_format!
      self.vertex_attrib_l_format ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_l_format)
    end

    # Checks if the OpenGL function *glVertexAttribLFormat* is available.
    def vertex_attrib_l_format?
      !get_address(520, Translations.vertex_attrib_l_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribBinding*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_attrib_binding
      get_proc(521, Translations.vertex_attrib_binding, Procs.vertex_attrib_binding)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexAttribBinding*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_attrib_binding!
      self.vertex_attrib_binding ||
        raise FunctionUnavailableError.new(Translations.vertex_attrib_binding)
    end

    # Checks if the OpenGL function *glVertexAttribBinding* is available.
    def vertex_attrib_binding?
      !get_address(521, Translations.vertex_attrib_binding)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexBindingDivisor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_binding_divisor
      get_proc(522, Translations.vertex_binding_divisor, Procs.vertex_binding_divisor)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexBindingDivisor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_binding_divisor!
      self.vertex_binding_divisor ||
        raise FunctionUnavailableError.new(Translations.vertex_binding_divisor)
    end

    # Checks if the OpenGL function *glVertexBindingDivisor* is available.
    def vertex_binding_divisor?
      !get_address(522, Translations.vertex_binding_divisor)
    end

    # Retrieves a `Proc` for the OpenGL function *glDebugMessageControl*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def debug_message_control
      get_proc(523, Translations.debug_message_control, Procs.debug_message_control)
    end

    # Retrieves a `Proc` for the OpenGL function *glDebugMessageControl*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def debug_message_control!
      self.debug_message_control ||
        raise FunctionUnavailableError.new(Translations.debug_message_control)
    end

    # Checks if the OpenGL function *glDebugMessageControl* is available.
    def debug_message_control?
      !get_address(523, Translations.debug_message_control)
    end

    # Retrieves a `Proc` for the OpenGL function *glDebugMessageInsert*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def debug_message_insert
      get_proc(524, Translations.debug_message_insert, Procs.debug_message_insert)
    end

    # Retrieves a `Proc` for the OpenGL function *glDebugMessageInsert*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def debug_message_insert!
      self.debug_message_insert ||
        raise FunctionUnavailableError.new(Translations.debug_message_insert)
    end

    # Checks if the OpenGL function *glDebugMessageInsert* is available.
    def debug_message_insert?
      !get_address(524, Translations.debug_message_insert)
    end

    # Retrieves a `Proc` for the OpenGL function *glDebugMessageCallback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def debug_message_callback
      get_proc(525, Translations.debug_message_callback, Procs.debug_message_callback)
    end

    # Retrieves a `Proc` for the OpenGL function *glDebugMessageCallback*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def debug_message_callback!
      self.debug_message_callback ||
        raise FunctionUnavailableError.new(Translations.debug_message_callback)
    end

    # Checks if the OpenGL function *glDebugMessageCallback* is available.
    def debug_message_callback?
      !get_address(525, Translations.debug_message_callback)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetDebugMessageLog*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_debug_message_log
      get_proc(526, Translations.get_debug_message_log, Procs.get_debug_message_log)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetDebugMessageLog*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_debug_message_log!
      self.get_debug_message_log ||
        raise FunctionUnavailableError.new(Translations.get_debug_message_log)
    end

    # Checks if the OpenGL function *glGetDebugMessageLog* is available.
    def get_debug_message_log?
      !get_address(526, Translations.get_debug_message_log)
    end

    # Retrieves a `Proc` for the OpenGL function *glPushDebugGroup*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def push_debug_group
      get_proc(527, Translations.push_debug_group, Procs.push_debug_group)
    end

    # Retrieves a `Proc` for the OpenGL function *glPushDebugGroup*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def push_debug_group!
      self.push_debug_group ||
        raise FunctionUnavailableError.new(Translations.push_debug_group)
    end

    # Checks if the OpenGL function *glPushDebugGroup* is available.
    def push_debug_group?
      !get_address(527, Translations.push_debug_group)
    end

    # Retrieves a `Proc` for the OpenGL function *glPopDebugGroup*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def pop_debug_group
      get_proc(528, Translations.pop_debug_group, Procs.pop_debug_group)
    end

    # Retrieves a `Proc` for the OpenGL function *glPopDebugGroup*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def pop_debug_group!
      self.pop_debug_group ||
        raise FunctionUnavailableError.new(Translations.pop_debug_group)
    end

    # Checks if the OpenGL function *glPopDebugGroup* is available.
    def pop_debug_group?
      !get_address(528, Translations.pop_debug_group)
    end

    # Retrieves a `Proc` for the OpenGL function *glObjectLabel*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def object_label
      get_proc(529, Translations.object_label, Procs.object_label)
    end

    # Retrieves a `Proc` for the OpenGL function *glObjectLabel*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def object_label!
      self.object_label ||
        raise FunctionUnavailableError.new(Translations.object_label)
    end

    # Checks if the OpenGL function *glObjectLabel* is available.
    def object_label?
      !get_address(529, Translations.object_label)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetObjectLabel*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_object_label
      get_proc(530, Translations.get_object_label, Procs.get_object_label)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetObjectLabel*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_object_label!
      self.get_object_label ||
        raise FunctionUnavailableError.new(Translations.get_object_label)
    end

    # Checks if the OpenGL function *glGetObjectLabel* is available.
    def get_object_label?
      !get_address(530, Translations.get_object_label)
    end

    # Retrieves a `Proc` for the OpenGL function *glObjectPtrLabel*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def object_ptr_label
      get_proc(531, Translations.object_ptr_label, Procs.object_ptr_label)
    end

    # Retrieves a `Proc` for the OpenGL function *glObjectPtrLabel*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def object_ptr_label!
      self.object_ptr_label ||
        raise FunctionUnavailableError.new(Translations.object_ptr_label)
    end

    # Checks if the OpenGL function *glObjectPtrLabel* is available.
    def object_ptr_label?
      !get_address(531, Translations.object_ptr_label)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetObjectPtrLabel*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_object_ptr_label
      get_proc(532, Translations.get_object_ptr_label, Procs.get_object_ptr_label)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetObjectPtrLabel*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_object_ptr_label!
      self.get_object_ptr_label ||
        raise FunctionUnavailableError.new(Translations.get_object_ptr_label)
    end

    # Checks if the OpenGL function *glGetObjectPtrLabel* is available.
    def get_object_ptr_label?
      !get_address(532, Translations.get_object_ptr_label)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetPointerv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_pointer_v
      get_proc(533, Translations.get_pointer_v, Procs.get_pointer_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetPointerv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_pointer_v!
      self.get_pointer_v ||
        raise FunctionUnavailableError.new(Translations.get_pointer_v)
    end

    # Checks if the OpenGL function *glGetPointerv* is available.
    def get_pointer_v?
      !get_address(533, Translations.get_pointer_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glBufferStorage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def buffer_storage
      get_proc(534, Translations.buffer_storage, Procs.buffer_storage)
    end

    # Retrieves a `Proc` for the OpenGL function *glBufferStorage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def buffer_storage!
      self.buffer_storage ||
        raise FunctionUnavailableError.new(Translations.buffer_storage)
    end

    # Checks if the OpenGL function *glBufferStorage* is available.
    def buffer_storage?
      !get_address(534, Translations.buffer_storage)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_tex_image
      get_proc(535, Translations.clear_tex_image, Procs.clear_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_tex_image!
      self.clear_tex_image ||
        raise FunctionUnavailableError.new(Translations.clear_tex_image)
    end

    # Checks if the OpenGL function *glClearTexImage* is available.
    def clear_tex_image?
      !get_address(535, Translations.clear_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearTexSubImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_tex_sub_image
      get_proc(536, Translations.clear_tex_sub_image, Procs.clear_tex_sub_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearTexSubImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_tex_sub_image!
      self.clear_tex_sub_image ||
        raise FunctionUnavailableError.new(Translations.clear_tex_sub_image)
    end

    # Checks if the OpenGL function *glClearTexSubImage* is available.
    def clear_tex_sub_image?
      !get_address(536, Translations.clear_tex_sub_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindBuffersBase*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_buffers_base
      get_proc(537, Translations.bind_buffers_base, Procs.bind_buffers_base)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindBuffersBase*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_buffers_base!
      self.bind_buffers_base ||
        raise FunctionUnavailableError.new(Translations.bind_buffers_base)
    end

    # Checks if the OpenGL function *glBindBuffersBase* is available.
    def bind_buffers_base?
      !get_address(537, Translations.bind_buffers_base)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindBuffersRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_buffers_range
      get_proc(538, Translations.bind_buffers_range, Procs.bind_buffers_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindBuffersRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_buffers_range!
      self.bind_buffers_range ||
        raise FunctionUnavailableError.new(Translations.bind_buffers_range)
    end

    # Checks if the OpenGL function *glBindBuffersRange* is available.
    def bind_buffers_range?
      !get_address(538, Translations.bind_buffers_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindTextures*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_textures
      get_proc(539, Translations.bind_textures, Procs.bind_textures)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindTextures*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_textures!
      self.bind_textures ||
        raise FunctionUnavailableError.new(Translations.bind_textures)
    end

    # Checks if the OpenGL function *glBindTextures* is available.
    def bind_textures?
      !get_address(539, Translations.bind_textures)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindSamplers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_samplers
      get_proc(540, Translations.bind_samplers, Procs.bind_samplers)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindSamplers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_samplers!
      self.bind_samplers ||
        raise FunctionUnavailableError.new(Translations.bind_samplers)
    end

    # Checks if the OpenGL function *glBindSamplers* is available.
    def bind_samplers?
      !get_address(540, Translations.bind_samplers)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindImageTextures*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_image_textures
      get_proc(541, Translations.bind_image_textures, Procs.bind_image_textures)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindImageTextures*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_image_textures!
      self.bind_image_textures ||
        raise FunctionUnavailableError.new(Translations.bind_image_textures)
    end

    # Checks if the OpenGL function *glBindImageTextures* is available.
    def bind_image_textures?
      !get_address(541, Translations.bind_image_textures)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindVertexBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_vertex_buffers
      get_proc(542, Translations.bind_vertex_buffers, Procs.bind_vertex_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindVertexBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_vertex_buffers!
      self.bind_vertex_buffers ||
        raise FunctionUnavailableError.new(Translations.bind_vertex_buffers)
    end

    # Checks if the OpenGL function *glBindVertexBuffers* is available.
    def bind_vertex_buffers?
      !get_address(542, Translations.bind_vertex_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glClipControl*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clip_control
      get_proc(543, Translations.clip_control, Procs.clip_control)
    end

    # Retrieves a `Proc` for the OpenGL function *glClipControl*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clip_control!
      self.clip_control ||
        raise FunctionUnavailableError.new(Translations.clip_control)
    end

    # Checks if the OpenGL function *glClipControl* is available.
    def clip_control?
      !get_address(543, Translations.clip_control)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateTransformFeedbacks*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_transform_feedbacks
      get_proc(544, Translations.create_transform_feedbacks, Procs.create_transform_feedbacks)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateTransformFeedbacks*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_transform_feedbacks!
      self.create_transform_feedbacks ||
        raise FunctionUnavailableError.new(Translations.create_transform_feedbacks)
    end

    # Checks if the OpenGL function *glCreateTransformFeedbacks* is available.
    def create_transform_feedbacks?
      !get_address(544, Translations.create_transform_feedbacks)
    end

    # Retrieves a `Proc` for the OpenGL function *glTransformFeedbackBufferBase*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def transform_feedback_buffer_base
      get_proc(545, Translations.transform_feedback_buffer_base, Procs.transform_feedback_buffer_base)
    end

    # Retrieves a `Proc` for the OpenGL function *glTransformFeedbackBufferBase*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def transform_feedback_buffer_base!
      self.transform_feedback_buffer_base ||
        raise FunctionUnavailableError.new(Translations.transform_feedback_buffer_base)
    end

    # Checks if the OpenGL function *glTransformFeedbackBufferBase* is available.
    def transform_feedback_buffer_base?
      !get_address(545, Translations.transform_feedback_buffer_base)
    end

    # Retrieves a `Proc` for the OpenGL function *glTransformFeedbackBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def transform_feedback_buffer_range
      get_proc(546, Translations.transform_feedback_buffer_range, Procs.transform_feedback_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glTransformFeedbackBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def transform_feedback_buffer_range!
      self.transform_feedback_buffer_range ||
        raise FunctionUnavailableError.new(Translations.transform_feedback_buffer_range)
    end

    # Checks if the OpenGL function *glTransformFeedbackBufferRange* is available.
    def transform_feedback_buffer_range?
      !get_address(546, Translations.transform_feedback_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTransformFeedbackiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_transform_feedback_iv
      get_proc(547, Translations.get_transform_feedback_iv, Procs.get_transform_feedback_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTransformFeedbackiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_transform_feedback_iv!
      self.get_transform_feedback_iv ||
        raise FunctionUnavailableError.new(Translations.get_transform_feedback_iv)
    end

    # Checks if the OpenGL function *glGetTransformFeedbackiv* is available.
    def get_transform_feedback_iv?
      !get_address(547, Translations.get_transform_feedback_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTransformFeedbacki_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_transform_feedback_i_v
      get_proc(548, Translations.get_transform_feedback_i_v, Procs.get_transform_feedback_i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTransformFeedbacki_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_transform_feedback_i_v!
      self.get_transform_feedback_i_v ||
        raise FunctionUnavailableError.new(Translations.get_transform_feedback_i_v)
    end

    # Checks if the OpenGL function *glGetTransformFeedbacki_v* is available.
    def get_transform_feedback_i_v?
      !get_address(548, Translations.get_transform_feedback_i_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTransformFeedbacki64_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_transform_feedback_i64_v
      get_proc(549, Translations.get_transform_feedback_i64_v, Procs.get_transform_feedback_i64_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTransformFeedbacki64_v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_transform_feedback_i64_v!
      self.get_transform_feedback_i64_v ||
        raise FunctionUnavailableError.new(Translations.get_transform_feedback_i64_v)
    end

    # Checks if the OpenGL function *glGetTransformFeedbacki64_v* is available.
    def get_transform_feedback_i64_v?
      !get_address(549, Translations.get_transform_feedback_i64_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_buffers
      get_proc(550, Translations.create_buffers, Procs.create_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_buffers!
      self.create_buffers ||
        raise FunctionUnavailableError.new(Translations.create_buffers)
    end

    # Checks if the OpenGL function *glCreateBuffers* is available.
    def create_buffers?
      !get_address(550, Translations.create_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedBufferStorage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_buffer_storage
      get_proc(551, Translations.named_buffer_storage, Procs.named_buffer_storage)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedBufferStorage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_buffer_storage!
      self.named_buffer_storage ||
        raise FunctionUnavailableError.new(Translations.named_buffer_storage)
    end

    # Checks if the OpenGL function *glNamedBufferStorage* is available.
    def named_buffer_storage?
      !get_address(551, Translations.named_buffer_storage)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedBufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_buffer_data
      get_proc(552, Translations.named_buffer_data, Procs.named_buffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedBufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_buffer_data!
      self.named_buffer_data ||
        raise FunctionUnavailableError.new(Translations.named_buffer_data)
    end

    # Checks if the OpenGL function *glNamedBufferData* is available.
    def named_buffer_data?
      !get_address(552, Translations.named_buffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_buffer_sub_data
      get_proc(553, Translations.named_buffer_sub_data, Procs.named_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_buffer_sub_data!
      self.named_buffer_sub_data ||
        raise FunctionUnavailableError.new(Translations.named_buffer_sub_data)
    end

    # Checks if the OpenGL function *glNamedBufferSubData* is available.
    def named_buffer_sub_data?
      !get_address(553, Translations.named_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyNamedBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_named_buffer_sub_data
      get_proc(554, Translations.copy_named_buffer_sub_data, Procs.copy_named_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyNamedBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_named_buffer_sub_data!
      self.copy_named_buffer_sub_data ||
        raise FunctionUnavailableError.new(Translations.copy_named_buffer_sub_data)
    end

    # Checks if the OpenGL function *glCopyNamedBufferSubData* is available.
    def copy_named_buffer_sub_data?
      !get_address(554, Translations.copy_named_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedBufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_named_buffer_data
      get_proc(555, Translations.clear_named_buffer_data, Procs.clear_named_buffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedBufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_named_buffer_data!
      self.clear_named_buffer_data ||
        raise FunctionUnavailableError.new(Translations.clear_named_buffer_data)
    end

    # Checks if the OpenGL function *glClearNamedBufferData* is available.
    def clear_named_buffer_data?
      !get_address(555, Translations.clear_named_buffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_named_buffer_sub_data
      get_proc(556, Translations.clear_named_buffer_sub_data, Procs.clear_named_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_named_buffer_sub_data!
      self.clear_named_buffer_sub_data ||
        raise FunctionUnavailableError.new(Translations.clear_named_buffer_sub_data)
    end

    # Checks if the OpenGL function *glClearNamedBufferSubData* is available.
    def clear_named_buffer_sub_data?
      !get_address(556, Translations.clear_named_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glMapNamedBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def map_named_buffer
      get_proc(557, Translations.map_named_buffer, Procs.map_named_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glMapNamedBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def map_named_buffer!
      self.map_named_buffer ||
        raise FunctionUnavailableError.new(Translations.map_named_buffer)
    end

    # Checks if the OpenGL function *glMapNamedBuffer* is available.
    def map_named_buffer?
      !get_address(557, Translations.map_named_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glMapNamedBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def map_named_buffer_range
      get_proc(558, Translations.map_named_buffer_range, Procs.map_named_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glMapNamedBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def map_named_buffer_range!
      self.map_named_buffer_range ||
        raise FunctionUnavailableError.new(Translations.map_named_buffer_range)
    end

    # Checks if the OpenGL function *glMapNamedBufferRange* is available.
    def map_named_buffer_range?
      !get_address(558, Translations.map_named_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glUnmapNamedBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def unmap_named_buffer
      get_proc(559, Translations.unmap_named_buffer, Procs.unmap_named_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glUnmapNamedBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def unmap_named_buffer!
      self.unmap_named_buffer ||
        raise FunctionUnavailableError.new(Translations.unmap_named_buffer)
    end

    # Checks if the OpenGL function *glUnmapNamedBuffer* is available.
    def unmap_named_buffer?
      !get_address(559, Translations.unmap_named_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glFlushMappedNamedBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def flush_mapped_named_buffer_range
      get_proc(560, Translations.flush_mapped_named_buffer_range, Procs.flush_mapped_named_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glFlushMappedNamedBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def flush_mapped_named_buffer_range!
      self.flush_mapped_named_buffer_range ||
        raise FunctionUnavailableError.new(Translations.flush_mapped_named_buffer_range)
    end

    # Checks if the OpenGL function *glFlushMappedNamedBufferRange* is available.
    def flush_mapped_named_buffer_range?
      !get_address(560, Translations.flush_mapped_named_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedBufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_named_buffer_parameter_iv
      get_proc(561, Translations.get_named_buffer_parameter_iv, Procs.get_named_buffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedBufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_named_buffer_parameter_iv!
      self.get_named_buffer_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_named_buffer_parameter_iv)
    end

    # Checks if the OpenGL function *glGetNamedBufferParameteriv* is available.
    def get_named_buffer_parameter_iv?
      !get_address(561, Translations.get_named_buffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedBufferParameteri64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_named_buffer_parameter_i64v
      get_proc(562, Translations.get_named_buffer_parameter_i64v, Procs.get_named_buffer_parameter_i64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedBufferParameteri64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_named_buffer_parameter_i64v!
      self.get_named_buffer_parameter_i64v ||
        raise FunctionUnavailableError.new(Translations.get_named_buffer_parameter_i64v)
    end

    # Checks if the OpenGL function *glGetNamedBufferParameteri64v* is available.
    def get_named_buffer_parameter_i64v?
      !get_address(562, Translations.get_named_buffer_parameter_i64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedBufferPointerv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_named_buffer_pointer_v
      get_proc(563, Translations.get_named_buffer_pointer_v, Procs.get_named_buffer_pointer_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedBufferPointerv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_named_buffer_pointer_v!
      self.get_named_buffer_pointer_v ||
        raise FunctionUnavailableError.new(Translations.get_named_buffer_pointer_v)
    end

    # Checks if the OpenGL function *glGetNamedBufferPointerv* is available.
    def get_named_buffer_pointer_v?
      !get_address(563, Translations.get_named_buffer_pointer_v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_named_buffer_sub_data
      get_proc(564, Translations.get_named_buffer_sub_data, Procs.get_named_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedBufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_named_buffer_sub_data!
      self.get_named_buffer_sub_data ||
        raise FunctionUnavailableError.new(Translations.get_named_buffer_sub_data)
    end

    # Checks if the OpenGL function *glGetNamedBufferSubData* is available.
    def get_named_buffer_sub_data?
      !get_address(564, Translations.get_named_buffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateFramebuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_framebuffers
      get_proc(565, Translations.create_framebuffers, Procs.create_framebuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateFramebuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_framebuffers!
      self.create_framebuffers ||
        raise FunctionUnavailableError.new(Translations.create_framebuffers)
    end

    # Checks if the OpenGL function *glCreateFramebuffers* is available.
    def create_framebuffers?
      !get_address(565, Translations.create_framebuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferRenderbuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_framebuffer_renderbuffer
      get_proc(566, Translations.named_framebuffer_renderbuffer, Procs.named_framebuffer_renderbuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferRenderbuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_framebuffer_renderbuffer!
      self.named_framebuffer_renderbuffer ||
        raise FunctionUnavailableError.new(Translations.named_framebuffer_renderbuffer)
    end

    # Checks if the OpenGL function *glNamedFramebufferRenderbuffer* is available.
    def named_framebuffer_renderbuffer?
      !get_address(566, Translations.named_framebuffer_renderbuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_framebuffer_parameter_i
      get_proc(567, Translations.named_framebuffer_parameter_i, Procs.named_framebuffer_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_framebuffer_parameter_i!
      self.named_framebuffer_parameter_i ||
        raise FunctionUnavailableError.new(Translations.named_framebuffer_parameter_i)
    end

    # Checks if the OpenGL function *glNamedFramebufferParameteri* is available.
    def named_framebuffer_parameter_i?
      !get_address(567, Translations.named_framebuffer_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_framebuffer_texture
      get_proc(568, Translations.named_framebuffer_texture, Procs.named_framebuffer_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferTexture*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_framebuffer_texture!
      self.named_framebuffer_texture ||
        raise FunctionUnavailableError.new(Translations.named_framebuffer_texture)
    end

    # Checks if the OpenGL function *glNamedFramebufferTexture* is available.
    def named_framebuffer_texture?
      !get_address(568, Translations.named_framebuffer_texture)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferTextureLayer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_framebuffer_texture_layer
      get_proc(569, Translations.named_framebuffer_texture_layer, Procs.named_framebuffer_texture_layer)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferTextureLayer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_framebuffer_texture_layer!
      self.named_framebuffer_texture_layer ||
        raise FunctionUnavailableError.new(Translations.named_framebuffer_texture_layer)
    end

    # Checks if the OpenGL function *glNamedFramebufferTextureLayer* is available.
    def named_framebuffer_texture_layer?
      !get_address(569, Translations.named_framebuffer_texture_layer)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferDrawBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_framebuffer_draw_buffer
      get_proc(570, Translations.named_framebuffer_draw_buffer, Procs.named_framebuffer_draw_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferDrawBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_framebuffer_draw_buffer!
      self.named_framebuffer_draw_buffer ||
        raise FunctionUnavailableError.new(Translations.named_framebuffer_draw_buffer)
    end

    # Checks if the OpenGL function *glNamedFramebufferDrawBuffer* is available.
    def named_framebuffer_draw_buffer?
      !get_address(570, Translations.named_framebuffer_draw_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferDrawBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_framebuffer_draw_buffers
      get_proc(571, Translations.named_framebuffer_draw_buffers, Procs.named_framebuffer_draw_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferDrawBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_framebuffer_draw_buffers!
      self.named_framebuffer_draw_buffers ||
        raise FunctionUnavailableError.new(Translations.named_framebuffer_draw_buffers)
    end

    # Checks if the OpenGL function *glNamedFramebufferDrawBuffers* is available.
    def named_framebuffer_draw_buffers?
      !get_address(571, Translations.named_framebuffer_draw_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferReadBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_framebuffer_read_buffer
      get_proc(572, Translations.named_framebuffer_read_buffer, Procs.named_framebuffer_read_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedFramebufferReadBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_framebuffer_read_buffer!
      self.named_framebuffer_read_buffer ||
        raise FunctionUnavailableError.new(Translations.named_framebuffer_read_buffer)
    end

    # Checks if the OpenGL function *glNamedFramebufferReadBuffer* is available.
    def named_framebuffer_read_buffer?
      !get_address(572, Translations.named_framebuffer_read_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateNamedFramebufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def invalidate_named_framebuffer_data
      get_proc(573, Translations.invalidate_named_framebuffer_data, Procs.invalidate_named_framebuffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateNamedFramebufferData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def invalidate_named_framebuffer_data!
      self.invalidate_named_framebuffer_data ||
        raise FunctionUnavailableError.new(Translations.invalidate_named_framebuffer_data)
    end

    # Checks if the OpenGL function *glInvalidateNamedFramebufferData* is available.
    def invalidate_named_framebuffer_data?
      !get_address(573, Translations.invalidate_named_framebuffer_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateNamedFramebufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def invalidate_named_framebuffer_sub_data
      get_proc(574, Translations.invalidate_named_framebuffer_sub_data, Procs.invalidate_named_framebuffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glInvalidateNamedFramebufferSubData*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def invalidate_named_framebuffer_sub_data!
      self.invalidate_named_framebuffer_sub_data ||
        raise FunctionUnavailableError.new(Translations.invalidate_named_framebuffer_sub_data)
    end

    # Checks if the OpenGL function *glInvalidateNamedFramebufferSubData* is available.
    def invalidate_named_framebuffer_sub_data?
      !get_address(574, Translations.invalidate_named_framebuffer_sub_data)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedFramebufferiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_named_framebuffer_iv
      get_proc(575, Translations.clear_named_framebuffer_iv, Procs.clear_named_framebuffer_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedFramebufferiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_named_framebuffer_iv!
      self.clear_named_framebuffer_iv ||
        raise FunctionUnavailableError.new(Translations.clear_named_framebuffer_iv)
    end

    # Checks if the OpenGL function *glClearNamedFramebufferiv* is available.
    def clear_named_framebuffer_iv?
      !get_address(575, Translations.clear_named_framebuffer_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedFramebufferuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_named_framebuffer_uiv
      get_proc(576, Translations.clear_named_framebuffer_uiv, Procs.clear_named_framebuffer_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedFramebufferuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_named_framebuffer_uiv!
      self.clear_named_framebuffer_uiv ||
        raise FunctionUnavailableError.new(Translations.clear_named_framebuffer_uiv)
    end

    # Checks if the OpenGL function *glClearNamedFramebufferuiv* is available.
    def clear_named_framebuffer_uiv?
      !get_address(576, Translations.clear_named_framebuffer_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedFramebufferfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_named_framebuffer_fv
      get_proc(577, Translations.clear_named_framebuffer_fv, Procs.clear_named_framebuffer_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedFramebufferfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_named_framebuffer_fv!
      self.clear_named_framebuffer_fv ||
        raise FunctionUnavailableError.new(Translations.clear_named_framebuffer_fv)
    end

    # Checks if the OpenGL function *glClearNamedFramebufferfv* is available.
    def clear_named_framebuffer_fv?
      !get_address(577, Translations.clear_named_framebuffer_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedFramebufferfi*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def clear_named_framebuffer_fi
      get_proc(578, Translations.clear_named_framebuffer_fi, Procs.clear_named_framebuffer_fi)
    end

    # Retrieves a `Proc` for the OpenGL function *glClearNamedFramebufferfi*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def clear_named_framebuffer_fi!
      self.clear_named_framebuffer_fi ||
        raise FunctionUnavailableError.new(Translations.clear_named_framebuffer_fi)
    end

    # Checks if the OpenGL function *glClearNamedFramebufferfi* is available.
    def clear_named_framebuffer_fi?
      !get_address(578, Translations.clear_named_framebuffer_fi)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlitNamedFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def blit_named_framebuffer
      get_proc(579, Translations.blit_named_framebuffer, Procs.blit_named_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glBlitNamedFramebuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def blit_named_framebuffer!
      self.blit_named_framebuffer ||
        raise FunctionUnavailableError.new(Translations.blit_named_framebuffer)
    end

    # Checks if the OpenGL function *glBlitNamedFramebuffer* is available.
    def blit_named_framebuffer?
      !get_address(579, Translations.blit_named_framebuffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glCheckNamedFramebufferStatus*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def check_named_framebuffer_status
      get_proc(580, Translations.check_named_framebuffer_status, Procs.check_named_framebuffer_status)
    end

    # Retrieves a `Proc` for the OpenGL function *glCheckNamedFramebufferStatus*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def check_named_framebuffer_status!
      self.check_named_framebuffer_status ||
        raise FunctionUnavailableError.new(Translations.check_named_framebuffer_status)
    end

    # Checks if the OpenGL function *glCheckNamedFramebufferStatus* is available.
    def check_named_framebuffer_status?
      !get_address(580, Translations.check_named_framebuffer_status)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedFramebufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_named_framebuffer_parameter_iv
      get_proc(581, Translations.get_named_framebuffer_parameter_iv, Procs.get_named_framebuffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedFramebufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_named_framebuffer_parameter_iv!
      self.get_named_framebuffer_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_named_framebuffer_parameter_iv)
    end

    # Checks if the OpenGL function *glGetNamedFramebufferParameteriv* is available.
    def get_named_framebuffer_parameter_iv?
      !get_address(581, Translations.get_named_framebuffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedFramebufferAttachmentParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_named_framebuffer_attachment_parameter_iv
      get_proc(582, Translations.get_named_framebuffer_attachment_parameter_iv, Procs.get_named_framebuffer_attachment_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedFramebufferAttachmentParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_named_framebuffer_attachment_parameter_iv!
      self.get_named_framebuffer_attachment_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_named_framebuffer_attachment_parameter_iv)
    end

    # Checks if the OpenGL function *glGetNamedFramebufferAttachmentParameteriv* is available.
    def get_named_framebuffer_attachment_parameter_iv?
      !get_address(582, Translations.get_named_framebuffer_attachment_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateRenderbuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_renderbuffers
      get_proc(583, Translations.create_renderbuffers, Procs.create_renderbuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateRenderbuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_renderbuffers!
      self.create_renderbuffers ||
        raise FunctionUnavailableError.new(Translations.create_renderbuffers)
    end

    # Checks if the OpenGL function *glCreateRenderbuffers* is available.
    def create_renderbuffers?
      !get_address(583, Translations.create_renderbuffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedRenderbufferStorage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_renderbuffer_storage
      get_proc(584, Translations.named_renderbuffer_storage, Procs.named_renderbuffer_storage)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedRenderbufferStorage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_renderbuffer_storage!
      self.named_renderbuffer_storage ||
        raise FunctionUnavailableError.new(Translations.named_renderbuffer_storage)
    end

    # Checks if the OpenGL function *glNamedRenderbufferStorage* is available.
    def named_renderbuffer_storage?
      !get_address(584, Translations.named_renderbuffer_storage)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedRenderbufferStorageMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def named_renderbuffer_storage_multisample
      get_proc(585, Translations.named_renderbuffer_storage_multisample, Procs.named_renderbuffer_storage_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glNamedRenderbufferStorageMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def named_renderbuffer_storage_multisample!
      self.named_renderbuffer_storage_multisample ||
        raise FunctionUnavailableError.new(Translations.named_renderbuffer_storage_multisample)
    end

    # Checks if the OpenGL function *glNamedRenderbufferStorageMultisample* is available.
    def named_renderbuffer_storage_multisample?
      !get_address(585, Translations.named_renderbuffer_storage_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedRenderbufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_named_renderbuffer_parameter_iv
      get_proc(586, Translations.get_named_renderbuffer_parameter_iv, Procs.get_named_renderbuffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetNamedRenderbufferParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_named_renderbuffer_parameter_iv!
      self.get_named_renderbuffer_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_named_renderbuffer_parameter_iv)
    end

    # Checks if the OpenGL function *glGetNamedRenderbufferParameteriv* is available.
    def get_named_renderbuffer_parameter_iv?
      !get_address(586, Translations.get_named_renderbuffer_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateTextures*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_textures
      get_proc(587, Translations.create_textures, Procs.create_textures)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateTextures*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_textures!
      self.create_textures ||
        raise FunctionUnavailableError.new(Translations.create_textures)
    end

    # Checks if the OpenGL function *glCreateTextures* is available.
    def create_textures?
      !get_address(587, Translations.create_textures)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_buffer
      get_proc(588, Translations.texture_buffer, Procs.texture_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_buffer!
      self.texture_buffer ||
        raise FunctionUnavailableError.new(Translations.texture_buffer)
    end

    # Checks if the OpenGL function *glTextureBuffer* is available.
    def texture_buffer?
      !get_address(588, Translations.texture_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_buffer_range
      get_proc(589, Translations.texture_buffer_range, Procs.texture_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureBufferRange*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_buffer_range!
      self.texture_buffer_range ||
        raise FunctionUnavailableError.new(Translations.texture_buffer_range)
    end

    # Checks if the OpenGL function *glTextureBufferRange* is available.
    def texture_buffer_range?
      !get_address(589, Translations.texture_buffer_range)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureStorage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_storage_1d
      get_proc(590, Translations.texture_storage_1d, Procs.texture_storage_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureStorage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_storage_1d!
      self.texture_storage_1d ||
        raise FunctionUnavailableError.new(Translations.texture_storage_1d)
    end

    # Checks if the OpenGL function *glTextureStorage1D* is available.
    def texture_storage_1d?
      !get_address(590, Translations.texture_storage_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureStorage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_storage_2d
      get_proc(591, Translations.texture_storage_2d, Procs.texture_storage_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureStorage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_storage_2d!
      self.texture_storage_2d ||
        raise FunctionUnavailableError.new(Translations.texture_storage_2d)
    end

    # Checks if the OpenGL function *glTextureStorage2D* is available.
    def texture_storage_2d?
      !get_address(591, Translations.texture_storage_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureStorage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_storage_3d
      get_proc(592, Translations.texture_storage_3d, Procs.texture_storage_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureStorage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_storage_3d!
      self.texture_storage_3d ||
        raise FunctionUnavailableError.new(Translations.texture_storage_3d)
    end

    # Checks if the OpenGL function *glTextureStorage3D* is available.
    def texture_storage_3d?
      !get_address(592, Translations.texture_storage_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureStorage2DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_storage_2d_multisample
      get_proc(593, Translations.texture_storage_2d_multisample, Procs.texture_storage_2d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureStorage2DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_storage_2d_multisample!
      self.texture_storage_2d_multisample ||
        raise FunctionUnavailableError.new(Translations.texture_storage_2d_multisample)
    end

    # Checks if the OpenGL function *glTextureStorage2DMultisample* is available.
    def texture_storage_2d_multisample?
      !get_address(593, Translations.texture_storage_2d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureStorage3DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_storage_3d_multisample
      get_proc(594, Translations.texture_storage_3d_multisample, Procs.texture_storage_3d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureStorage3DMultisample*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_storage_3d_multisample!
      self.texture_storage_3d_multisample ||
        raise FunctionUnavailableError.new(Translations.texture_storage_3d_multisample)
    end

    # Checks if the OpenGL function *glTextureStorage3DMultisample* is available.
    def texture_storage_3d_multisample?
      !get_address(594, Translations.texture_storage_3d_multisample)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_sub_image_1d
      get_proc(595, Translations.texture_sub_image_1d, Procs.texture_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_sub_image_1d!
      self.texture_sub_image_1d ||
        raise FunctionUnavailableError.new(Translations.texture_sub_image_1d)
    end

    # Checks if the OpenGL function *glTextureSubImage1D* is available.
    def texture_sub_image_1d?
      !get_address(595, Translations.texture_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_sub_image_2d
      get_proc(596, Translations.texture_sub_image_2d, Procs.texture_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_sub_image_2d!
      self.texture_sub_image_2d ||
        raise FunctionUnavailableError.new(Translations.texture_sub_image_2d)
    end

    # Checks if the OpenGL function *glTextureSubImage2D* is available.
    def texture_sub_image_2d?
      !get_address(596, Translations.texture_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_sub_image_3d
      get_proc(597, Translations.texture_sub_image_3d, Procs.texture_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_sub_image_3d!
      self.texture_sub_image_3d ||
        raise FunctionUnavailableError.new(Translations.texture_sub_image_3d)
    end

    # Checks if the OpenGL function *glTextureSubImage3D* is available.
    def texture_sub_image_3d?
      !get_address(597, Translations.texture_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTextureSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def compressed_texture_sub_image_1d
      get_proc(598, Translations.compressed_texture_sub_image_1d, Procs.compressed_texture_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTextureSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def compressed_texture_sub_image_1d!
      self.compressed_texture_sub_image_1d ||
        raise FunctionUnavailableError.new(Translations.compressed_texture_sub_image_1d)
    end

    # Checks if the OpenGL function *glCompressedTextureSubImage1D* is available.
    def compressed_texture_sub_image_1d?
      !get_address(598, Translations.compressed_texture_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTextureSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def compressed_texture_sub_image_2d
      get_proc(599, Translations.compressed_texture_sub_image_2d, Procs.compressed_texture_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTextureSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def compressed_texture_sub_image_2d!
      self.compressed_texture_sub_image_2d ||
        raise FunctionUnavailableError.new(Translations.compressed_texture_sub_image_2d)
    end

    # Checks if the OpenGL function *glCompressedTextureSubImage2D* is available.
    def compressed_texture_sub_image_2d?
      !get_address(599, Translations.compressed_texture_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTextureSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def compressed_texture_sub_image_3d
      get_proc(600, Translations.compressed_texture_sub_image_3d, Procs.compressed_texture_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCompressedTextureSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def compressed_texture_sub_image_3d!
      self.compressed_texture_sub_image_3d ||
        raise FunctionUnavailableError.new(Translations.compressed_texture_sub_image_3d)
    end

    # Checks if the OpenGL function *glCompressedTextureSubImage3D* is available.
    def compressed_texture_sub_image_3d?
      !get_address(600, Translations.compressed_texture_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTextureSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_texture_sub_image_1d
      get_proc(601, Translations.copy_texture_sub_image_1d, Procs.copy_texture_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTextureSubImage1D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_texture_sub_image_1d!
      self.copy_texture_sub_image_1d ||
        raise FunctionUnavailableError.new(Translations.copy_texture_sub_image_1d)
    end

    # Checks if the OpenGL function *glCopyTextureSubImage1D* is available.
    def copy_texture_sub_image_1d?
      !get_address(601, Translations.copy_texture_sub_image_1d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTextureSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_texture_sub_image_2d
      get_proc(602, Translations.copy_texture_sub_image_2d, Procs.copy_texture_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTextureSubImage2D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_texture_sub_image_2d!
      self.copy_texture_sub_image_2d ||
        raise FunctionUnavailableError.new(Translations.copy_texture_sub_image_2d)
    end

    # Checks if the OpenGL function *glCopyTextureSubImage2D* is available.
    def copy_texture_sub_image_2d?
      !get_address(602, Translations.copy_texture_sub_image_2d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTextureSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def copy_texture_sub_image_3d
      get_proc(603, Translations.copy_texture_sub_image_3d, Procs.copy_texture_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glCopyTextureSubImage3D*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def copy_texture_sub_image_3d!
      self.copy_texture_sub_image_3d ||
        raise FunctionUnavailableError.new(Translations.copy_texture_sub_image_3d)
    end

    # Checks if the OpenGL function *glCopyTextureSubImage3D* is available.
    def copy_texture_sub_image_3d?
      !get_address(603, Translations.copy_texture_sub_image_3d)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameterf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_parameter_f
      get_proc(604, Translations.texture_parameter_f, Procs.texture_parameter_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameterf*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_parameter_f!
      self.texture_parameter_f ||
        raise FunctionUnavailableError.new(Translations.texture_parameter_f)
    end

    # Checks if the OpenGL function *glTextureParameterf* is available.
    def texture_parameter_f?
      !get_address(604, Translations.texture_parameter_f)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_parameter_fv
      get_proc(605, Translations.texture_parameter_fv, Procs.texture_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_parameter_fv!
      self.texture_parameter_fv ||
        raise FunctionUnavailableError.new(Translations.texture_parameter_fv)
    end

    # Checks if the OpenGL function *glTextureParameterfv* is available.
    def texture_parameter_fv?
      !get_address(605, Translations.texture_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_parameter_i
      get_proc(606, Translations.texture_parameter_i, Procs.texture_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameteri*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_parameter_i!
      self.texture_parameter_i ||
        raise FunctionUnavailableError.new(Translations.texture_parameter_i)
    end

    # Checks if the OpenGL function *glTextureParameteri* is available.
    def texture_parameter_i?
      !get_address(606, Translations.texture_parameter_i)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_parameter_i_iv
      get_proc(607, Translations.texture_parameter_i_iv, Procs.texture_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_parameter_i_iv!
      self.texture_parameter_i_iv ||
        raise FunctionUnavailableError.new(Translations.texture_parameter_i_iv)
    end

    # Checks if the OpenGL function *glTextureParameterIiv* is available.
    def texture_parameter_i_iv?
      !get_address(607, Translations.texture_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_parameter_i_uiv
      get_proc(608, Translations.texture_parameter_i_uiv, Procs.texture_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_parameter_i_uiv!
      self.texture_parameter_i_uiv ||
        raise FunctionUnavailableError.new(Translations.texture_parameter_i_uiv)
    end

    # Checks if the OpenGL function *glTextureParameterIuiv* is available.
    def texture_parameter_i_uiv?
      !get_address(608, Translations.texture_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_parameter_iv
      get_proc(609, Translations.texture_parameter_iv, Procs.texture_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_parameter_iv!
      self.texture_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.texture_parameter_iv)
    end

    # Checks if the OpenGL function *glTextureParameteriv* is available.
    def texture_parameter_iv?
      !get_address(609, Translations.texture_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenerateTextureMipmap*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def generate_texture_mipmap
      get_proc(610, Translations.generate_texture_mipmap, Procs.generate_texture_mipmap)
    end

    # Retrieves a `Proc` for the OpenGL function *glGenerateTextureMipmap*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def generate_texture_mipmap!
      self.generate_texture_mipmap ||
        raise FunctionUnavailableError.new(Translations.generate_texture_mipmap)
    end

    # Checks if the OpenGL function *glGenerateTextureMipmap* is available.
    def generate_texture_mipmap?
      !get_address(610, Translations.generate_texture_mipmap)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindTextureUnit*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def bind_texture_unit
      get_proc(611, Translations.bind_texture_unit, Procs.bind_texture_unit)
    end

    # Retrieves a `Proc` for the OpenGL function *glBindTextureUnit*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def bind_texture_unit!
      self.bind_texture_unit ||
        raise FunctionUnavailableError.new(Translations.bind_texture_unit)
    end

    # Checks if the OpenGL function *glBindTextureUnit* is available.
    def bind_texture_unit?
      !get_address(611, Translations.bind_texture_unit)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_texture_image
      get_proc(612, Translations.get_texture_image, Procs.get_texture_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_texture_image!
      self.get_texture_image ||
        raise FunctionUnavailableError.new(Translations.get_texture_image)
    end

    # Checks if the OpenGL function *glGetTextureImage* is available.
    def get_texture_image?
      !get_address(612, Translations.get_texture_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetCompressedTextureImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_compressed_texture_image
      get_proc(613, Translations.get_compressed_texture_image, Procs.get_compressed_texture_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetCompressedTextureImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_compressed_texture_image!
      self.get_compressed_texture_image ||
        raise FunctionUnavailableError.new(Translations.get_compressed_texture_image)
    end

    # Checks if the OpenGL function *glGetCompressedTextureImage* is available.
    def get_compressed_texture_image?
      !get_address(613, Translations.get_compressed_texture_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureLevelParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_texture_level_parameter_fv
      get_proc(614, Translations.get_texture_level_parameter_fv, Procs.get_texture_level_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureLevelParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_texture_level_parameter_fv!
      self.get_texture_level_parameter_fv ||
        raise FunctionUnavailableError.new(Translations.get_texture_level_parameter_fv)
    end

    # Checks if the OpenGL function *glGetTextureLevelParameterfv* is available.
    def get_texture_level_parameter_fv?
      !get_address(614, Translations.get_texture_level_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureLevelParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_texture_level_parameter_iv
      get_proc(615, Translations.get_texture_level_parameter_iv, Procs.get_texture_level_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureLevelParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_texture_level_parameter_iv!
      self.get_texture_level_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_texture_level_parameter_iv)
    end

    # Checks if the OpenGL function *glGetTextureLevelParameteriv* is available.
    def get_texture_level_parameter_iv?
      !get_address(615, Translations.get_texture_level_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_texture_parameter_fv
      get_proc(616, Translations.get_texture_parameter_fv, Procs.get_texture_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureParameterfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_texture_parameter_fv!
      self.get_texture_parameter_fv ||
        raise FunctionUnavailableError.new(Translations.get_texture_parameter_fv)
    end

    # Checks if the OpenGL function *glGetTextureParameterfv* is available.
    def get_texture_parameter_fv?
      !get_address(616, Translations.get_texture_parameter_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_texture_parameter_i_iv
      get_proc(617, Translations.get_texture_parameter_i_iv, Procs.get_texture_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureParameterIiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_texture_parameter_i_iv!
      self.get_texture_parameter_i_iv ||
        raise FunctionUnavailableError.new(Translations.get_texture_parameter_i_iv)
    end

    # Checks if the OpenGL function *glGetTextureParameterIiv* is available.
    def get_texture_parameter_i_iv?
      !get_address(617, Translations.get_texture_parameter_i_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_texture_parameter_i_uiv
      get_proc(618, Translations.get_texture_parameter_i_uiv, Procs.get_texture_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureParameterIuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_texture_parameter_i_uiv!
      self.get_texture_parameter_i_uiv ||
        raise FunctionUnavailableError.new(Translations.get_texture_parameter_i_uiv)
    end

    # Checks if the OpenGL function *glGetTextureParameterIuiv* is available.
    def get_texture_parameter_i_uiv?
      !get_address(618, Translations.get_texture_parameter_i_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_texture_parameter_iv
      get_proc(619, Translations.get_texture_parameter_iv, Procs.get_texture_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureParameteriv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_texture_parameter_iv!
      self.get_texture_parameter_iv ||
        raise FunctionUnavailableError.new(Translations.get_texture_parameter_iv)
    end

    # Checks if the OpenGL function *glGetTextureParameteriv* is available.
    def get_texture_parameter_iv?
      !get_address(619, Translations.get_texture_parameter_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateVertexArrays*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_vertex_arrays
      get_proc(620, Translations.create_vertex_arrays, Procs.create_vertex_arrays)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateVertexArrays*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_vertex_arrays!
      self.create_vertex_arrays ||
        raise FunctionUnavailableError.new(Translations.create_vertex_arrays)
    end

    # Checks if the OpenGL function *glCreateVertexArrays* is available.
    def create_vertex_arrays?
      !get_address(620, Translations.create_vertex_arrays)
    end

    # Retrieves a `Proc` for the OpenGL function *glDisableVertexArrayAttrib*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def disable_vertex_array_attrib
      get_proc(621, Translations.disable_vertex_array_attrib, Procs.disable_vertex_array_attrib)
    end

    # Retrieves a `Proc` for the OpenGL function *glDisableVertexArrayAttrib*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def disable_vertex_array_attrib!
      self.disable_vertex_array_attrib ||
        raise FunctionUnavailableError.new(Translations.disable_vertex_array_attrib)
    end

    # Checks if the OpenGL function *glDisableVertexArrayAttrib* is available.
    def disable_vertex_array_attrib?
      !get_address(621, Translations.disable_vertex_array_attrib)
    end

    # Retrieves a `Proc` for the OpenGL function *glEnableVertexArrayAttrib*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def enable_vertex_array_attrib
      get_proc(622, Translations.enable_vertex_array_attrib, Procs.enable_vertex_array_attrib)
    end

    # Retrieves a `Proc` for the OpenGL function *glEnableVertexArrayAttrib*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def enable_vertex_array_attrib!
      self.enable_vertex_array_attrib ||
        raise FunctionUnavailableError.new(Translations.enable_vertex_array_attrib)
    end

    # Checks if the OpenGL function *glEnableVertexArrayAttrib* is available.
    def enable_vertex_array_attrib?
      !get_address(622, Translations.enable_vertex_array_attrib)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayElementBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_array_element_buffer
      get_proc(623, Translations.vertex_array_element_buffer, Procs.vertex_array_element_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayElementBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_array_element_buffer!
      self.vertex_array_element_buffer ||
        raise FunctionUnavailableError.new(Translations.vertex_array_element_buffer)
    end

    # Checks if the OpenGL function *glVertexArrayElementBuffer* is available.
    def vertex_array_element_buffer?
      !get_address(623, Translations.vertex_array_element_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayVertexBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_array_vertex_buffer
      get_proc(624, Translations.vertex_array_vertex_buffer, Procs.vertex_array_vertex_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayVertexBuffer*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_array_vertex_buffer!
      self.vertex_array_vertex_buffer ||
        raise FunctionUnavailableError.new(Translations.vertex_array_vertex_buffer)
    end

    # Checks if the OpenGL function *glVertexArrayVertexBuffer* is available.
    def vertex_array_vertex_buffer?
      !get_address(624, Translations.vertex_array_vertex_buffer)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayVertexBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_array_vertex_buffers
      get_proc(625, Translations.vertex_array_vertex_buffers, Procs.vertex_array_vertex_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayVertexBuffers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_array_vertex_buffers!
      self.vertex_array_vertex_buffers ||
        raise FunctionUnavailableError.new(Translations.vertex_array_vertex_buffers)
    end

    # Checks if the OpenGL function *glVertexArrayVertexBuffers* is available.
    def vertex_array_vertex_buffers?
      !get_address(625, Translations.vertex_array_vertex_buffers)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayAttribBinding*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_array_attrib_binding
      get_proc(626, Translations.vertex_array_attrib_binding, Procs.vertex_array_attrib_binding)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayAttribBinding*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_array_attrib_binding!
      self.vertex_array_attrib_binding ||
        raise FunctionUnavailableError.new(Translations.vertex_array_attrib_binding)
    end

    # Checks if the OpenGL function *glVertexArrayAttribBinding* is available.
    def vertex_array_attrib_binding?
      !get_address(626, Translations.vertex_array_attrib_binding)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayAttribFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_array_attrib_format
      get_proc(627, Translations.vertex_array_attrib_format, Procs.vertex_array_attrib_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayAttribFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_array_attrib_format!
      self.vertex_array_attrib_format ||
        raise FunctionUnavailableError.new(Translations.vertex_array_attrib_format)
    end

    # Checks if the OpenGL function *glVertexArrayAttribFormat* is available.
    def vertex_array_attrib_format?
      !get_address(627, Translations.vertex_array_attrib_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayAttribIFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_array_attrib_i_format
      get_proc(628, Translations.vertex_array_attrib_i_format, Procs.vertex_array_attrib_i_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayAttribIFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_array_attrib_i_format!
      self.vertex_array_attrib_i_format ||
        raise FunctionUnavailableError.new(Translations.vertex_array_attrib_i_format)
    end

    # Checks if the OpenGL function *glVertexArrayAttribIFormat* is available.
    def vertex_array_attrib_i_format?
      !get_address(628, Translations.vertex_array_attrib_i_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayAttribLFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_array_attrib_l_format
      get_proc(629, Translations.vertex_array_attrib_l_format, Procs.vertex_array_attrib_l_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayAttribLFormat*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_array_attrib_l_format!
      self.vertex_array_attrib_l_format ||
        raise FunctionUnavailableError.new(Translations.vertex_array_attrib_l_format)
    end

    # Checks if the OpenGL function *glVertexArrayAttribLFormat* is available.
    def vertex_array_attrib_l_format?
      !get_address(629, Translations.vertex_array_attrib_l_format)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayBindingDivisor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def vertex_array_binding_divisor
      get_proc(630, Translations.vertex_array_binding_divisor, Procs.vertex_array_binding_divisor)
    end

    # Retrieves a `Proc` for the OpenGL function *glVertexArrayBindingDivisor*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def vertex_array_binding_divisor!
      self.vertex_array_binding_divisor ||
        raise FunctionUnavailableError.new(Translations.vertex_array_binding_divisor)
    end

    # Checks if the OpenGL function *glVertexArrayBindingDivisor* is available.
    def vertex_array_binding_divisor?
      !get_address(630, Translations.vertex_array_binding_divisor)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexArrayiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_vertex_array_iv
      get_proc(631, Translations.get_vertex_array_iv, Procs.get_vertex_array_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexArrayiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_vertex_array_iv!
      self.get_vertex_array_iv ||
        raise FunctionUnavailableError.new(Translations.get_vertex_array_iv)
    end

    # Checks if the OpenGL function *glGetVertexArrayiv* is available.
    def get_vertex_array_iv?
      !get_address(631, Translations.get_vertex_array_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexArrayIndexediv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_vertex_array_indexed_iv
      get_proc(632, Translations.get_vertex_array_indexed_iv, Procs.get_vertex_array_indexed_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexArrayIndexediv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_vertex_array_indexed_iv!
      self.get_vertex_array_indexed_iv ||
        raise FunctionUnavailableError.new(Translations.get_vertex_array_indexed_iv)
    end

    # Checks if the OpenGL function *glGetVertexArrayIndexediv* is available.
    def get_vertex_array_indexed_iv?
      !get_address(632, Translations.get_vertex_array_indexed_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexArrayIndexed64iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_vertex_array_indexed_64iv
      get_proc(633, Translations.get_vertex_array_indexed_64iv, Procs.get_vertex_array_indexed_64iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetVertexArrayIndexed64iv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_vertex_array_indexed_64iv!
      self.get_vertex_array_indexed_64iv ||
        raise FunctionUnavailableError.new(Translations.get_vertex_array_indexed_64iv)
    end

    # Checks if the OpenGL function *glGetVertexArrayIndexed64iv* is available.
    def get_vertex_array_indexed_64iv?
      !get_address(633, Translations.get_vertex_array_indexed_64iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateSamplers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_samplers
      get_proc(634, Translations.create_samplers, Procs.create_samplers)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateSamplers*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_samplers!
      self.create_samplers ||
        raise FunctionUnavailableError.new(Translations.create_samplers)
    end

    # Checks if the OpenGL function *glCreateSamplers* is available.
    def create_samplers?
      !get_address(634, Translations.create_samplers)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateProgramPipelines*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_program_pipelines
      get_proc(635, Translations.create_program_pipelines, Procs.create_program_pipelines)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateProgramPipelines*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_program_pipelines!
      self.create_program_pipelines ||
        raise FunctionUnavailableError.new(Translations.create_program_pipelines)
    end

    # Checks if the OpenGL function *glCreateProgramPipelines* is available.
    def create_program_pipelines?
      !get_address(635, Translations.create_program_pipelines)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateQueries*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def create_queries
      get_proc(636, Translations.create_queries, Procs.create_queries)
    end

    # Retrieves a `Proc` for the OpenGL function *glCreateQueries*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def create_queries!
      self.create_queries ||
        raise FunctionUnavailableError.new(Translations.create_queries)
    end

    # Checks if the OpenGL function *glCreateQueries* is available.
    def create_queries?
      !get_address(636, Translations.create_queries)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryBufferObjecti64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_query_buffer_object_i64v
      get_proc(637, Translations.get_query_buffer_object_i64v, Procs.get_query_buffer_object_i64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryBufferObjecti64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_query_buffer_object_i64v!
      self.get_query_buffer_object_i64v ||
        raise FunctionUnavailableError.new(Translations.get_query_buffer_object_i64v)
    end

    # Checks if the OpenGL function *glGetQueryBufferObjecti64v* is available.
    def get_query_buffer_object_i64v?
      !get_address(637, Translations.get_query_buffer_object_i64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryBufferObjectiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_query_buffer_object_iv
      get_proc(638, Translations.get_query_buffer_object_iv, Procs.get_query_buffer_object_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryBufferObjectiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_query_buffer_object_iv!
      self.get_query_buffer_object_iv ||
        raise FunctionUnavailableError.new(Translations.get_query_buffer_object_iv)
    end

    # Checks if the OpenGL function *glGetQueryBufferObjectiv* is available.
    def get_query_buffer_object_iv?
      !get_address(638, Translations.get_query_buffer_object_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryBufferObjectui64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_query_buffer_object_ui64v
      get_proc(639, Translations.get_query_buffer_object_ui64v, Procs.get_query_buffer_object_ui64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryBufferObjectui64v*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_query_buffer_object_ui64v!
      self.get_query_buffer_object_ui64v ||
        raise FunctionUnavailableError.new(Translations.get_query_buffer_object_ui64v)
    end

    # Checks if the OpenGL function *glGetQueryBufferObjectui64v* is available.
    def get_query_buffer_object_ui64v?
      !get_address(639, Translations.get_query_buffer_object_ui64v)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryBufferObjectuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_query_buffer_object_uiv
      get_proc(640, Translations.get_query_buffer_object_uiv, Procs.get_query_buffer_object_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetQueryBufferObjectuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_query_buffer_object_uiv!
      self.get_query_buffer_object_uiv ||
        raise FunctionUnavailableError.new(Translations.get_query_buffer_object_uiv)
    end

    # Checks if the OpenGL function *glGetQueryBufferObjectuiv* is available.
    def get_query_buffer_object_uiv?
      !get_address(640, Translations.get_query_buffer_object_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glMemoryBarrierByRegion*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def memory_barrier_by_region
      get_proc(641, Translations.memory_barrier_by_region, Procs.memory_barrier_by_region)
    end

    # Retrieves a `Proc` for the OpenGL function *glMemoryBarrierByRegion*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def memory_barrier_by_region!
      self.memory_barrier_by_region ||
        raise FunctionUnavailableError.new(Translations.memory_barrier_by_region)
    end

    # Checks if the OpenGL function *glMemoryBarrierByRegion* is available.
    def memory_barrier_by_region?
      !get_address(641, Translations.memory_barrier_by_region)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureSubImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_texture_sub_image
      get_proc(642, Translations.get_texture_sub_image, Procs.get_texture_sub_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetTextureSubImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_texture_sub_image!
      self.get_texture_sub_image ||
        raise FunctionUnavailableError.new(Translations.get_texture_sub_image)
    end

    # Checks if the OpenGL function *glGetTextureSubImage* is available.
    def get_texture_sub_image?
      !get_address(642, Translations.get_texture_sub_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetCompressedTextureSubImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_compressed_texture_sub_image
      get_proc(643, Translations.get_compressed_texture_sub_image, Procs.get_compressed_texture_sub_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetCompressedTextureSubImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_compressed_texture_sub_image!
      self.get_compressed_texture_sub_image ||
        raise FunctionUnavailableError.new(Translations.get_compressed_texture_sub_image)
    end

    # Checks if the OpenGL function *glGetCompressedTextureSubImage* is available.
    def get_compressed_texture_sub_image?
      !get_address(643, Translations.get_compressed_texture_sub_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetGraphicsResetStatus*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def get_graphics_reset_status
      get_proc(644, Translations.get_graphics_reset_status, Procs.get_graphics_reset_status)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetGraphicsResetStatus*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def get_graphics_reset_status!
      self.get_graphics_reset_status ||
        raise FunctionUnavailableError.new(Translations.get_graphics_reset_status)
    end

    # Checks if the OpenGL function *glGetGraphicsResetStatus* is available.
    def get_graphics_reset_status?
      !get_address(644, Translations.get_graphics_reset_status)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnCompressedTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def getn_compressed_tex_image
      get_proc(645, Translations.getn_compressed_tex_image, Procs.getn_compressed_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnCompressedTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def getn_compressed_tex_image!
      self.getn_compressed_tex_image ||
        raise FunctionUnavailableError.new(Translations.getn_compressed_tex_image)
    end

    # Checks if the OpenGL function *glGetnCompressedTexImage* is available.
    def getn_compressed_tex_image?
      !get_address(645, Translations.getn_compressed_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def getn_tex_image
      get_proc(646, Translations.getn_tex_image, Procs.getn_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnTexImage*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def getn_tex_image!
      self.getn_tex_image ||
        raise FunctionUnavailableError.new(Translations.getn_tex_image)
    end

    # Checks if the OpenGL function *glGetnTexImage* is available.
    def getn_tex_image?
      !get_address(646, Translations.getn_tex_image)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnUniformdv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def getn_uniform_dv
      get_proc(647, Translations.getn_uniform_dv, Procs.getn_uniform_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnUniformdv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def getn_uniform_dv!
      self.getn_uniform_dv ||
        raise FunctionUnavailableError.new(Translations.getn_uniform_dv)
    end

    # Checks if the OpenGL function *glGetnUniformdv* is available.
    def getn_uniform_dv?
      !get_address(647, Translations.getn_uniform_dv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnUniformfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def getn_uniform_fv
      get_proc(648, Translations.getn_uniform_fv, Procs.getn_uniform_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnUniformfv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def getn_uniform_fv!
      self.getn_uniform_fv ||
        raise FunctionUnavailableError.new(Translations.getn_uniform_fv)
    end

    # Checks if the OpenGL function *glGetnUniformfv* is available.
    def getn_uniform_fv?
      !get_address(648, Translations.getn_uniform_fv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnUniformiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def getn_uniform_iv
      get_proc(649, Translations.getn_uniform_iv, Procs.getn_uniform_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnUniformiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def getn_uniform_iv!
      self.getn_uniform_iv ||
        raise FunctionUnavailableError.new(Translations.getn_uniform_iv)
    end

    # Checks if the OpenGL function *glGetnUniformiv* is available.
    def getn_uniform_iv?
      !get_address(649, Translations.getn_uniform_iv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnUniformuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def getn_uniform_uiv
      get_proc(650, Translations.getn_uniform_uiv, Procs.getn_uniform_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glGetnUniformuiv*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def getn_uniform_uiv!
      self.getn_uniform_uiv ||
        raise FunctionUnavailableError.new(Translations.getn_uniform_uiv)
    end

    # Checks if the OpenGL function *glGetnUniformuiv* is available.
    def getn_uniform_uiv?
      !get_address(650, Translations.getn_uniform_uiv)
    end

    # Retrieves a `Proc` for the OpenGL function *glReadnPixels*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def readn_pixels
      get_proc(651, Translations.readn_pixels, Procs.readn_pixels)
    end

    # Retrieves a `Proc` for the OpenGL function *glReadnPixels*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def readn_pixels!
      self.readn_pixels ||
        raise FunctionUnavailableError.new(Translations.readn_pixels)
    end

    # Checks if the OpenGL function *glReadnPixels* is available.
    def readn_pixels?
      !get_address(651, Translations.readn_pixels)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureBarrier*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def texture_barrier
      get_proc(652, Translations.texture_barrier, Procs.texture_barrier)
    end

    # Retrieves a `Proc` for the OpenGL function *glTextureBarrier*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def texture_barrier!
      self.texture_barrier ||
        raise FunctionUnavailableError.new(Translations.texture_barrier)
    end

    # Checks if the OpenGL function *glTextureBarrier* is available.
    def texture_barrier?
      !get_address(652, Translations.texture_barrier)
    end

    # Retrieves a `Proc` for the OpenGL function *glSpecializeShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def specialize_shader
      get_proc(653, Translations.specialize_shader, Procs.specialize_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glSpecializeShader*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def specialize_shader!
      self.specialize_shader ||
        raise FunctionUnavailableError.new(Translations.specialize_shader)
    end

    # Checks if the OpenGL function *glSpecializeShader* is available.
    def specialize_shader?
      !get_address(653, Translations.specialize_shader)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawArraysIndirectCount*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def multi_draw_arrays_indirect_count
      get_proc(654, Translations.multi_draw_arrays_indirect_count, Procs.multi_draw_arrays_indirect_count)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawArraysIndirectCount*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def multi_draw_arrays_indirect_count!
      self.multi_draw_arrays_indirect_count ||
        raise FunctionUnavailableError.new(Translations.multi_draw_arrays_indirect_count)
    end

    # Checks if the OpenGL function *glMultiDrawArraysIndirectCount* is available.
    def multi_draw_arrays_indirect_count?
      !get_address(654, Translations.multi_draw_arrays_indirect_count)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawElementsIndirectCount*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def multi_draw_elements_indirect_count
      get_proc(655, Translations.multi_draw_elements_indirect_count, Procs.multi_draw_elements_indirect_count)
    end

    # Retrieves a `Proc` for the OpenGL function *glMultiDrawElementsIndirectCount*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def multi_draw_elements_indirect_count!
      self.multi_draw_elements_indirect_count ||
        raise FunctionUnavailableError.new(Translations.multi_draw_elements_indirect_count)
    end

    # Checks if the OpenGL function *glMultiDrawElementsIndirectCount* is available.
    def multi_draw_elements_indirect_count?
      !get_address(655, Translations.multi_draw_elements_indirect_count)
    end

    # Retrieves a `Proc` for the OpenGL function *glPolygonOffsetClamp*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Returns nil if the function isn't found or available.
    def polygon_offset_clamp
      get_proc(656, Translations.polygon_offset_clamp, Procs.polygon_offset_clamp)
    end

    # Retrieves a `Proc` for the OpenGL function *glPolygonOffsetClamp*.
    # Attempts to retrieve (load) the address of the function if it hasn't already been retrieved.
    # Raises `FunctionUnavailableError` if the function isn't found or available.
    def polygon_offset_clamp!
      self.polygon_offset_clamp ||
        raise FunctionUnavailableError.new(Translations.polygon_offset_clamp)
    end

    # Checks if the OpenGL function *glPolygonOffsetClamp* is available.
    def polygon_offset_clamp?
      !get_address(656, Translations.polygon_offset_clamp)
    end
  end
end
